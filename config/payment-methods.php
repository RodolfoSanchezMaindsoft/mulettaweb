<?php

return [

    'enabled' => [
        'mercadopago',
    ],

    'use_sandbox' => 'SANDBOX_GATEWAYS',

    'mercadopago' => [
        'logo' => '/img/payment/mercadopago.png',
        'display' => 'MercadoPago',
        'client' => 'TEST-694fe1aa-f3af-4021-af44-c3a2a16cb668',
        'secret' => 'TEST-757646614191839-110719-82c1947e82d20a5d18f742a9f306a593-487206732',
    ],
];