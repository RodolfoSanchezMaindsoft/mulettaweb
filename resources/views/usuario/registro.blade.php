@extends('template.main')

@section('title', 'Registrar')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/register.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover"
                       href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item g-color-primary text-lowercase">
                    <span>Registro</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- START: REGISTER FORM  -->
    <div class="container mb-5 margin-top">
        <div class="g-max-width-445 text-center rounded mx-auto g-pa-20">
            <div class="g-bg-white mb-4">
        
                <div class="text-center ">
                    <h1 class="h4 g-font-weight-600 txt-muletta-gris">CREA TU CUENTA</h1>
                </div>
        
                <!-- Form -->
                <form class="g-py-15" id="register_user" method="POST" action="{{route('register')}}">
                    {{ csrf_field() }}
                    <div class="row no-gutters">
                        <div class="col-4 px-5 mb-4">
                            <div class="input-group">
                                <!-- nombre -->
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15"
                                    value="{{old('nombre')}}"
                                    name="nombre"
                                    type="text"
                                    id="nombre"
                                    placeholder="Nombre">
                            </div>
                        </div>

                        <div class="col-4 px-5 mb-4">
                            <div class="input-group">
                                <!-- apellido -->
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15"
                                    value="{{old('apellidoP')}}"
                                    name="apellidoP"
                                    type="text"
                                    id="apellidoP"
                                    placeholder="Apellido paterno">
                            </div>
                        </div>

                        <div class="col-4 px-5 mb-4">
                            <div class="input-group">
                                <!-- apellido -->
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15"
                                    value="{{old('apellidoM')}}"
                                    name="apellidoM"
                                    type="text"
                                    id="apellidoM"
                                    placeholder="Apellido materno">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <p id="error_nombre"> <span class="help-block g-color-red">Campo obligatorio</span> </p>
                        </div>
                    </div>

                    <!-- fecha de nacimiento -->
                    <div class="row no-gutters text-center">
                        <div class="col-4 px-5 mb-4">
                            <div class="input-group">   
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-py-15 g-px-15"
                                    value="{{old('dia')}}"
                                    name="dia"
                                    type="text"
                                    id="dia"
                                    pattern="[0-9]{2}"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    maxlength="2"
                                    placeholder="Dia (01)">
                            </div>
                        </div>

                        <div class="col-4 px-5 mb-4">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-py-15 g-px-15"
                                    value="{{old('mes')}}"
                                    name="mes"
                                    type="text"
                                    id="mes"
                                    pattern="[0-9]{2}"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    maxlength="2"
                                    placeholder="Mes (01)">
                            </div>
                        </div>

                        <div class="col-4 px-5 mb-4">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-py-15 g-px-15"
                                    value="{{old('año')}}"
                                    name="año"
                                    type="text"
                                    id="anio"
                                    pattern="[0-9]{4}"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    maxlength="4"
                                    placeholder="Año (1999)">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12">
                            <p id="error_Fecha"> <span class="help-block g-color-red">Campo obligatorio</span> </p>
                            <p id="error_Fecha_valid"> <span class="help-block g-color-red">Formato inválido</span> </p>
                        </div>
                    </div>
                    

                    <!-- telefono -->
                    <div class="row no-gutters">
                        <div class="col-xs-12 col-sm-12 mb-4 px-5">
                            <div class="input-group">
                                
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-py-15 g-px-15"
                                    value="{{old('telefono')}}"
                                    name="telefono"
                                    type="text"
                                    id="telefono"
                                    pattern="[0-9]{10}"
                                    onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                    maxlength="10"
                                    require
                                    placeholder="Teléfono (4491265566)">
                            </div>
                            <p id="error_telefono" style="margin-top: 0.5em;"> <span class="help-block g-color-red">Campo obligatorio</span> </p>
                        </div>
                    </div>

                    <!-- correo -->
                    <div class="row no-gutters">
                        <div class="col-xs-12 col-sm-12 mb-4 px-5">
                            <div class="input-group">
                                
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus   g-py-15 g-px-15"
                                    value="{{old('email')}}"
                                    name="email"
                                    type="email"
                                    id="email"
                                    placeholder="Correo electronico">
                            </div>
                            <p id="error_email" style="margin-top: 0.5em;"> <span class="help-block g-color-red">Campo obligatorio</span> </p>
                        </div>
                    </div>
                    
                    <!-- contraseña -->
                    <div class="row no-gutters">
                        <div class="col-xs-12 col-sm-12 mb-4">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15"
                                    value="{{old('contrasena')}}"
                                    name="contrasena"
                                    type="password"
                                    placeholder="Contraseña" id="password-field">
                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password "></span>
                            </div>
                            <p id="error_contraseña" style="margin-top: 0.5em;"> <span class="help-block g-color-red">Campo obligatorio</span> </p>
                            <p id="error_contraseña_min" style="margin-top: 0.5em;"> <span class="help-block g-color-red">Minimo 8 caracteres</span> </p>
                        </div>
                    </div>

                    <div class="row no-gutters">
                        <div class="col-xs-12 col-sm-12 " style="text-align:left;">
                            <!-- Checkboxes Option 2 -->
                            <label class="form-check-inline u-check g-pl-25">
                                <input id="terminos" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" checked="">
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon=""></i>
                                </div>
                                Acepto terminos y condiciónes
                            </label>
                            <p id="error_terminos" style="margin-top: 0.5em;"> <span class="help-block g-color-red">Por favor de aceptar los terminos y condiciones</span> </p>
                            <!-- End Checkboxes Option 2 -->
                        </div>
                        <div class="col-xs-12 col-sm-12 mb-4" style="text-align:left;">
                            <!-- Checkboxes Option 2 -->
                            <label class="form-check-inline u-check g-pl-25">
                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" checked="">
                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon=""></i>
                                </div>
                                Quiero recibir promociones y cupones de descuento
                            </label>
                            <!-- End Checkboxes Option 2 -->
                        </div>
                        
                    </div>
                    
                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-md btn-block u-btn-primary rounded-0 mb-3" type="submit">CREAR CUENTA</button>

                            <div class="text-center">
                                <p class="g-color-gray-dark-v5 mb-0">¿Ya tienes cuenta?
                                <a class="g-font-weight-600 g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('login')}}">Inicia sesión aquí</a></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div> 
    </div>
    <!-- END: REGISTER FORM  -->
@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/global/js/maindsoft/register.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/global/js/maindsoft/login.js') }}"></script>
@endsection