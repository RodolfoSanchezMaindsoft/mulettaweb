@extends('template.main')

<?php
    $perfil_usuario = session()->get('perfilUsuario')['usuario'];
    $date = new \DateTime($perfil_usuario->FECHA_NACIMIENTO);
    $fecha = explode('-', $date->format('Y-m-d') );
    $dia = $fecha[2];
    $mes = $fecha[1];
    $año = $fecha[0];
?>

@section('title', 'Mi cuenta')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/account.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <div class="d-sm-flex ">
                

                <div class="align-self-left ">
                    <ul class="u-list-inline">
                        <li class="list-inline-item g-mr-5">
                            <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                        <li class="list-inline-item g-color-primary">
                            <span>Mi cuenta</span>
                        </li>
                    </ul>
                </div>

                <div class="align-self-center ml-auto">
                    <h1 class="h3 mb-0">Mi Cuenta | {{ $perfil_usuario->NOMBRE }}</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- Start Container-->
    <div class="container g-pt-70 g-pb-30">
        <div class="row">
            <!-- Profile Settings -->
            <div class="col-lg-3 g-mb-50">
                <?php
                    $pagina = 'Mi_Cuenta';
                ?>
                @include('componentes.lateral_perfil')
            </div>
            <!-- End Profile Settings -->

            <!-- Start edit account form -->
            <div class="col-lg-9 g-mb-50" >
                <form method="post" id="from_user" action="{{route('updateAccount')}}">
                    {{ csrf_field() }}
                    <meta name="csrf-token" content="{{ csrf_token() }}">

                    <input type="hidden" class="form-control" name="email_ant" id="id" value="<?=$perfil_usuario->EMAIL?>">
                    <div class="g-brd-around g-brd-gray-light-v4 rounded g-pa-30 g-mb-30">
                        <div class="row" style="margin-left: -14px;">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-12 col-md-4">
                                        <span class="d-block g-color-text g-font-size-13 mb-1">Nombre:</span>
                                        <input type="text" class="form-control" name="nombre" id="nombre" maxlength="500" value="<?=$perfil_usuario->NOMBRE?>">
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <span class="d-block g-color-text g-font-size-13 mb-1">Apellido paterno:</span>
                                        <input type="text" class="form-control" name="apellidoP" id="apellidoP" maxlength="500" value="<?=$perfil_usuario->APELLIDO_PATERNO?>">
                                    </div>
                                    <div class="col-sm-12 col-md-4">
                                        <span class="d-block g-color-text g-font-size-13 mb-1">Apellido materno:</span>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="apellidoM" id="apellidoM" maxlength="500" value="<?=$perfil_usuario->APELLIDO_MATERNO?>">

                                            <div class="input-group-prepend">
                                                <span id="btn-nombre" class="input-group-text g-width-45 g-brd-right-none g-brd-white g-color-gray-dark-v3">
                                                    <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr class="g-brd-gray-light-v4 g-my-20">

                        <div class="row">
                            <div class="col-12">
                                <span class="d-block g-color-text g-font-size-13 mb-1">Teléfono:</span>
                                <div class="input-group">
                                    <input type="text" pattern="[0-9]{10}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10" class="form-control" name="tel" id="tel" value="<?=$perfil_usuario->TELEFONO?>">
                                    
                                    <div class="input-group-prepend">
                                        <span id="btn-tel" class="input-group-text g-width-45 g-brd-right-none g-brd-white g-color-gray-dark-v3"><i class="fa fa-pencil-alt" aria-hidden="true"></i></span>
                                    </div>
                                </div>
                                <p id="error_telefono" style="margin-top: 0.5em;"> <span class="help-block g-color-red">Campo obligatorio</span> </p>
                            </div>
                        </div>

                        <hr class="g-brd-gray-light-v4 g-my-20">

                        <div class="row">
                            <div class="col-12">
                                <span class="d-block g-color-text g-font-size-13 mb-1">Email:</span>
                                <input type="email" class="form-control" maxlength="100" id="email" value="<?=$perfil_usuario->EMAIL?>">
                            </div>
                        </div>

                        <hr class="g-brd-gray-light-v4 g-my-20">

                         <!-- fecha de nacimiento -->
                        <div class="row">
                            <div class="col-8">
                                <span class="d-block g-color-text g-font-size-13 mb-1">Cumpleaños:</span>
                                <div class="row no-gutters text-center">
                                    <div class="col-3">
                                        <div class="input-group">   
                                            <input class="form-control"
                                                value="{{$dia}}"
                                                name="dia"
                                                type="text"
                                                required
                                                id="dia"
                                                pattern="[0-9]{2}"
                                                onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                maxlength="2"
                                                placeholder="Dia (01)">
                                        </div>
                                    </div>
                                    <div class="col-1" style="font-size: 2em;">/</div>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="form-control"
                                                value="{{$mes}}"
                                                name="mes"
                                                type="text"
                                                required
                                                id="mes"
                                                pattern="[0-9]{2}"
                                                onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                maxlength="2"
                                                placeholder="Mes (01)">
                                        </div>
                                    </div>
                                    <div class="col-1" style="font-size: 2em;">/</div>
                                    <div class="col-3">
                                        <div class="input-group">
                                            <input class="form-control"
                                                value="{{$año}}"
                                                name="anio"
                                                required
                                                type="text"
                                                id="anio"
                                                pattern="[0-9]{4}"
                                                onkeypress='return event.charCode >= 48 && event.charCode <= 57'
                                                maxlength="4"
                                                placeholder="Año (1999)">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <p id="error_Fecha_valid"> <span class="help-block g-color-red">Formato inválido</span> </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4 text-right" style="display: table;height: 10vh;">
                                <a id="btn-datepicker" class="g-color-gray-dark-v3 g-font-size-13 g-px-18 g-py-7" href="#!" style="display: table-cell;vertical-align: middle;">
                                    <i class="fa fa-pencil-alt" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        
                        @if($perfil_usuario->TIPO_LOGIN == 1)
                            <hr class="g-brd-gray-light-v4 g-my-20">

                            <div class="row">
                                <div class="col-12">
                                    <span class="d-block g-color-text g-font-size-13 mb-1">Contraseña:</span>
                                    <div class="input-group">
                                        <input type="text" class="form-control" disabled value="*******">
                                        
                                        <div class="input-group-prepend">
                                            <span id="btn-contrasenia" class="input-group-text g-width-45 g-brd-right-none g-brd-white g-color-gray-dark-v3"><i class="fa fa-pencil-alt" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif()

                        <div class="row">
                            <div class="col-12 text-right">
                                <button id="btn-actualizar" class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 ml-auto margin-top" type="submit">Actualizar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End edit account form -->
        </div>
    </div>

    <!-- Modal para editar las contraseñas -->
    <div class="modal fade" id="modalCheckout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Cambiar contraseña</h3>
                    <a href="#!" data-dismiss="modal" class="float-right g-font-size-20 g-text-underline--none--hover g-color-muletta-gray-dark g-color-primary--hover g-mb-10">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <form id="form-pass">
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id_cliente" class="form-control" value="<?=$perfil_usuario->ID_CLIENTE?>">
                        <div class="form-group">
                            <label for="password">Contraseña anterior</label>
                            <input type="password" name="password_ant" id="password_ant" class="form-control" style="width: 100%; !important">
                            <p id="txt-password_ant_valid" style="color: red;">Campo requerido</p>
                            <p id="txt-password_ant" style="color: red;">Contraseña incorrecta</p>
                        </div>

                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input type="password" name="password" id="password" class="form-control" style="width: 100%; !important">
                            <p id="txt-password_valid" style="color: red;">Campo requerido</p>
                            <p id="txt-password_valid_length" style="color: red;">Minimo 8 caracteres</p>
                        </div>

                        <div class="form-group">
                            <label for="password">Repetir contraseña</label>
                            <input type="password" name="repeat_password" id="repeat_password" class="form-control" style="width: 100%; !important">
                            <p id="txt-contrasenia" style="color: red;">Las contraseñas no coinciden</p>
                            <p id="txt-repeat_password_valid" style="color: red;">Campo requerido</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btn_contrasenia_update" type="button" class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 ml-auto">Guardar contraseña</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal para editar las contraseñas -->

@endsection


@section('script')
    <script src="{{ asset('assets/global/js/maindsoft/account.js') }}"></script>
@endsection