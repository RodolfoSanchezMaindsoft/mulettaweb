@extends('template.main')

@section('title', 'Registrar')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/register.css') }}">
@endsection

@section('content')
     <!-- Pestañas Navegación -->
     <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover"
                       href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item g-color-primary text-lowercase">
                    <span>Restablecer</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- START: CANT PASSWORD BOX   -->
    <div class="container mb-5 margin-top">
        <div class="g-max-width-445 text-center rounded mx-auto g-pa-20">
            <div class="g-bg-white mb-4">
        
                <header class="text-center ">
                    <h1 class="h4 g-font-weight-600 txt-muletta-gris">NO SE PUEDE ENVIAR CORREO PARA RE ESTABLECER TU CONTRASEÑA</h1>
                </header>
        
                   

                    <div class="row">
                        <div class="col-12">
                            <p>No contamos con una contraseña para esta cuenta. Puede que hayas iniciado sesión a esta pagina usando tu cuenta de Facebook o Google. Prueba iniciando session con Facebook o tu cuente de Google. </p>
                            
                            <a class="btn btn-block u-btn-primary g-font-size-20 text-uppercase  g-px-25 mb-3 g-color-white--hover" href="{{route('login')}}">
                                INICIAR SESIÓN
                            </a>
                            
                        </div>
                    </div>
               
            </div>
        </div>
    </div>
     <!-- END: CANT PASSWORD BOX   -->
     
     <!-- START: CREA UN NUEVO USUARIO -->
    <div class="container mb-3">
        <div class="g-max-width-445 text-center rounded mx-auto g-pa-20">
            <div class="g-bg-white mb-4">
                 
                <div class="row">
                    <div class="col-12">
                       <P>Si quieres contar con una contraseña para mayor seguridad, puedes probar generando una cuenta nueva usando tu correo.</P>
                        
                        <a class="btn btn-block u-btn-primary g-font-size-20 text-uppercase  g-px-25 mb-3 g-color-white--hover" href="{{route('register')}}">
                            CREA NUEVO USUARIO
                        </a>
                        
                    </div>
                </div>
               
            </div>
        </div>
    </div>
    <!-- END: CREA UN NUEVO USUARIO -->

@endsection