<aside class="g-brd-around g-brd-gray-light-v4 rounded g-px-20 g-py-30">
    <!-- Profile Picture -->
    <div class="text-center g-pos-rel g-mb-30">
        <div class="g-width-100 g-height-100 mx-auto mb-3">
            <img class="img-fluid rounded-circle" src="{{ asset('assets/global/img/compras/user.png') }}" alt="Foto Perfil">
        </div>

        <span class="d-block g-font-weight-500">{{ $perfil_usuario->NOMBRE }}</span>
        <?php
            $virtualMoney = session()->get('virtualMoney')['virtualMoney'];
        ?>

        <span class="d-block align-items-end u-link-v5 g-color-text  g-color-black--focus g-color-black--hover g-color-primary--hover g-font-size-14 g-bg-gray-light-v5--hover rounded g-pa-3">
            Saldo disponible
        </span>
        <span style="display: flex;justify-content: center;">
            <span class="txt-muletta-oro coins-cantidad" style="font-size: 20px;">
                <img src="{{ asset('assets/global/img/icons/moneda_muletta.png') }}" class="g-width-20 g-height-20">
                {{ $virtualMoney && $virtualMoney->DINERO_VIRTUAL ? $virtualMoney->DINERO_VIRTUAL : '0.00' }}
            </span>
        </span>
    </div>
    <!-- End Profile Picture -->

    <hr class="g-brd-gray-light-v4 g-my-30">

    <!-- Profile Settings List -->
    <ul class="list-unstyled mb-0">
    <li class="g-pb-3 g-pa-5">
            <a class="{{ $pagina == 'Mi_Cuenta' ? 'active' : '' }} d-block align-items-end u-link-v5 g-color-text  g-color-black--focus g-color-black--hover g-color-primary--hover g-font-size-14 g-bg-gray-light-v5--hover rounded g-pa-3" href="{{route('mi-cuenta')}}">
                <img class="g-width-20 g-height-20" src="{{ asset('assets/global/img/icons/iconos-pw-08.png') }}" alt="icono Perfil">
                Mi Cuenta
            </a>
        </li>
        <li class="g-pb-3 g-pa-5">
            <a class="{{ $pagina == 'Pedidos' ? 'active' : '' }} d-block align-items-end u-link-v5 g-color-text  g-color-black--focus g-color-black--hover g-color-primary--hover g-font-size-14 g-bg-gray-light-v5--hover rounded g-pa-3" href="{{route('editar-cuenta')}}">
                <img class=" g-width-20 g-height-20" src="{{ asset('assets/global/img/icons/iconos-pw-09.png') }}" alt="icono pedidos">
                Mis Pedidos
            </a>
        </li>
        <li class="g-pb-3 g-pa-5">
            <a class="{{ $pagina == 'Devoluciones' ? 'active' : '' }} d-block align-items-end u-link-v5 g-color-text  g-color-black--focus g-color-black--hover g-color-primary--hover g-font-size-14 g-bg-gray-light-v5--hover rounded g-pa-3" href="{{route('returns')}}">
                <img class=" g-width-20 g-height-20" src="{{ asset('assets/global/img/icons/iconos_cambios.png') }}" alt="icono pedidos">
                Cambios y Devoluciones
            </a>
        </li>
        <li class="g-pb-3 g-pa-5">
            <a class="{{ $pagina == 'Direcciones' ? 'active' : '' }} d-block align-items-end u-link-v5 g-color-text  g-color-black--focus g-color-black--hover g-color-primary--hover g-font-size-14 g-bg-gray-light-v5--hover rounded g-pa-3" href="{{route('editar-direcciones')}}">
                <img class="mr-auto g-width-20 g-height-20" src="{{ asset('assets/global/img/icons/iconos-pw-10.png') }}" alt="icono direcciones">
                Mis Direcciones
            </a>
        </li>
        <li class="g-pb-3 g-pa-5">
            <a class="{{ $pagina == 'muletta-cash' ? 'active' : '' }} d-block align-items-end u-link-v5 g-color-text  g-color-black--focus g-color-black--hover g-color-primary--hover g-font-size-14 g-bg-gray-light-v5--hover rounded g-pa-3" href="{{route('MulettaCash')}}">
                <img class="mr-auto g-width-20 g-height-20" src="{{ asset('assets/global/img/icons/iconos_cash.png') }}" alt="icono direcciones">
                Muletta Cash
            </a>
        </li>
        <li class="g-pb-3 g-pa-5">
            <a class="d-block align-items-end u-link-v5 g-color-text  g-color-black--focus g-color-black--hover g-color-primary--hover g-font-size-14 g-bg-gray-light-v5--hover rounded g-pa-3" href="{{route('logout')}}">
                <img class="mr-auto g-width-20 g-height-20" src="{{ asset('assets/global/img/icons/iconos-pw-14.png') }}" alt="icono logout"> 
                Cerrar sesión
            </a>
        </li>
    </ul>
    <!-- End Profile Settings List -->
</aside>