<?php
    $generos = session()->get('genero')['genero'];
    $tipoProducto = session()->get('tipoProducto')['tipoProducto'];
    $clasificacionProductos = session()->get('clasificacionProductos')['clasificacionProductos'];
?>

<input type="hidden" id="url_tallas" value="<?=Route('tallasProductos')?>">

{{ csrf_field() }}
<meta name="csrf-token" content="{{ csrf_token() }}">

<div id="menu-wrapper-productos" class="g-pt-30 g-mb-50">

    <div>
        <a href="#" class="closeMenu" onclick="cerrar_menu()">
            <i class="fa fa-times"></i>
        </a>
    </div>

    <div id="div-filtros">
        <span class="titulo">
            GÉNERO
        </span>
        <ul>
            @foreach($generos as $genero)
                @if($genero->ID < 3)
                    <?php
                        $checked_genero = '';
                        if( isset($genero_seleccionado) ){
                            for ($i=0; $i < count($genero_seleccionado); $i++) { 
                                if ($genero_seleccionado[$i] == $genero->ID) {
                                    $checked_genero = 'checked';
                                }
                            }
                        }
                    ?>
                    <li class="checkbox-inline">
                        <input type="checkbox" name="genero" <?=$checked_genero?> id="genero-<?=$genero->DESCRIPCION?>" value="<?=$genero->ID?>">
                        <span id="genero-<?=$genero->DESCRIPCION?>-txt" > {{ $genero->DESCRIPCION }} </span>
                        <span class="checkmark"></span>
                    </li>
                @endif
            @endforeach
        </ul>

        <span class="titulo">
            PRODUCTOS
        </span>
        <ul>
            @foreach($tipoProducto as $tipo_prod)
                <?php
                    $checked_producto = '';
                    if( isset($producto_seleccionado) ){
                        for ($i=0; $i < count($producto_seleccionado); $i++) { 
                            if ($producto_seleccionado[$i] == $tipo_prod->ID) {
                                $checked_producto = 'checked';
                            }
                        }
                    }
                ?>
                @if($tipo_prod->ID > 0 && $tipo_prod->ID != 17 && $tipo_prod->ID != 2 && $tipo_prod->ID != 14)
                    <li class="checkbox-inline">
                        <input type="checkbox" name="productos" id="tipo-<?=$tipo_prod->DESCRIPCION?>" <?=$checked_producto?> value="<?=$tipo_prod->ID?>" onchange="changeTipo()" >
                        <span id="tipo-<?=$tipo_prod->DESCRIPCION?>-txt" > {{ $tipo_prod->DESCRIPCION }} </span>
                        <span class="checkmark"></span>
                    </li>
                @endif
            @endforeach
        </ul>

        <span class="titulo">
            TALLAS DISPONIBLES
        </span>
        <ul>
            <?php
                $checked_todas_tallas = '';
                if( !isset($tallas_seleccionadas) || count($tallas_seleccionadas) == 0 ){
                    $checked_todas_tallas = 'checked';
                }
            ?>
            <li class="checkbox-inline">
                <input type="checkbox" name="tallas" id="tallas-todas" <?=$checked_todas_tallas?> value="0" onchange="changeCheckbox(0)">
                <span id="tallas-todas-txt" >TODAS</span>
                <span class="checkmark"></span>
            </li>
            @foreach($tallas as $talla)
                <?php
                    $checked_talla = '';
                    if( isset($tallas_seleccionadas) ){
                        for ($i=0; $i < count($tallas_seleccionadas); $i++) { 
                            if ($tallas_seleccionadas[$i] == $talla->ID) {
                                $checked_talla = 'checked';
                            }
                        }
                    }
                ?>
                <li class="checkbox-inline">
                    <input type="checkbox" name="tallas" <?=$checked_talla?> id="tallas-<?=str_replace(' ', '', $talla->DESCRIPCION)?>" value="<?=$talla->ID?>" onchange="changeCheckbox(1)">
                    <span id="tallas-<?=str_replace(' ', '', $talla->DESCRIPCION)?>-txt" class="tallas-class"><?=$talla->DESCRIPCION?></span>
                    <span class="checkmark"></span>
                </li>
            @endforeach
        </ul>

        <span class="titulo">
            PRECIO
        </span>
        <div class="precio-top">
            <div class="row">
                <div class="col-5 offset-1 border text-center">
                    <span type="text" id="price_min" disabled class="number_range"></span>
                </div>
                <div class="col-5 offset-1 border text-center">
                    <span type="text" id="price_max" disabled class="number_range"></span>
                </div>
            </div>
        </div>
        <div id="slider-3"></div>

    </div>
</div>

<div class="text-center margin-top">
    <button type="button" class="btn-block u-btn-primary g-font-size-12 text-uppercase g-py-6 g-px-25 g-brd-transparent g-rounded-0" onclick="buscar()">
        Aplicar
    </button>
</div>