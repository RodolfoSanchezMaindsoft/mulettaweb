<?php
    $clases = 'col-xs-12 col-md-4 col-lg-4 g-mb-30 g-pa-7';
    $menu_PrimerNivel = session()->get('menuPrimerNivel');
    // var_dump($id_primer_nivel);
    // exit;
    foreach($menu_PrimerNivel as $primer){
        foreach($primer as $details){
            if($sub == $details->ID_CATEGORIA_FLEXIBLE){
                if ($details->ID_CATEGORIA_FLEXIBLE == 31) {
                    $clases = 'col-md-3 col-xs-12 col-lg-3 g-mb-30 g-pa-7';
                }
            }
        }
    }
?>
<div id="wrapper">
    <div class="row g-pt-30 g-mb-50">
    <!-- <div id="infinite" class="row g-pt-30 g-mb-50 scrollbar"> -->
        @if ($productos)
            @foreach($productos as $producto)
            
                <div id="product" class="<?=$clases?>">
                    <a href="{{ route('producto_individual', ['DESCRIPCION_MODELO' => str_replace(' ','-',str_replace('/','-',$producto->DESCRIPCION_MODELO)), 'ID_MODELO' => $producto->ID_MODELO]) }}">

                        @if(isset($producto->TIPO_DESCUENTO) && $producto->TIPO_DESCUENTO == 2)
                            <div class="offer-desc">
                                <text class="texto-oferta-verde">-{{ $producto->PORCENTAJE_DESCUENTO }}%</text>
                            </div>
                        @endif


                        <figure class="g-pos-rel g-mb-20">
                            
                            @if(substr($producto->IMAGEN_100, 0, 4) != 'http')
                                <img id="imgProd" class="photo img-fluid w-100" data-src="{{!empty($producto->IMAGEN_100) ? str_replace('~', '', 'http://creatmos.net'.str_replace(' ','%20',$producto->IMAGEN_100))  : 'https://via.placeholder.com/605x745'}}" alt="{{$producto->DESCRIPCION_MODELO}}">
                            @else
                                <img id="imgProd" class="photo img-fluid w-100" data-src="{{!empty($producto->IMAGEN_100) ? str_replace(' ','%20',$producto->IMAGEN_100)  : 'https://via.placeholder.com/605x745'}}" alt="{{$producto->DESCRIPCION_MODELO}}">
                            @endif
                            
                            @if($producto->EXISTENCIA <= 0)
                                <span class="text-danger" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 4em;">AGOTADO</span>
                            @endif
                            
                        </figure>

                        <div class="media text-center">

                            <div class="product-content text-center">
                                <h2>{{$producto->DESCRIPCION_MODELO}} {{ $producto->EXISTENCIA }} </h2>
                                @if(($producto->PRECIO_VENTA_DESCUENTO != NULL && $producto->PRECIO_VENTA != $producto->PRECIO_VENTA_DESCUENTO))
                                    <span style="text-decoration:line-through; color: red !important;">${{number_format($producto->PRECIO_VENTA, 2)}}</span>  &nbsp;
                                    <span style="font-weight: bold !important;">${{number_format($producto->PRECIO_VENTA_DESCUENTO, 2)}}</span>
                                @else
                                    <span style="font-weight: bold !important;">${{number_format($producto->PRECIO_VENTA, 2)}}</span>
                                @endif
                                <br>
                                @if(($producto->_XS !== null) && ($producto->_XS >= 0) || ($producto->_S !== null) && ($producto->_S >= 0) || ($producto->_M !== null) && ($producto->_M >= 0) || ($producto->_L !== null) && ($producto->_L >= 0)
                                || ($producto->_XL !== null) && ($producto->_XL >= 0) || ($producto->_2XL !== null) && ($producto->_2XL >= 0) || ($producto->_3XL !== null) && ($producto->_3XL >= 0))
                                    <span class="{{ $producto->_XS > 0 ? 'existe-talla' : 'no-existe-talla' }}">XS <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_S > 0 ? 'existe-talla' : 'no-existe-talla' }}">S <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_M > 0 ? 'existe-talla' : 'no-existe-talla' }}">M <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_L > 0 ? 'existe-talla' : 'no-existe-talla' }}">L <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_XL > 0 ? 'existe-talla' : 'no-existe-talla' }}">XL <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_2XL > 0 ? 'existe-talla' : 'no-existe-talla' }}">2XL <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_3XL > 0 ? 'existe-talla' : 'no-existe-talla' }}"> 3XL</span>
                                @endif
                                @if($producto->_U !== null && ($producto->_U >= 0))
                                    <span class="{{ $producto->_U > 0 ? 'existe-talla' : 'no-existe-talla' }}"> U </span>
                                @endif
                                @if(($producto->_18cm !== null) && ($producto->_18cm >= 0) || ($producto->_19cm !== null) && ($producto->_19cm >= 0) || ($producto->_20cm !== null) && ($producto->_20cm >= 0) || ($producto->_21cm !== null) && ($producto->_21cm >= 0))
                                    <span class="{{ $producto->_18cm > 0 ? 'existe-talla' : 'no-existe-talla' }}">18cm <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_19cm > 0 ? 'existe-talla' : 'no-existe-talla' }}">19cm <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_20cm > 0 ? 'existe-talla' : 'no-existe-talla' }}">20cm <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_21cm > 0 ? 'existe-talla' : 'no-existe-talla' }}"> 21cm</span>
                                @endif
                                @if(($producto->_CH !== null) && ($producto->_CH >= 0) || ($producto->_MED !== null) && ($producto->_MED >= 0) || ($producto->_GDE !== null) && ($producto->_GDE >= 0))
                                    <span class="{{ $producto->_CH > 0 ? 'existe-talla' : 'no-existe-talla' }}">CH <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_MED > 0 ? 'existe-talla' : 'no-existe-talla' }}">MED <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_GDE > 0 ? 'existe-talla' : 'no-existe-talla' }}"> GDE</span>
                                @endif
                                @if(($producto->_28 !== null) && ($producto->_28 >= 0) || ($producto->_30 !== null) && ($producto->_30 >= 0) || ($producto->_32 !== null) && ($producto->_32 >= 0) || ($producto->_34 !== null) && ($producto->_34 >= 0)
                                || ($producto->_36 !== null) && ($producto->_36 >= 0) || ($producto->_38 !== null) && ($producto->_38 >= 0) || ($producto->_40 !== null) && ($producto->_40 >= 0))
                                    <span class="{{ $producto->_28 > 0 ? 'existe-talla' : 'no-existe-talla' }}">28 <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_30 > 0 ? 'existe-talla' : 'no-existe-talla' }}">30 <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_32 > 0 ? 'existe-talla' : 'no-existe-talla' }}">32 <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_34 > 0 ? 'existe-talla' : 'no-existe-talla' }}">34 <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_36 > 0 ? 'existe-talla' : 'no-existe-talla' }}">36 <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_38 > 0 ? 'existe-talla' : 'no-existe-talla' }}">38 <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_40 > 0 ? 'existe-talla' : 'no-existe-talla' }}"> 40</span>
                                @endif
                                @if($producto->_UNI !== null && ($producto->_UNI >= 0))
                                    <span class="{{ $producto->_UNI > 0 ? 'existe-talla' : 'no-existe-talla' }}"> UNI </span>
                                @endif
                                @if(($producto->_CHXS !== null) && ($producto->_CHXS >= 0) || ($producto->_CHS !== null) && ($producto->_CHS >= 0) || ($producto->_CHM !== null) && ($producto->_CHM >= 0) || ($producto->_CHL !== null) && ($producto->_CHL >= 0)
                                || ($producto->_CHXL !== null) && ($producto->_CHXL >= 0) || ($producto->_CH2XL !== null) && ($producto->_CH2XL >= 0))
                                    <span class="{{ $producto->_CHXS > 0 ? 'existe-talla' : 'no-existe-talla' }}">CHXS <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_CHS > 0 ? 'existe-talla' : 'no-existe-talla' }}">CHS <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_CHM > 0 ? 'existe-talla' : 'no-existe-talla' }}">CHM <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_CHL > 0 ? 'existe-talla' : 'no-existe-talla' }}">CHL <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_CHXL > 0 ? 'existe-talla' : 'no-existe-talla' }}">CHXL <i class="fa fa-circle" style="font-size: 3px;"></i></span>
                                    <span class="{{ $producto->_CH2XL > 0 ? 'existe-talla' : 'no-existe-talla' }}"> CH2XL</span>
                                @endif
                            </div>

                        </div>
                    </a>

                </div>

            @endforeach

        @endif
    </div>
</div>