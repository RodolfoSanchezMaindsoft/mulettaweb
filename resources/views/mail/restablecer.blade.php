<!DOCTYPE html>
<html>
    <head>
        <style>
            #contenido {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            #contenido td,
            #contenido th {
                border: 1px solid #ddd;
                padding: 8px;
                text-align: center;
            }
            
            h1{
                text-align: center;
            }

            #contenido tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            #contenido tr:hover {
                background-color: #ddd;
            }

            #contenido th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #b78b1e;
                color: white;
                text-align: center;
            }

            #datos-solicitados {
                width: 30% !important;
            }

            #info-recibida {
                width: 70% !important;
            }

        </style>
    </head>

    <body>

        <center>
            <img src="{{ asset('assets/global/img/logo/logo-muletta-header.png') }}" width="200" height="50" alt="Logo-Muletta">
        </center>
        <h3>Hola {{ $orden['nombre'] }}, te hemos enviado la contraseña para ingresar a nuestra página web o aplicación movil </h3>

        <table id="contenido">
            <tr>
                <th id="datos-solicitados">Tu contraseña es:</th>
            </tr>
            <tr>
                <td>{{ $orden['password'] }}</td>
            </tr>
        </table>
    </body>

</html>
