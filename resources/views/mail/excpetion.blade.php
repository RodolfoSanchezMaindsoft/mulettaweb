@if($detalles != null)
    <table>
        <tbody>
            <tr>
                <td style="font-weight: 600; border: 1px solid black; background-color: #ac822e; color: #ffffff;">Código de error</td>
                <td style="border: 1px solid black;"><?= $detalles['codigo'] ?></td>
            </tr>
            <tr>
                <td style="font-weight: 600; border: 1px solid black; background-color: #ac822e; color: #ffffff;">Url Actual</td>
                <td style="border: 1px solid black;"><?= $detalles['url_actual'] ?></td>
            </tr>
            <tr>
                <td style="font-weight: 600; border: 1px solid black;">Url anterior</td>
                <td style="border: 1px solid black;"><?= $detalles['urlAnterior'] ?></td>
            </tr>
            <tr>
                <td style="font-weight: 600; border: 1px solid black; background-color: #ac822e; color: #ffffff;">Archivo del error</td>
                <td style="border: 1px solid black;"><?= $detalles['archivoError'] ?></td>
            </tr>
            <tr>
                <td style="font-weight: 600; border: 1px solid black; background-color: #ac822e; color: #ffffff;">Linea del error</td>
                <td style="border: 1px solid black;"><?= $detalles['lineaError'] ?></td>
            </tr>
            <tr>
                <td style="font-weight: 600; border: 1px solid black; background-color: #ac822e; color: #ffffff;">Origen</td>
                <td style="border: 1px solid black;"><?= $detalles['origen'] ?></td>
            </tr>
            <tr>
                <td style="font-weight: 600; border: 1px solid black; background-color: #ac822e; color: #ffffff;">Mensaje</td>
                <td style="border: 1px solid black;"><?= $detalles['mensaje'] ?></td>
            </tr>
            <tr>
                <td style="font-weight: 600; border: 1px solid black; background-color: #ac822e; color: #ffffff;">Parametros post</td>
                <td style="border: 1px solid black;"><?= $detalles['params_post'] ?></td>
            </tr>
            <tr>
                <td style="font-weight: 600; border: 1px solid black; background-color: #ac822e; color: #ffffff;">Parametros get</td>
                <td style="border: 1px solid black;"><?= $detalles['params_params'] ?></td>
            </tr>
        </tbody>
    </table>
@endif

{!! $content !!}