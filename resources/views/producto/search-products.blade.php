@extends('template.main')

@section('title', 'Busqueda')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/catalogo-productos.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/filtros.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container-flex grid-menu">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item txt-muletta-oro text-lowercase">
                    <span>Busqueda</span>
                </li>
            </ul>

            <div class="filtro-btn">
                <div class="txt-muletta-oro" onclick="mostrarFiltros()">
                    <span class="cool-link">
                        <i class="fa fa-sliders-h"></i> &nbsp;
                        FILTROS
                    <span>
                </div>
                <div id="menu-categoria">
                    @include('componentes.filtros_productos')
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Pestañas Navegación -->

    <div class="container">
        <div class="row margin-top d-flex justify-content-center">
            <div class="col-sm-12 col-md-8 col-lg-6">
                <form id="search-view" class="js-validate js-step-form form-inline mr-auto ga-search-form" action="{{ Route('buscar_productos') }}" method="post">
                    {{ csrf_field() }}
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <div class="input-group">
                        <input aria-label="Caja de búsqueda" id="NOMBRE" autocomplete="off" maxlength="64" placeholder="Buscar por..." name="NOMBRE" value="{{ $name }}" inputmode="search" type="search" class="form-control rounded-0 form-control-md ">
                        <div class="input-group-append">
                            <button id="search-btn" type="button" onclick="buscar()" class="btn btn-primary btn-sm ng-binding ng-isolate-scope btn-sm my-0 ml-sm-2" >Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-flex">
        <div class="row">
            <!-- Content -->
            <div class="col-md-12 order-md-2">
                <div id="div_productos" class="g-pl-15--lg">
                    <!-- Products -->
                    @if($productos != "Sin resultados")
                        @include('componentes.productos')
                    @else
                        <div class="row g-pt-30 g-mb-50">
                            <div class="no-results" style="min-height:50vw;">
                                <h1><?=$productos?>...</h1>
                            </div>
                        </div>
                    @endif
                    <!-- End Products -->
                </div>
            </div>
            <!-- End Content -->
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/global/js/maindsoft/filtros.js') }}"></script>
@endsection