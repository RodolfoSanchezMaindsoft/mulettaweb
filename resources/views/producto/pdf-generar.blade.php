<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="<?=asset('assets/global/css/maindsoft/pdf.css')?>">
        <link rel="stylesheet" href="<?=asset('assets/global/css/bootstrap/bootstrap.min.css')?>">
    </head>
    <body>
        
        <table class="table">
            <tr>
                <td class="text-center" style="width: 45%;">
                    <img class="herradura" src="https://muletta.com/assets/global/img/logo/logo-muletta-footer.png" width="70px">
                    <br>
                    <img src="https://muletta.com/assets/global/img/logo/logo-muletta-header.png">
                </td>
                <td style="width: 55%;">
                    <h4>CAMBIOS Y DEVOLUCIONES</h4>
                    <div class="division" style="border-top: 1px #cecece solid;">
                        <span style="color:#8a8a8a">ID:</span>
                        {{ $devolucion->NO_TICKET }}
                    </div>
                    <div class="division">
                        <span style="color:#8a8a8a">PEDIDO:</span>
                        {{ $devolucion->ID_VENTA}}
                    </div>
                    <div class="division">
                        <span style="color:#8a8a8a">MOTIVO:</span>
                        {{ $devolucion->RAZON}}
                    </div>
                    <div class="division">
                        <span style="color:#8a8a8a">FECHA DE ORDEN:</span>
                        {{ $devolucion->FECHA }} &nbsp;&nbsp; {{ $devolucion->HORA }}
                    </div>
                    <div class="division">
                        <span style="color:#8a8a8a">CLIENTE:</span>
                        {{ $devolucion->NOMBRE}}
                    </div>
                </td>
            </tr>
        </table>

        <h4 style="color:#8a8a8a">DETALLES DEL PRODUCTO:</h4>

        <?php
            $table = '';
            foreach($producto_final as $index=>$producto){
                $total = 0;
                $table = $table.'<table class="table">
                    <thead>
                        <tr class="text-center">';
                            foreach($producto as $key=>$prod){
                                $table = $table.'<th style="color:#8a8a8a">'.$key.'</th>';
                            }
            $table = $table.'<th style="color:#8a8a8a">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-center">';
                        foreach($producto as $prod){
                            if (is_numeric($prod)) {
                                $total = $total + $prod;
                            }
                            $table = $table.'<td>'.$prod.'</td>';
                        }
            $table = $table.'<td>'.$total.'</td>
                        </tr>
                    </tbody>
                </table>';
            }

            echo $table;
        ?>

        <div class="text-center">
            <h6 style="color:#8a8a8a">INSTRUCCIONES</h6>
        </div>

        <table class="table text-justify" style="font-size:10px;">
            <tr>
                <td style="width: 50%">
                    <p style="color:#8a8a8a">Para que el producto que deseas cambiar / devolver sea aprobado, deberás tomar en cuenta lo siguiente:</p>
                    <ul style="color:#8a8a8a">
                        <li>
                            Revisar que el producto esté en buenas condiciones, como se le envío o adquirió en algún Punto de Venta.
                        </li>
                        <li>Revisar que el producto mantenga el etiquetado original.</li>
                        <li>De ser posible enviar el producto con el empaque original.</li>
                        <li>Dentro del paquete, asegurate de inclur este archivo impreso junto con el producto a cambiar / devolver.</li>
                    </ul>
                </td>
                <td>
                    <p style="color:#8a8a8a"> Háznos el envío por la paquetería de tu elección a esta dirección:</p>
                    <p style="color:#8a8a8a">
                        Destinario: Muletta <br>
                        Calle: 22 de Octubre # 105 <br>
                        Colonia: Modelo <br>
                        CP: 20080 <br>
                        Ciudad: Aguascalientes <br>
                        Estado: Aguascalientes <br>
                        País: México <br>
                        Teléfono: 449 919 63 07 <br>
                    </p>
                    <p style="color:#8a8a8a">Recibido el paquete, revisaremos que cumpla con las condiciones mencionadas anteriormente. </p>
                    <p style="color:#8a8a8a">Una vez aprobado, se procederá al envío del artículo que nos estés solicitando o se generará el reembolso por el total del precio pagado por el producto en Muletta Cash; el cual podrás usar dentro de nuestro sitio web www.muletta.com.</p>
                </td>
            </tr>
        </table>

        <?php
            if (count($producto_final) > 1 && count($producto_final) < 5) {
                echo '
                <footer class="text-center">
                    <div class="linea"></div>
                    <a href="https://muletta.com" style="color:#8a8a8a;">WWW.MULETTA.COM</a>
                </footer>
                <div class="page-break"></div>';
            }else{
                echo '<hr>';
            }
        ?>

        <div class="text-center">
            <h6 style="color:#8a8a8a">POLÍTICAS DE CAMBIOS Y DEVOLUCIONES</h6>
        </div>

        <table class="table text-justify" style="font-size:10px;">
            <tr>
                <td style="width: 50%">
                    <p style="color:#8a8a8a">
                        Muletta funciona con una política de devolución bien marcada y
                        establecida en las condiciones de contratación.<br>
                        Esta política de devoluciones solamente es válido en productos
                        comprados en nuestra Tienda Online, Facebook y WhatsApp
                        Sólo se cubrirá el importe de los gastos de envío realizado en online
                        en los siguientes casos:
                    </p>

                    <ul style="color:#8a8a8a">
                        <li>Error en el Envío. </li>
                        <li>Error en el producto Enviado</li>
                        <li>Defectos de Fabricación</li>
                    </ul>
                </td>
                <td>
                    <p style="color:#8a8a8a">En el resto de casos, el cliente se hará cargo de los gastos de envío</p>
                    <p style="color:#8a8a8a">Cualquier persona puede cambiar un artículo sin utilizar dentro de los 10 días hábiles de la compra poniéndose en contacto con nuestro servicio de atención al cliente y cumpliendo el siguiente proceso.</p>
                    <p style="color:#8a8a8a">Tarifa de Envíos</p>
                    <ul style="color:#8a8a8a">
                        <li>Dentro de la Ciudad de Aguascalientes: $99.00 MN</li>
                        <li>Interior de la República: $99.00 MN</li>
                        <li>Resto del Mundo: se definirá en base a una cotización</li>
                    </ul>
                </td>
            </tr>
        </table>

        <footer id="footer" class="text-center">
            <div class="linea"></div>
            <a href="https://muletta.com" style="color:#8a8a8a;">WWW.MULETTA.COM</a>
            
            <script type="text/php">
                if ( isset($pdf) ) {
                    $pdf->page_script('
                        $font = $fontMetrics->get_font("Nunito-Italic, Helvetica, sans-serif", "normal");
                        $pdf->text(270, 730, "Pagina $PAGE_NUM de $PAGE_COUNT", $font, 10);
                    ');
                }
            </script>
        </footer>


    </body>
</html>