@extends('template.main')

@section('title', 'Inicio')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/register.css') }}">
@endsection

@section('content')
    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item txt-muletta-oro">
                    <span>Bolsa de trabajo</span>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container mb-5 g-pt-50">
        <div class="  g-max-width-445 text-center rounded mx-auto g-pa-20 ">
            <div class="  g-bg-white  mb-4">

                <header class="text-center ">
                    <h1 class="h4  g-font-weight-600 txt-muletta-gris">BOLSA DE TRABAJO</h1>
                </header>
                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
                @if ($message = Session::get('exito'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif
                <!-- Form -->
                <form class="g-py-15" method="POST" action="{{url('sendemail/sendBolsaTrabajo')}}">
                    {{ csrf_field() }}

                    <div class="row no-gutters">
                        <!-- nombre -->
                        <div class="col-12 px-5 mb-4">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="name" type="text" placeholder="NOMBRE*">
                            </div>
                        </div>

                        <!-- Correo -->
                        <div class="col-12 px-5 mb-4">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="email" type="email" placeholder="CORREO ELECTRÓNICO*">
                            </div>
                        </div>

                        <!-- Teléfono -->
                        <div class="col-12 px-5 mb-4">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="telefono" type="text" placeholder="TELÉFONO*">
                            </div>
                        </div>

                        <!-- Dirección -->
                        <div class="col-12 px-5 mb-4">
                            <div class="input-group">
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus  g-py-15 g-px-15" value="" name="direccion" type="text" placeholder="DIRECCIÓN">
                            </div>
                        </div>




                        <!-- file -->
                        <div class="col-12 px-5 mb-4">
                            <div class="input-group">
                                <label for="exampleTextarea">SUBE TU CV</label>
                                <input type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileCv" name="cv">
                            </div>
                        </div>

                        <!-- Mensaje -->
                        <div class="col-12 px-5 mb-4">
                            <label for="exampleTextarea">MENSAJE</label>
                            <div class="input-group">
                                <textarea name="message" class="form-control rounded-0 form-control-md" id="exampleTextarea" rows="6"></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12">
                            <button class="btn btn-md btn-block u-btn-primary rounded-0  mb-3" type="submit" name="enviar">ENVIAR</button>
                            <p class="g-color-gray-dark-v5 mb-0 text-center">* CAMPOS REQUERIDO </p>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection