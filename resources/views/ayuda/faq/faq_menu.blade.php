@extends('template.main')

@section('title', 'Inicio')

@section('content')
<!-- Breadcrumbs -->
<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
        <ul class="u-list-inline">
            <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
            </li>

            <li class="list-inline-item txt-muletta-oro">
                <span>Preguntas frecuentes </span>
            </li>
        </ul>
    </div>
</section>
<!-- End Breadcrumbs -->

<div class="container text-center g-py-100">
    <div class="mb-5">
        <span class="d-block g-color-gray-light-v1 g-font-size-70 g-font-size-90--md mb-4">
            <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i>
        </span>
        <h2 class="g-mb-30">PREGUNTAS FRECUENTES</h2>

        <div class="row d-flex justify-content-center">
            <div class="col-sm-3 menu-box">
                <a href="{{ Route('faq_cuenta') }}">
                    <h3 class="box-menu-title">MI CUENTA</h3>
                </a>
            </div>
            <div class="col-sm-3 menu-box">
                <a href="{{ Route('faq_pedidos') }}">
                    <h3 class="box-menu-title">PEDIDOS</h3>
                </a>
            </div>
            <div class="col-sm-3 menu-box">
                <a href="{{ Route('faq_metodo_pago') }}">
                    <h3 class="box-menu-title">MÉTODOS DE PAGO</h3>
                </a>
            </div>
            <div class="col-sm-3 menu-box">
                <a href="{{ Route('faq_proceso_envio') }}">
                    <h3 class="box-menu-title">PROCESO DE ENVIO</h3>
                </a>
            </div>
            <div class="col-sm-3 menu-box">
                <a href="{{ Route('faq_cambios') }}">
                    <h3 class="box-menu-title">CAMBIOS Y DEVOLUCIONES</h3>
                </a>
            </div>
            <div class="col-sm-3 menu-box">
                <a href="{{ Route('faq_producto') }}">
                    <h3 class="box-menu-title">PRODUCTOS</h3>
                </a>
            </div>
            <div class="col-sm-3 menu-box">
                <a href="{{ Route('faq_distribuidores') }}">
                    <h3 class="box-menu-title">DISTRIBUIDORES</h3>
                </a>
            </div>
            <div class="col-sm-3 menu-box">
                <a href="{{ Route('faq_info_gen') }}">
                    <h3 class="box-menu-title">INFORMACIÓN GENERAL</h3>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
