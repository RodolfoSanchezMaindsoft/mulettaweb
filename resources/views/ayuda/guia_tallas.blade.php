@extends('template.main')

@section('title', 'Inicio')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/register.css') }}">
@endsection

@section('content')
    <!-- Breadcrumbs -->
    <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
            <ul class="u-list-inline">
                <li class="list-inline-item g-mr-5">
                    <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                    <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                </li>
                <li class="list-inline-item txt-muletta-oro">
                    <span>Guía de Tallas </span>
                </li>
            </ul>
        </div>
    </section>
    <!-- End Breadcrumbs -->

    <div class="container mb-5 g-pt-50">
        <div class="  rounded mx-auto g-pa-20 ">
            <div class="  g-bg-white  mb-4">
                <header class="text-center ">
                    <h1 class="h4  g-font-weight-600 txt-muletta-gris ">Guía de Tallas</h1>
                </header>
        
                <div class="wpb_wrapper g-pt-50">
                    @if($contenido)
                        @foreach($contenido as $key => $cont)
                            <img class="img-fluid g-brd-around g-brd-gray-light-v2 margin-top" src="<?= 'http://creatmos.net/'.$cont->CONTENIDO_FOOTER ?>" alt="Image Description">
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection