<?php
$code = app()->isDownForMaintenance() ? 'maintenance' : $exception->getStatusCode();
?>

<html lang="{!! app()->getLocale() !!}">
@extends('template.main')

@section('title', 'Error')
<meta name="description" content="{!! trans("server-error-pages::server-error-pages.$code.description", ['domain'=> request()->getHost()]) !!}">


@section('content')
<!-- Breadcrumbs -->
<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
        <ul class="u-list-inline">
            <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-text" href="{{route('index')}}">Inicio</a>
                <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
            </li>
            
            
            <li class="list-inline-item ">
                <span>{!! trans("server-error-pages::server-error-pages.$code.title") !!}</span>
            </li>
        </ul>
    </div>
</section>
<!-- End Breadcrumbs -->

<div class="container text-center g-py-50">
    <div class="mb-5">
        <span class="d-block g-color-gray-light-v1 g-font-size-70 g-font-size-90--md mb-4">                        
            <i class="{!! trans("server-error-pages::server-error-pages.$code.icon") !!}">
            </i>
        </span>

        <h2 class="g-mb-30">
            {!! trans("server-error-pages::server-error-pages.$code.title") !!}
        </h2>
        <p class="lead text-center">{!! config('app.env') === 'production' ? trans("server-error-pages::server-error-pages.$code.description", ['domain'=> request()->getHost()]) : $exception->getMessage() ?? trans("server-error-pages::server-error-pages.$code.description", ['domain'=> request()->getHost()]) !!}
                               </p>  
        <hr class="g-brd-gray-light-v3 g-my-15">
        
        <div class="g-absolute-centered g-pt-50">
            <p class="g-font-size-18 mb-0">
           
           <a class="text-center g-color-black g-color-primary--hover g-text-no-underline--hover" href="{!! 
                      trans(\"server-error-pages::server-error-pages.$code.button.link_to\") === 'home' ? url('/') :  trans(\"server-error-pages::server-error-pages.$code.button.link_to\") === 'reload' 
                      ? url()->current() : url()->previous() !!}">←<span class="">{!! trans("server-error-pages::server-error-pages.$code.button.name") !!}</span></a>                        
        </p>
         <p class="g-font-size-18 mb-0">
          <a class="text-center g-color-black g-color-primary--hover g-text-no-underline--hover" href="https://www.muletta.com/">
                       <span class="">Prueba con la pagina previa de Muletta</span> →
              </a>
          </p>
        </div>

            <div style="font-family: Rufina;max-height: 200px;" class="g-font-size-180  g-font-weight-600er g-color-gray-light-v4">
                {!! trans("server-error-pages::server-error-pages.$code.code") !!}
            </div>

            <div class="row">
                <div class="col-12 accordeon-ctm-wraper">
                   
                    <ul class="accordion-ctm">
                        <li class="accordion-ctm-item">
                            <a href="#!">
                                {!! trans("server-error-pages::server-error-pages.$code.why.title") !!}
                                <i class=" ml-auto  fas fa-chevron-down"></i>
                            </a>
                            <div class="content">
                                <p>
                                    {!! trans("server-error-pages::server-error-pages.$code.why.description") !!}
                                </p>
                            </div>
                            
                        </li>
                        <li class="accordion-ctm-item">
                            <a href="#!">
                                {!! trans("server-error-pages::server-error-pages.$code.what_do.title") !!}
                                <i class="ml-auto  fas fa-chevron-down"></i></a>
                            <div class="content">

                                <ul>
                                    <strong>
                                        {!! trans("server-error-pages::server-error-pages.$code.what_do.visitor.title") !!}
                                    </strong>
                                    
                                    <li>
                                        {!! trans("server-error-pages::server-error-pages.$code.what_do.visitor.description", ['domain'=> request()->getHost()]) !!}
                                    </li>
                                    <br>
                                    <strong>
                                        {!! trans("server-error-pages::server-error-pages.$code.what_do.owner.title") !!}
                                    </strong>
                                    
                                    <li>
                                        {!! trans("server-error-pages::server-error-pages.$code.what_do.owner.description") !!}
                                    </li>
                                    
                                </ul>
                            </div><!-- .content -->
                        </li><!-- .accordion-ctm-item -->

                      
                    </ul><!-- .accordion-ctm -->
                </div> <!-- .col-12 accordeon-ctm-wraper -->
            </div><!-- .row -->
    </div><!-- .mb-5 -->
</div><!-- .container text-center g-py-100 -->
@endsection
