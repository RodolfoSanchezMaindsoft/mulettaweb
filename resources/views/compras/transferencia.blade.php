<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Pago en tienda</title>
<link href="<?=asset('assets/global/css/maindsoft/pago.css')?>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="whitepaper">
	<div class="Header">
	<div class="Logo_empresa">
    	<img src="<?=asset('assets/global/img/logo/logo-muletta-header.png')?>" alt="Logo">
    </div>
    <div class="Logo_paynet">
    	<div>Transferencia Interbancaría (SPEI)</div>
    </div>
    </div>
    <div class="Data">
    	<div class="Big_Bullet">
        	<span></span>
        </div>
    	<div class="col1">
        	<h3>Fecha límite de pago</h3>
            <h4>
                <?php
                    setlocale(LC_ALL,"es_ES");
                    setlocale(LC_TIME, "spanish");
                    $date = date("d-m-Y");

                    $inicio = strftime("%d de %B %Y", strtotime($date."+ 5 days"));
                    echo $inicio;

                    $hora = new DateTime("now", new DateTimeZone('America/Mexico_City'));
                    echo ' a las '.$hora->format('G:i');
                ?>
            </h4>
            <br>
            <br>
        	<h3>Beneficiario</h3>
            <h4>
                CREATMOS API
            </h4>
        </div>
        <div class="col2">
        	<h2>Total a pagar</h2>
            <h1>$<?=number_format(floatval($ordenes->TOTAL), 2, '.', '')?><small> MXN</small></h1>
            <span class="note">La comisión por recepción del pago varía de acuerdo a los términos y condiciones que cada cadena comercial establece.</span>
        </div>
    </div>
    <div class="DT-margin"></div>
    <div class="Data">
    	<div class="Big_Bullet">
        	<span></span>
        </div>
    	<div class="col1">
        	<h3>Detalles de la compra</h3>
        </div>
	</div>
    <div class="Table-Data">
    	<div class="table-row color1">
        	<div>Descripción</div>
            <span>Cargo a tienda <?=$id_venta?></span>
        </div>
    	<div class="table-row color2">
        	<div>Fecha y hora</div>
            <span>
                <?php
                    echo strftime("%A %d de %B del %Y");

                    echo ', '.$hora->format('G:i:s');
                ?>
            </span>
        </div>
    	<div class="table-row color1">
        	<div>Correo del cliente</div>
            <span><?=$perfil_usuario->EMAIL?></span>
        </div>
    	<div class="table-row color2"  style="display:none">
        	<div>&nbsp;</div>
            <span>&nbsp;</span>
        </div>
    	<div class="table-row color1" style="display:none">
        	<div>&nbsp;</div>
            <span>&nbsp;</span>
        </div>
    </div>
    <div class="DT-margin"></div>
    <div>
        <div class="Data">
            <div class="Big_Bullet">
                <span></span>
            </div>
            <div class="col1">
                <h3>Pasos para realizar el pago</h3>
            </div>
        </div>
        <div class="Table-Data">
            <div class="table-row color1">
                <div style="width: 49%;">
                    <h4>Desde BBVA Bancomer</h4>
                    <ol>
                        <li>
                            Dentro del menu de "Pagar" seleccione la opcion "De servicios" e ingrese el siguiente "Numero de convenio CIE"
                            <h5>Numero de convenio CIE: <small> <?=$charge->payment_method->agreement?> </small></h3>
                        </li>
                        <li>
                            Ingrese los datos de registro para concluir con la operación
                            <h5>Referencía: <small> <?=$charge->payment_method->clabe?> </small></h3>
                            <h5>Importe: <small> $<?=number_format(floatval($ordenes->TOTAL), 2, '.', ',')?> MXN </small></h3>
                            <h5>Concepto: <small> Compra del producto X </small></h3>
                        </li>
                    </ol>
                </div>
                <div style="width: 48%;padding-left: 8px;border-left: 6px white solid;">
                    <h4>Desde cualquier otro banco</h4>
                    <ol>
                        <li>
                            Ingresar a la seccion de transferencias y pagos o pagos a otros bancos y proporciona los datos de la transferencia
                            <h5>Beneficiario: <small> Muletta/Federico Melo </small></h3>
                            <h5>Banco destino: <small> BBVA Bancomer </small></h3>
                            <h5>Clabe: <small> <?=$charge->payment_method->clabe?> </small></h3>
                            <h5>Concepto de pago: <small> <?=$charge->payment_method->name?> </small></h3>
                            <h5>Referencia: <small> <?=$charge->payment_method->agreement?> </small></h3>
                            <h5>Importe: <small> $<?=number_format(floatval($ordenes->TOTAL), 2, '.', ',')?> MXN </small></h3>
                        </li>
                    </ol>
                </div>
                
            </div>
        </div>
    </div>

    <div class="logos-tiendas">

	    <ul>
		    <li><img src="<?=asset('assets/global/img/tarjetas/cards2.png')?>" style="margin-left: -75px;margin-top: 38px;height: 33px;"></li>
	    </ul>
        <div style="height: 90px; width: 190px; float: right; margin-top: 30px;">
	        ¿Quieres pagar en otros bancos con servicio de SPEI? visítanos en: www.openpay.mx/tiendas
        </div>

    </div>
</div>
<div style="height: 40px; width: 100%; float left;"></div>

</body>
</html>