@extends('template.main')

@section('title', 'carrito')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/checkout.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/stripe.css') }}">
@endsection

@section('content')
<input id="id_user" name="id_user" type="hidden" value="{{ $client->ID_CLIENTE }}">
<div class="loader" style="position: fixed;"></div><!-- .loader -->

<div class="row" style="margin-top: 2em;margin-right: 15em;margin-left: 15em;">

    <div class="col-8">
        <div class="col-sm-12 g-brd-bottom g-brd-gray-light-v3 txt-muletta-gris text-uppercase g-mb-50">
            <h3>MÉTODO DE PAGO</h3>
        </div>

        <div class="col-12">
            <img src="{{ asset('assets/global/img/compras/PAGOSEGURO_MULETTA.png') }}" class="pago_seguro">
        </div>

        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header header_method" id="headingOne">
                    <h2 class="mb-0 title_method">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" onclick="changeinput(2)">
                            <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                <input id="inputMercadopago" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="2" checked>
                                <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                MERCADO PAGO
                            </label>
                        </button>
                    </h2>
                    <img src="{{ asset('assets/global/img/compras/MercadoPago.png') }}" class="img_mercadopago">
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        Al dar "Finalizar compra" seras redirigido A MercadoPago para completar tu compra de forma segura
                        <br>
                        <br>
                        <span style="color: #4c4c4c;font-size: 14px; font-weight: bold; text-decoration: underline;">*Recuerda que si pagas con Oxxo o deposito bancario tienes que imprimir el comprobante.</span>
                    </div>
                </div>
            </div>
            
            <!-- <div class="card">
                <div class="card-header header_method" id="headingTree">
                    <h2 class="mb-0 title_method">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTree" aria-expanded="true" aria-controls="collapseTree" onclick="changeinput(3)">
                            <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                <input id="inputOpenPay" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="3">
                                <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                TARJETA DEBITO/CREDITO
                            </label>
                        </button>
                    </h2>
                    <img src="{{ asset('assets/global/img/compras/openpay.png') }}" class="img_openpay">
                </div>

                <div id="collapseTree" class="collapse" aria-labelledby="headingTree" data-parent="#accordionExample">
                    <div class="card-body">
                        Al dar "Finalizar compra" seras redirigido a nuestro sistema de cobro para completar tu compra de forma segura
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header header_method" id="headingFive">
                    <h2 class="mb-0 title_method">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive" onclick="changeinput(5)">
                            <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                <input id="inputOpenPayTransf" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="5">
                                <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                TRANSFERENCIA BANCARIA
                            </label>
                        </button>
                    </h2>
                    <img src="{{ asset('assets/global/img/compras/openpay.png') }}" class="img_openpay">
                </div>

                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                    <div class="card-body">
                        Al dar "Finalizar compra" proporcionaremos el numero de cuenta
                    </div>
                </div>
            </div>

            
            <div class="card">
                <div class="card-header header_method" id="headingSix">
                    <h2 class="mb-0 title_method">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix" onclick="changeinput(6)">
                            <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                <input id="inputOpenPayPayNet" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="6">
                                <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                PAGO EN TIENDA
                            </label>
                        </button>
                    </h2>
                    <img src="{{ asset('assets/global/img/compras/openpay.png') }}" class="img_openpay">
                </div>

                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                    <div class="card-body">
                        Al dar "Finalizar compra" proporcionaremos el numero de cuenta
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header header_method" id="headingfour">
                    <h2 class="mb-0 title_method">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" onclick="changeinput(4)">
                            <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                <input id="inputWebPay" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="4">
                                <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                    <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                WEBPAY
                            </label>
                        </button>
                    </h2>
                    <img src="{{ asset('assets/global/img/index/03_Logo_webpay_plus-300px.png') }}" class="img_openpay">
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExample">
                    <div class="card-body text-center">
                        Al dar "Finalizar compra" seras redirigido a WebPay para completar tu compra  de forma segura.

                        <input type="hidden" id="IDV">
                        <input type="hidden" id="IDD">

                        <br>
                        <br>
                        </div>
                </div>
            </div> -->

        </div>
        <div style="display: block !important;">
            <img src="{{ asset('assets/global/img/compras/pagoseguro_gris.png') }}" class="img_pago_seguro">
            &nbsp;DUDAS Y ACLARACIONES VÍA WHATSAPP AL &nbsp;
            <img src="{{ asset('assets/global/img/compras/whatsapp.png') }}" class="img_whatsapp">
            &nbsp;449 919 63 07
        </div>
    </div>

    <div class="col-4">
        <!-- Accordion -->
        <div id="accordion-05" class="mb-4" role="tablist" aria-multiselectable="true">
            <div id="accordion-05-heading-05" class="g-brd-y g-brd-gray-light-v2 py-3" role="tab">
                <h5 class="g-font-weight-400 g-font-size-default mb-0">
                    <a class="g-color-gray-dark-v4 g-text-underline--none--hover " href="#accordion-05-body-05" data-toggle="collapse" data-parent="#accordion-05" aria-expanded="false" aria-controls="accordion-05-body-05">
                        {{ count($productos) }} productos
                        <span class="ml-3 fa fa-angle-down"></span>
                    </a>
                </h5>
            </div>

            <div id="accordion-05-body-05" class="collapse show" role="tabpanel" aria-labelledby="accordion-05-heading-05">
                <div class="g-py-15">
                    <ul class="list-unstyled mb-3">
                        @foreach ($productos as $producto)
                            <!-- Product -->
                            <li class="d-flex justify-content-start mb-3">
                                <img class="g-width-100  mr-3" src="{{ $producto->IMAGEN }}" alt="Image Description">
                                <div class="d-block">
                                    <h4 class="h6 g-color-black">{{ $producto->DESCRIPCION }}</h4>
                                    <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_4 mb-1">
                                        @if ($producto->TALLA)
                                            <li>Talla: {{ $producto->TALLA }}</li>
                                        @endif
                                        @if ($producto->ID_MODELO)
                                            <li>Modelo: {{ $producto->ID_MODELO }}</li>
                                        @endif
                                    </ul>
                                    <span class="d-block g-color-black g-font-weight-400">{{ $producto->CANTIDAD_VENDIDA }}
                                        x $ {{ $producto->PRECIO_ORIGINAL ? $producto->PRECIO_ORIGINAL : $producto->PRECIO_SIN_DESC }}
                                    </span>

                                </div>
                            </li>
                            <!-- End Product -->
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Accordion -->

        <!-- Order Summary -->
        <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
            <div class="g-brd-bottom g-brd-gray-light-v3 g-mb-15">
                <h4 class="h6 text-uppercase mb-3">
                    Resumen de la orden
                    <br>
                    <input type="hidden" id="id_venta" value="{{ $id_venta }}">
                    Id venta: {{ $id_venta }}
                    <br>
                    No. ticket: {{ $ordenes->NO_TICKET }}
                </h4>
            </div>

            <div class="d-flex justify-content-between mb-2">
                <span class="g-color-black">Subtotal</span>
                <span class="g-color-black g-font-weight-300">${{ number_format($ordenes->SUBTOTAL, 2) }}</span>
            </div>

            <div class="d-flex justify-content-between mb-2">
                <span class="g-color-black">Envío</span>
                <span class="g-color-black g-font-weight-300 envio_costo">${{ number_format($ordenes->ENVIO, 2) }}</span>
            </div>

            <div class="d-flex justify-content-between mb-2">
                <span class="g-color-black">Descuento</span>
                <span class="g-color-black g-font-weight-300">${{ $ordenes->DESCUENTO }}</span>
            </div>

            @if($ordenes->REGALO > 0)
                <div class="d-flex justify-content-between mb-2">
                    <span class="g-color-black">Regalo</span>
                    <span class="g-color-black g-font-weight-300">${{ $ordenes->REGALO }}</span>
                </div>
            @endif

            <div class="d-flex justify-content-between">
                <span class="g-color-black">Total</span>
                <input id="input_total" type="hidden" value="<?= $ordenes->TOTAL ?>">
                <span class="g-color-black g-font-weight-300">$<span class="total">{{ number_format($ordenes->TOTAL, 2) }}</span> </span>
            </div>

        </div>
        <!-- End Order Summary -->

        <input id="url_mercado_pago" type="hidden" value="{{ Route('initMercadoOrderReintentar', ['ID_VENTA' => $id_venta]) }}">
        <button class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover" id="fwebpay_url">Finalizar compra</button>
        <button id="btn_finalizar" onclick="MercadoPago()" class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover" style="display:none;">Finalizar compra</button>
    </div>
    
</div>

<div id="error-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <span id="url_pedidos" style="display: none;" >{{ route('editar-cuenta') }} </span>
                </div>
                
                <div>
                    <h1 class="text-center">OOPS!! <img src=""></h1>
                    <h2 class="text-center">ALGO SALIO MAL</h2>

                    <h4 class="text-center">NO TE PREOCUPES, NOSOTROS TE PODEMOS AYUDAR.</h4>

                    <P>La plataforma no pudo procesar tu pago por el siguiente motivo:</P>

                    <div class="text-center" id="error_tarjeta">
                        <img src="{{ asset('assets/global/img/compras/Incorrecto_.png') }}"  height="55px" style="margin-right: 5px;">
                        <span id="message-error" style="vertical-align: middle;"></span>
                    </div>

                    <p class="razon-why" id="errores_principales">
                        <span>
                            <img src="{{ asset('assets/global/img/compras/Incorrecto_.png') }}" width="50px" style="margin-right: 5px;"> Información incorrecta.
                        </span>
                        <br>
                        <span>
                            <img src="{{ asset('assets/global/img/compras/Billete_.png') }}" width="45px" style="margin-right: 10px;"> Fondos insuficientes.
                        </span>
                        <br>
                        <span>
                            <img src="{{ asset('assets/global/img/compras/Validacion_.png') }}" width="55px" style="margin-right: 2px;"> Validación de seguridad.
                        </span>
                    </p>

                    <p>Intenta hacer tu pedido con otro metodo de pago otra tarjeta ó validando que tus datos sean correctos </p>

                    <p>Ayuda personalizada vía <img src="{{ asset('assets/global/img/compras/whatsapp.png') }}" class="img_whatsapp"> Whatsapp (449 919 63 07), escribenos y te ayudaremos a completar tu compra</p>

                    <a onclick="location.reload(true);" class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover">Reintentar Pago</a>

                </div>

            </div>
        </div>
    </div>
</div>

<div id="security-modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="security-body" class="modal-body">

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('assets/global/js/maindsoft/reintentar.js') }}"></script>
@endsection