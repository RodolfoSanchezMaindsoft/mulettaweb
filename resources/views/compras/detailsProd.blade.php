@extends('template.main')

@section('title', 'Detalles de la compra')

@section('css')
   
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/details-ventas.css') }}">
    
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-py-30 g-px-70--md g-px-35">
        <ul class="u-list-inline">
            <div class="align-self-left ">
                <ul class="u-list-inline">
                    <li class="list-inline-item g-mr-5">
                        <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover" href="{{route('index')}}">Inicio</a>
                        <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                    </li>
                    @if($perfil_usuario)
                        <li class="list-inline-item">
                            <a class="u-link-v5 g-color-text g-color-black--active g-color-black--focus g-color-black--hover"
                                href="{{ route('editar-cuenta') }}">Mis Pedidos</a>
                            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
                        </li>
                    @endif
                    <li class="list-inline-item g-mr-5 g-color-primary">
                        <span>Detalles de la compra</span>
                    </li>
                </ul>
            </div>
        </ul>
    </section>
    <!-- Fin Pestañas Navegación -->

    <!-- Detalles -->
    <div class="general">

        <div class="row g-brd-bottom g-brd-gray-light-v3">
            <div class="col-md-12">
                <h2>GRACIAS POR TU COMPRA</h2>
            </div>
        </div>

        <div class="row" style="margin-top: 40px; margin-bottom: 40px;">
            <div class="col-12">
            @if (!$ordenes->NO_TICKET)
                <h4 class="float-left g-color-muletta-gray-dark"> Pedido: #{{ $ID_VENTA }} </h4>
            @else
                <h4 class="float-left g-color-muletta-gray-dark"> <small> Pedido: #{{ $ID_VENTA }} </small>
                <br>
                Ticket No: {{ $ordenes->NO_TICKET }} </h4>
            @endif
            </div>
        </div>

        <div class="row g-brd-bottom g-brd-gray-light-v3 header-desktop" style="margin-bottom: 3em;">
            <div class="col-8 g-mb-30">
            <h5 class="g-color-muletta-gray-dark"> DETALLES DEL PEDIDO </h5>
            </div>
            <div class="col-4 g-mb-30">
                <h5 class="g-color-muletta-gray-dark"> TU PEDIDO SERÁ ENVIADO A: </h5>
            </div>
        </div>

        <div class="row g-brd-bottom g-brd-gray-light-v3 header-movil" style="margin-bottom: 3em;">
            <div class="col-md-8 g-mb-30">
                <h5 class="g-color-muletta-gray-dark"> DETALLES DEL PEDIDO </h5>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 g-mb-30">
                    @foreach ($productos as $producto)
                        <!-- Movil -->
                        <div class="w-100 g-py-10 g-brd-bottom g-brd-gray-light-v3 table-movil">
                            <div class="row">
                                <div class="col-4">
                                    @if(substr($producto->IMAGEN, 0, 4) != 'http')
                                        <img class="img-fluid" src="{{ 'http://creatmos.net'.str_replace('~', '', str_replace(' ', '%20', $producto->IMAGEN) ) }}" alt="Producto" width="100px">
                                    @else
                                        <img class="img-fluid" src="{{ str_replace(' ', '%20', $producto->IMAGEN) }}" alt="Producto" width="100px">
                                    @endif
                                </div>
                                <div class="col-8" style="float:right;">
                                    <div class="g-py-10">
                                        <h5 class="g-color-muletta-gray-theme muletta-tittle-featured-products" style="color: #091b34; margin-top: -11px; font-weight: bold;">{{ $producto->DESCRIPCION }}</h5>
                                        <br>
                                        <h5 class="g-color-muletta-gray-theme  muletta-tittle-featured-products" style="color: #091b34; margin-top: -29px;">{{ $producto->ID_MODELO }}</h4>
                                    </div>
                                </div>
                            </div>
                                    
                            <table class="table text-center">
                                <thead>
                                    <th>Talla</th>
                                    <th>Cant</th>
                                    <th>Precio</th>
                                    <th>Desc</th>
                                    <th>Total</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span class="g-font-size-12 g-color-muletta-gray-light">{{ $producto->DESCRIPCION_TALLA }}</span></td>
                                        <td>{{ $producto->CANTIDAD_VENDIDA }}</td>
                                        <td> $ {{ $producto->PRECIO_SIN_DESC }}</td>
                                        <td><span style="color:red;">$ {{ $producto->PRECIO_SIN_DESC - $producto->PRECIO_ORIGINAL }}<span></td>
                                        <td><span class="g-font-size-12 g-color-muletta-gray-light">$ {{ number_format($producto->PRECIO * $producto->CANTIDAD_VENDIDA, 2) }}</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- End Movil -->

                        <!-- Item-->
                        <div class="w-100 g-py-10 g-brd-bottom g-brd-gray-light-v3 table-desktop">
                            <div class="row">
                                <div class="col-md-2">
                                    @if(substr($producto->IMAGEN, 0, 4) != 'http')
                                        <img class="img-fluid" src="{{ 'http://creatmos.net'.str_replace('~', '', str_replace(' ', '%20', $producto->IMAGEN) ) }}" alt="Producto" width="100px">
                                    @else
                                        <img class="img-fluid" src="{{ str_replace(' ', '%20', $producto->IMAGEN) }}" alt="Producto" width="100px">
                                    @endif
                                </div>
                                <div class="col-md-10">
                                    <div class="g-py-10">
                                        <h5 class="g-color-muletta-gray-theme muletta-tittle-featured-products" style="color: #091b34; margin-top: -11px; font-weight: bold;">{{ $producto->DESCRIPCION }}</h5>
                                        <br>
                                        <h5 class="g-color-muletta-gray-theme  muletta-tittle-featured-products" style="color: #091b34; margin-top: -29px;">{{ $producto->ID_MODELO }}</h4>
                                    </div>

                                    <div class="row col-12">
                                        <div class="col-2">
                                            <p class="g-color-muletta-gray-dark g-font-size-14 text-center">
                                                <b>Talla</b>
                                                <br>
                                                <span class="g-font-size-12 g-color-muletta-gray-light">{{ $producto->DESCRIPCION_TALLA }}</span>
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            <p class="g-color-muletta-gray-dark g-font-size-14 text-center">
                                                <b>Cantidad</b>
                                                <br>
                                                {{ $producto->CANTIDAD_VENDIDA }}
                                            </p>
                                        </div>
                                        <div class="col-2">
                                            <p class="g-color-muletta-gray-dark g-font-size-14 text-center">
                                                <b>Precio</b>
                                                <br>
                                                $ {{ $producto->PRECIO_SIN_DESC }}
                                            </p>
                                        </div>
                                        <div class="col-3">
                                            <p class="g-color-muletta-gray-dark g-font-size-14 text-center">
                                                <b>Descuento</b>
                                                <br>
                                                <span style="color:red;">$ {{ $producto->PRECIO_SIN_DESC - $producto->PRECIO_ORIGINAL }}<span>
                                            </p>
                                        </div>
                                        <div class="col-2">
                                            <p class="g-color-muletta-gray-dark g-font-size-14 text-center">
                                                <b>Total</b>
                                                <br>
                                                <span class="g-font-size-12 g-color-muletta-gray-light">$ {{ number_format($producto->PRECIO * $producto->CANTIDAD_VENDIDA, 2) }}</span>
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- End Item-->
                    @endforeach

                <div class="g-brd-bottom g-brd-gray-light-v3 g-pb-30 g-mb-30">
                    <h5 class="g-font-weight-400 g-font-size-default mb-0" style="margin-top: 2em; margin-left: 1em;">
                        <span class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#accordion-05-body-05" data-toggle="collapse" data-parent="#accordion-05" aria-expanded="false" aria-controls="accordion-05-body-05"> {{ count($productos) }} productos
                        </span>
                    </h5>
                </div>

                <div class="g-brd-gray-light-v3 g-pb-30 g-mb-30">
                    @if($ordenes)
                        <!-- Order Summary -->
                        <div class="g-pa-20 g-pb-50 mb-4 total-movil" style="width: 25em; float: right;">

                            <div class="d-flex justify-content-between mb-2">
                                <span class="g-color-black">TOTAL PRODUCTOS</span>
                                <span class="g-color-black g-font-weight-300">${{ number_format($ordenes->SUBTOTAL, 2) }}</span>
                            </div>

                            <div class="g-brd-bottom d-flex justify-content-between mb-2 g-brd-bottom g-brd-gray-light-v3">
                                <span class="g-color-black">ENVÍO 2 A 5 DÍAS</span>
                                <span class="g-color-black g-font-weight-300">${{ number_format($ordenes->ENVIO, 2) }}</span>
                            </div>

                            <div class="g-brd-bottom d-flex justify-content-between mb-2 g-brd-bottom g-brd-gray-light-v3">
                                <span class="g-color-black">CÓDIGO DE DESCUENTO</span>
                                <span class="g-color-black g-font-weight-300">${{ number_format($ordenes->DESCUENTO, 2) }}</span>
                            </div>

                            <div class="g-brd-bottom d-flex justify-content-between mb-2 g-brd-bottom g-brd-gray-light-v3">
                                <span class="g-color-black">PACKING DE REGALO</span>
                                <span class="g-color-black g-font-weight-300">${{ number_format($ordenes->REGALO, 2) }}</span>
                            </div>

                            <div class="d-flex justify-content-between">
                                <span class="g-color-black">IMPORTE TOTAL</span>
                                <span class="g-color-black g-font-weight-300">${{ number_format($ordenes->SUBTOTAL + $ordenes->ENVIO - $ordenes->DESCUENTO + $ordenes->REGALO, 2) }}</span>
                            </div>

                        </div>
                        <!-- End Order Summary -->
                    @endif
                    <!-- <div class="text-right" id="Paypal">
                        <button class="btn u-btn-primary g-font-size-13 text-uppercase g-px-40 g-py-15" type="submit">Realizar pago</button>
                    </div> -->
                </div>
            </div>

            <div class="row g-brd-bottom g-brd-gray-light-v3 header-movil" style="margin-bottom: 3em;">
                <div class="col-md-4 g-mb-30">
                    <h5 class="g-color-muletta-gray-dark"> TU PEDIDO SERÁ ENVIADO A: </h5>
                </div>
            </div>

            <div class="col-md-4 g-mb-30" style="background-color: #f2f2f2;padding-top: 15px;">
                <div class="row">
                    <div class="col-12">
                        <span> {{ $ordenes->NOMBRE }} </span>
                        <br>
                        <span> {{ $ordenes->EMAIL }} </span>
                        <br>
                        <span> {{ $ordenes->TELEFONO }} </span>
                        <br>
                        <span>
                            {{ $ordenes->CALLE }} {{ $ordenes->NUM_INT }} {{ $ordenes->NUM_EXT }}
                        </span>
                        <br>
                        <span>
                            {{ $ordenes->COLONIA }} C.P.{{ $ordenes->CODIGO }}
                        </span>
                        <br>
                        <span>
                            {{ $ordenes->MUNICIPIO }}, {{ $ordenes->ESTADO }}
                        </span>
                    </div>

                    <div class="col-12">
                        <div class="w-100 g-py-10 g-brd-bottom g-brd-gray-light-v3" style="padding-bottom: 0rem !important; margin-bottom: 15px; margin-top: 7px;">
                            <h5>MÉTODO DE PAGO:</h5>
                        </div>
                        <span> {{ $ordenes->METODO_PAGO }} </span>
                    </div>

                    <div class="col-12">
                        <div class="w-100 g-py-10 g-brd-bottom g-brd-gray-light-v3" style="padding-bottom: 0rem !important; margin-bottom: 15px; margin-top: 7px;">
                            <h5>ENTREGA:</h5>
                        </div>
                        <p> Envío de 2 a 5 días hábiles</p>
                        @if($ordenes->NOMBRE_PAQUETERIA && $ordenes->NO_RASTREO)
                            <p> Paquetería: {{ $ordenes->NOMBRE_PAQUETERIA }} </p>
                            <p> No de rastreo {{ $ordenes->NO_RASTREO }} </p>
                            <a class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 mb-4" href="{{ $ordenes->URL_RASTREO }}" target="_blank"> Rastrear pedido </a>
                        @endif
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Fin Detalles -->
@endsection