@extends('template.main')

@section('title', 'carrito')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/tarjetas.css') }}">

    @if(isset($errorMessage))
        <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/checkout.css') }}">
    @endif
@endsection

@section('content')
    <div class="payment">
        <form action="{{ Route('realizarPago', ['ID_VENTA' => $id_venta, 'ID_CLIENTE' => $id_client]) }}" class="form-payment" id="payment-form" method="POST">
            {{ csrf_field() }}
            <div class="payment-header">
                <h2>Tarjeta de crédito o débito</h2>
                <input type="hidden" name="token_id" id="token_id">
                <input type="hidden" name="device_id" id="device_id">
                <input type="hidden" id="client_id" value="{{ $id_client }}">
            </div>

            <div class="payment-content">
                <div class="row card-exp">
                    <div class="col-12 col-md-4 credit">
                        <h4>Tarjetas de crédito</h4>
                        <img src="{{ asset('assets/global/img/tarjetas/cards1.png') }}" style="width: 100%;">
                    </div>
                    <div class="col-12 col-md-8 debit">
                        <h4>Tarjetas de débito</h4>
                        <img src="{{ asset('assets/global/img/tarjetas/cards2.png') }}" style="width: 100%;">
                    </div>
                </div>

                <div class="row card-exp">
                    <div class="col-12 col-md-6">
                        <label class="d-block g-color-text mb-1">
                            <h4>Nombre del titular</h4>
                        </label>
                        <input type="text" class="form-control" name="nombre" required placeholder="Como aparece en la tarjeta" autocomplete="off" data-openpay-card="holder_name">
                        <span class="text-danger">* Si el nombre no coincide con la tarjeta, se rechazará el pago</span>
                    </div>
                    <div class="col-12 col-md-6">
                        <label class="d-block g-color-text mb-1">
                            <h4>Número de tarjeta</h4>
                        </label>
                        <input type="number" class="form-control" name="tarjeta" onKeyDown="if(this.value.length==16 && event.keyCode!=8 && event.keyCode!=9) return false;" required autocomplete="off" data-openpay-card="card_number">
                    </div>
                </div>

                <div class="row card-exp">
                    <div class="col-12 col-md-6">
                        <label class="d-block g-color-text mb-1">
                            <h4>Fecha de expiración</h4>
                        </label>
                        <div class="row">
                            <div class="col-6">
                                <input type="number" class="form-control" name="mes" onKeyDown="if(this.value.length==2 && event.keyCode!=8 && event.keyCode!=9) return false;" required placeholder="Mes" data-openpay-card="expiration_month">
                            </div>
                            <div class="col-6">
                                <input type="number" class="form-control" name="anio" onKeyDown="if(this.value.length==2 && event.keyCode!=8 && event.keyCode!=9) return false;" required placeholder="Año" data-openpay-card="expiration_year">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 cvv">
                        <label class="d-block g-color-text mb-1">
                            <h4>Código de seguridad</h4>
                        </label>
                        <div class="row">
                            <div class="col-5">
                                <input type="number" class="form-control" name="cvv" onKeyDown="if(this.value.length==4 && event.keyCode!=8 && event.keyCode!=9) return false;" required placeholder="3 dígitos" autocomplete="off" data-openpay-card="cvv2">
                            </div>
                            <div class="col-7">
                                <img src="{{ asset('assets/global/img/tarjetas/cvv.png') }}" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row card-exp">
                    <div class="col-6 col-md-3 offset-md-6 credit">
                        Transacciones realizadas vía:
                        <br>
                        <img src="{{ asset('assets/global/img/tarjetas/openpay.png') }}" alt="">
                    </div>
                    <div class="col-6 col-md-3 openpay">
                        <img src="{{ asset('assets/global/img/tarjetas/security.png') }}" alt="">
                        <span style="font-size:10px;">Tus pagos se realizan de forma segura con encriptación de 256 bits</span>
                    </div>
                </div>
            </div>

            <div class="payment-footer">
                <button class="btn btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 ml-auto margin-top" id="pay-button" type="button">Pagar</button>
            </div>

        </form>
    </div>

    @if(isset($errorMessage))

        <div id="error-modal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="modal-header">
                            <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            <span id="url_pedidos" style="display: none;" >{{ route('editar-cuenta') }} </span>
                        </div>
                        
                        <div>
                            <h1 class="text-center">OOPS!! <img src=""></h1>
                            <h2 class="text-center">ALGO SALIO MAL</h2>

                            <h4 class="text-center">NO TE PREOCUPES, NOSOTROS TE PODEMOS AYUDAR.</h4>

                            <P>La plataforma no pudo procesar tu pago por el siguiente motivo:</P>

                            <div class="text-center" id="error_tarjeta">
                                <img src="{{ asset('assets/global/img/compras/Incorrecto_.png') }}"  height="55px" style="margin-right: 5px;">
                                <span id="message-error" style="vertical-align: middle;">
                                    {{ $errorMessage }}
                                </span>
                            </div>

                            <p>Intenta hacer tu pedido con otro metodo de pago otra tarjeta ó validando que tus datos sean correctos </p>

                            <p>Ayuda personalizada vía <img src="{{ asset('assets/global/img/compras/whatsapp.png') }}" class="img_whatsapp"> Whatsapp (449 919 63 07), escribenos y te ayudaremos a completar tu compra</p>

                            <a onclick="closeModal()" class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover">Reintentar Pago</a>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('script')
    <script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>
    <script type='text/javascript' src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>
    <script src="{{ asset('assets/global/js/maindsoft/tarjetas.js') }}"></script>
@endsection