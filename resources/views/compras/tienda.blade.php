<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Pago en tienda</title>
<link href="<?=asset('assets/global/css/maindsoft/pago.css')?>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="whitepaper">
	<div class="Header">
	<div class="Logo_empresa">
    	<img src="<?=asset('assets/global/img/logo/logo-muletta-header.png')?>" alt="Logo">
    </div>
    <div class="Logo_paynet">
    	<div>Servicio a pagar</div>
    	<img src="<?=asset('assets/global/img/images/paynet_logo.png')?>" alt="Logo Paynet">
    </div>
    </div>
    <div class="Data">
    	<div class="Big_Bullet">
        	<span></span>
        </div>
    	<div class="col1">
        	<h3>Fecha límite de pago</h3>
            <h4>
                <?php
                    setlocale(LC_ALL,"es_ES");
                    setlocale(LC_TIME, "spanish");
                    $date = date("d-m-Y");

                    $inicio = strftime("%d de %B %Y", strtotime($date."+ 5 days"));
                    echo $inicio;

                    $hora = new DateTime("now", new DateTimeZone('America/Mexico_City'));
                    echo ' a las '.$hora->format('G:i');
                ?>
            </h4>
            <img width="300" src="<?=$charge->payment_method->barcode_url?>" alt="Código de Barras">
        	<center><span><?=$charge->payment_method->reference?></span></center>
            <small>En caso de que el escáner no sea capaz de leer el código de barras, escribir la referencia tal como se muestra.</small>

        </div>
        <div class="col2">
        	<h2>Total a pagar</h2>
            <h1>$<?=number_format(floatval($ordenes->TOTAL), 2, '.', '')?><small> MXN</small></h1>
            <span class="note">La comisión por recepción del pago varía de acuerdo a los términos y condiciones que cada cadena comercial establece.</span>
        </div>
    </div>
    <div class="DT-margin"></div>
    <div class="Data">
    	<div class="Big_Bullet">
        	<span></span>
        </div>
    	<div class="col1">
        	<h3>Detalles de la compra</h3>
        </div>
	</div>
    <div class="Table-Data">
    	<div class="table-row color1">
        	<div>Descripción</div>
            <span>Cargo a tienda <?=$id_venta?></span>
        </div>
    	<div class="table-row color2">
        	<div>Fecha y hora</div>
            <span>
                <?php
                    echo strftime("%A %d de %B del %Y");

                    echo ', '.$hora->format('G:i:s');
                ?>
            </span>
        </div>
    	<div class="table-row color1">
        	<div>Correo del cliente</div>
            <span><?=$perfil_usuario->EMAIL?></span>
        </div>
    	<div class="table-row color2"  style="display:none">
        	<div>&nbsp;</div>
            <span>&nbsp;</span>
        </div>
    	<div class="table-row color1" style="display:none">
        	<div>&nbsp;</div>
            <span>&nbsp;</span>
        </div>
    </div>
    <div class="DT-margin"></div>
    <div>
        <div class="Big_Bullet">
        	<span></span>
        </div>
    	<div class="col1">
        	<h3>Como realizar el pago</h3>
            <ol>
            	<li>Acude a cualquier tienda afiliada</li>
                <li>Entrega al cajero el código de barras y menciona que realizarás un pago de servicio Paynet</li>
                <li>Realizar el pago en efectivo por $9,000.00 MXN </li>
                <li>Conserva el ticket para cualquier aclaración</li>
            </ol>
            <small>Si tienes dudas comunícate a NOMBRE TIENDA al teléfono TELEFONO TIENDA o al correo CORREO SOPORTE TIENDA</small>
        </div>
    	<div class="col1">
        	<h3>Instrucciones para el cajero</h3>
            <ol>
            	<li>Ingresar al menú de Pago de Servicios</li>
                <li>Seleccionar Paynet</li>
                <li>Escanear el código de barras o ingresar el núm. de referencia</li>
                <li>Ingresa la cantidad total a pagar</li>
                <li>Cobrar al cliente el monto total más la comisión</li>
                <li>Confirmar la transacción y entregar el ticket al cliente</li>
            </ol>
            <small>Para cualquier duda sobre como cobrar, por favor llamar al teléfono +52 (55) 5351 7371 en un horario de 8am a 9pm de lunes a domingo</small>
        </div>
    </div>

    <div class="logos-tiendas">

	    <ul>
		    <li><img src="<?=asset('assets/global/img/images/01.png')?>" width="100" height="50"></li>
		    <li><img src="<?=asset('assets/global/img/images/02.png')?>" width="100" height="50"></li>
		    <li><img src="<?=asset('assets/global/img/images/03.png')?>" width="100" height="50"></li>
		    <li><img src="<?=asset('assets/global/img/images/04.png')?>" width="100" height="50"></li>
		    <li><img src="<?=asset('assets/global/img/images/05.png')?>" width="100" height="50"></li>
		    <li><img src="<?=asset('assets/global/img/images/06.png')?>" width="100" height="50"></li>
		    <li><img src="<?=asset('assets/global/img/images/07.png')?>" width="100" height="50"></li>
		    <li><img src="<?=asset('assets/global/img/images/08.png')?>" width="100" height="50"></li>
	    </ul>
        <div style="height: 90px; width: 190px; float: right; margin-top: 30px;">
	        ¿Quieres pagar en otras tiendas? visítanos en: www.openpay.mx/tiendas
        </div>

    </div>
</div>
<div style="height: 40px; width: 100%; float left;"></div>

</body>
</html>