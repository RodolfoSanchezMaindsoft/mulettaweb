<?php
    $virtualMoney = session()->get('virtualMoney');
    if ($virtualMoney) {
        $virtualMoney = $virtualMoney['virtualMoney'];
    }

    $min_envio = session()->get('min_envio');
?>

@extends('template.main')

@section('title', 'carrito')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/desc-producto.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/checkout.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/stripe.css') }}">
@endsection

@section('content')
    <!-- Pestañas Navegación -->
    <section class="g-py-30 g-px-70--md g-px-35">
        <ul class="u-list-inline">
            <li class="list-inline-item g-mr-5">
                <a class="u-link-v5muletta-tittle-tag g-color-muletta-gray-light text-uppercase g-color-primary--hover g-text-underline--none--hover" href="{{route('index')}}">Volver</a>
            </li>
        </ul>
    </section>
    <!-- Fin Pestañas Navegación -->

    <input type="hidden" id="urlVerificar" value="<?=Route('verificar', 1)?>">
    <input type="hidden" id="costo_envio" value="<?=session()->get('envio')?>">

    <div class="container g-pt-100 g-pb-70">
        <!-- START: CHECKOUT FORM -------------------------------------------------------------------->
        <form class="js-validate js-step-form" id="formPago" onsubmit="return formSubmit(this);" action="{{ Route('initMercadoOrder',['ID_CLIENTE' => '', 'EMAIL' => '']) }}" method="post" data-progress-id="#stepFormProgress" data-steps-id="#stepFormSteps">
            {{ csrf_field() }}
            <meta name="csrf-token" content="{{ csrf_token() }}">

            <div class="g-mb-100">
                <!-- Step Titles -->
                <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
                    <li id="firstStep" class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                        <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-primary g-color-primary g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                            <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
                            <i class="fa fa-check g-show-check"></i>
                        </span>
                        <h4 class="g-font-size-16 text-uppercase mb-0">Carrito de compra</h4>
                    </li>

                    <li id="secondStep" class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
                        <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                            <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
                            <i class="fa fa-check g-show-check"></i>
                        </span>
                        <h4 class="g-font-size-16 text-uppercase mb-0">Datos de envío</h4>
                    </li>

                    <li id="finaltStep" class="col-3 list-inline-item">
                        <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                            <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
                            <i class="fa fa-check g-show-check"></i>
                        </span>
                        <h4 class="g-font-size-16 text-uppercase mb-0">Pago</h4>
                    </li>
                </ul>
                <!-- End Step Titles -->
            </div><!-- .g-mb-100 -->

            <div class="loader" style="position: fixed;"></div><!-- .loader -->

            <!-- START: Paso 01 - Shopping Cart -------------------------------------------------------------------->
            <div id="stepFormSteps">

                <div id="step1" class="active">
                    <div class="row">
                        <div class="col-sm-12 g-brd-bottom g-brd-gray-light-v3 txt-muletta-gris text-uppercase g-mb-50">
                            <h3>PRODUCTO</h3>
                        </div>
                        <div class="col-md-8 g-mb-30">
                            <!-- Products Block -->
                            <div class="g-overflow-x-scroll g-overflow-x-visible--lg">
                                @if($productos)
                                <table class="text-center w-100" id="productTable">

                                    <thead class="h6  g-brd-gray-light-v3 txt-muletta-gris text-uppercase">
                                        <tr>
                                            <th class="g-font-weight-700 text-left g-pb-20">Producto</th>
                                            <th class="g-font-weight-700 g-width-50 g-pb-20">Cantidad</th>
                                            <th class="g-font-weight-700 g-width-130 g-pb-20">Total</th>
                                            <th class="g-font-weight-700 g-width-130 g-pb-20">Borrar</th>
                                            <th></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($productos as $key => $producto)
                                            <!-- Item Picture & description-->
                                            <?php
                                                $existe = $producto['existencia'] - $producto['qty'];
                                            ?>
                                            <tr class="g-brd-bottom g-brd-gray-light-v3 {{ $existe < 0 ? 'disabled' : ''}} product-cart-row">
                                                <td class="text-left g-py-25">
                                                    <input class="existencia" type="hidden" value="{{ $existe }}">
                                                    <img class="d-inline-block g-width-100 mr-4" src="{{ str_replace(' ', '%20', $producto['image']) }}" alt="Producto">
                                                    <div class="d-inline-block align-middle">
                                                        <h4 class="txt-muletta-oro product-cart-name">{{ $producto['name'] }}</h4>
                                                        <ul class="list-unstyled  g-font-size-12 g-line-height-1_6 mb-0">
                                                            <li>ID: <span class="product-cart-id">{{ $producto['id'] }}</span></li>
                                                            <li>Talla: <span class="product-cart-size">{{ $producto['talla'] }}</span></li>
                                                            <li style="display:none">Categoria: <span class="product-cart-category">{{ $producto['category'] }}</span></li>
                                                            <li>Precio: $ <span class="product-cart-price">{{ number_format($producto['price'], 2) }}</span></li>
                                                            @if($existe < 0)
                                                                <li style="color: red;">(Producto sin existencia)</li>
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </td>
                                                <!-- Item QTY-->
                                                <td>
                                                    <div class="js-quantity input-group u-quantity-v1 g-width-150 g-brd-primary--focus">
                                                        <i class="js-minus g-color-gray g-color-primary--hover fa fa-minus g-ml-25" onclick=" changedown( {{ $key }} )"></i>
                                                        <input class="js-result form-control text-center g-font-size-13 rounded-0 g-pa-0 g-brd-none product-cart-quantity" type="text" value="{{ $producto['qty'] }}" readonly style="width: 50% !important;">
                                                        <i class="js-plus g-color-gray g-color-primary--hover fa fa-plus g-mr-25 {{ $existe < 0 ? 'disabled' : ''}}" onclick=" changeup( {{ $key }}, {{ $producto['existencia'] }}, {{ $producto['qty'] }} )"></i>
                                                    </div>
                                                </td>

                                                <td class="text-center g-color-black">
                                                    <span class="g-color-gray-dark-v2 g-font-size-13 mr-4">
                                                        $ {{ number_format($producto['price'] * $producto['qty'], 2) }}
                                                    </span>
                                                </td>
                                                <td class="text-center g-color-black">
                                                    <span class=" g-color-gray-dark-v4 g-color-black--hover g-cursor-pointer g-font-size-20" onclick=" changedelete( {{ $key }} )">
                                                        <i class="mt-auto fa fa-trash"></i>
                                                    </span>
                                                </td>
                                            </tr>
                                            <!-- End Item-->
                                        @endforeach
                                    </tbody>
                                </table>
                                @endIf
                            </div>
                            <!-- End Products Block -->
                        </div>

                        <div class="col-md-4 g-mb-30">
                            <!-- Descuentos -->
                            <div class="row" id="row_descuentos" style="margin-bottom: 10px;">
                                <div class="col-12" style="margin-bottom: 5px;">
                                    <h5 class="d-block g-color-gray-dark-v2">¿Tienes un cupón?</h5>
                                </div>
                                <div class="col-8">
                                    <input type="text" id="txt_desc" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15 desc">
                                </div>
                                <div class="col-4">
                                    <button id="btn_desc" type="button" class="btn u-btn-primary-outline g-font-size-13 text-uppercase g-bg-black--hover g-brd-black--hover">Aplicar</button>
                                </div>
                                <p id="errorCodigo" style="color: red; padding-left: 1.2em;">El cupón no es válido</p>
                            </div>
                            <div id="descuentos_success" class="row" style="display: none; margin-bottom: 5px;">
                                <div class="col-12 text-green">
                                    Descuento aplicado
                                </div>
                            </div>
                            <!-- Fin Descuentos -->

                            <!-- Inicia regalos -->
                            <!-- <div class="row">
                                <div class="col-12">
                                    <label class="form-check-inline u-check g-pl-25">
                                        <input id="regalo_check" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" onchange="getRegalo('<?=session()->get('regalo')?>')" value="<?=session()->get('regalo')?>">
                                        <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        Envoltura de regalo
                                    </label>
                                </div>
                            </div> -->
                            <!-- Fin regalos -->


                            @if($carrito)
                            <!-- Summary -->
                            <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                                <h4 class="h6 text-uppercase mb-3">Total</h4>

                                <div class="d-flex justify-content-between mb-2">
                                    <span class="g-color-black">Subtotal</span>
                                    <span class="g-color-black g-font-weight-300">$<span id="subtotal">{{ number_format($carrito->subtotal,2) }}</span></span>
                                </div>
                                @if ($carrito->subtotal >= $min_envio)
                                <div class="d-flex justify-content-between mb-2">
                                    <?php $envio = 0 ?>
                                    <span class="g-color-black">Envío</span>
                                    <span id="envios" class="g-color-black g-font-weight-300 envio_costo">${{ number_format($envio, 2) }}</span>
                                </div>
                                @else
                                <?php $envio = session()->get('envio') ?>
                                <div class="d-flex justify-content-between mb-2">
                                    <span class="g-color-black">Envío</span>
                                    <span id="envios" class="g-color-black g-font-weight-300 envio_costo">${{ number_format($envio, 2) }}</span>
                                </div>
                                @endif

                                <div class="d-flex justify-content-between mb-2">
                                    <span class="g-color-black">Descuento</span>
                                    <span class="g-color-black g-font-weight-300">$<span class="val_desc">0.00</span></span>
                                </div>

                                <div id="regalo_div" class="justify-content-between mb-2">
                                    <span class="g-color-black">Regalo</span>
                                    <span class="g-color-black g-font-weight-300">$<span class="val_regalo">0.00</span></span>
                                </div>

                                <div class="d-flex justify-content-between">
                                    <span class="g-color-black">Total</span>
                                    <span id="total" class="g-color-black g-font-weight-300">$<span class="total">{{ number_format($carrito->total >= $min_envio ? $carrito->total : $carrito->total + $envio, 2) }} </span></span>
                                </div>
                                <br>
                                <div class="d-flex justify-content-between">
                                    @if(($min_envio-$carrito->subtotal) > 0)
                                        <h6>Faltan <b>$<?=(number_format($min_envio-$carrito->subtotal, 2))?></b> para obtener envío gratis.</h6>
                                    @else
                                        <h6 style="color:#b78b1e;">¡Felicidades! <br>Alcanzaste la orden mínima de <b>${{$min_envio}}</b>. <br>Disfruta de tu envío gratuito. </h6>
                                    @endif
                                </div>
                            </div>
                            <!-- End Summary -->
                            @endif
                            <button class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover" id="procesar" type="button" data-next-step="#step2">Procesar compra</button>
                        </div>
                    </div>
                </div>
                <!-- END Paso 01 -Shopping Cart -------------------------------------------------------------------->

                <!-- START: Paso 02 -Envio -------------------------------------------------------------------->
                <div id="step2">
                    <div class="row">
                        <div class="col-md-8 g-mb-30">
                            <input id="id_user" name="id_user" type="hidden">

                            <div class="row" id="datos-personales-row">
                                <div class="col-sm-12 g-brd-bottom g-brd-gray-light-v3 txt-muletta-gris text-uppercase g-mb-50">
                                    <h3>MIS DATOS PERSONALES</h3>
                                </div>
                                <div class="col-sm-6 g-mb-20">
                                    <div class="form-group">
                                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Nombre</label>
                                        <input id="nombre" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="nombre" type="text" placeholder="Alexander" required data-msg="Este es un campo obligatorio">
                                        <label id="nombre-error" class="error" for="nombre">Este es un campo obligatorio</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 g-mb-20">
                                    <div class="form-group">
                                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Correo electrónico</label>
                                        <input class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" id="email" name="email" type="email" placeholder="ejemplo@mail.com" required data-msg="Este es un campo obligatorio">
                                        <label id="email-error" class="error" for="email">Este es un campo obligatorio</label>
                                    </div>
                                </div>

                                <div class="col-sm-6 g-mb-20">
                                    <div class="form-group">
                                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Teléfono</label>
                                        <input id="tel_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="tel_user" type="text" placeholder="+01 (00) 555 666 77" required data-msg="Este es un campo obligatorio">
                                        <label id="tel_user-error" class="error" for="tel_user">Este es un campo obligatorio</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="direccion-row">
                                <div class="col-sm-12 g-brd-bottom g-brd-gray-light-v3 txt-muletta-gris text-uppercase g-mb-50">
                                    <h3>Dirección de envio</h3>
                                </div>

                                <!--START:DIRECCIONES -------------------------------->
                                    <div id="form-new-dir">
                                        <!--FORMULARIO FOR NEW DIRECCION-->

                                        <div class="row" id="1-row-dir">
                                            <div class="col-sm-6 g-mb-20">
                                                <div id="form_street_user" class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Calle</label>
                                                    <input id="street_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="street_user" type="text" placeholder="">
                                                    <label id="street_user-error" class="error" for="street_user">Este es un campo obligatorio</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 g-mb-20">
                                                <div id="form_num_ext_user" class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Num. ext</label>
                                                    <input id="num_ext_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="num_ext_user" type="number" placeholder="">
                                                    <label id="num_ext_user-error" class="error" for="num_ext_user">Este es un campo obligatorio</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 g-mb-20">
                                                <div class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Num. int</label>
                                                    <input id="num_int_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="num_int_user" type="text" placeholder="" data-msg="Este es un campo obligatorio" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" value="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" id="2-row-dir">
                                            <div class="col-sm-6 g-mb-20">
                                                <div id="form_col_user" class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Colonia</label>
                                                    <input id="col_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="col_user" type="text" placeholder="">
                                                    <label id="col_user-error" class="error" for="col_user">Este es un campo obligatorio</label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 g-mb-20">
                                                <div id="form_code_postal_user" class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Código postal</label>
                                                    <input id="code_postal_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="code_postal_user" type="text" placeholder="">
                                                    <label id="code_postal_user-error" class="error" for="code_postal_user">Este es un campo obligatorio</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" id="3-row-dir">
                                            <div class="col-sm-6 g-mb-20">
                                                <div id="form_state_user" class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Estado</label>
                                                    <select id="state_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="state_user" onchange="buscarMunicipios()">
                                                        <option value=""></option>
                                                        @foreach($estados as $estado)
                                                            <option value="{{ $estado->ID_ESTADO }}">{{ $estado->DESCRIPCION_ESTADO }}</option>
                                                        @endforeach
                                                    </select>
                                                    <label id="state_user-error" class="error" for="state_user">Este es un campo obligatorio</label>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-6 g-mb-20">
                                                <div id="form_mun_user" class="form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Municipio</label>
                                                    <!-- <input id="mun_user" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="mun_user" type="text" placeholder=""> -->
                                                    <select class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="mun_user" id="mun_user">
                                        
                                                    </select>
                                                    <label id="mun_user-error" class="error" for="mun_user">Este es un campo obligatorio</label>
                                                </div>
                                            </div>


                                            <div class="col-md-12 g-mb-20">
                                                <div class="form_referencia form-group">
                                                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Referencia</label>
                                                    <textarea id="referencia" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="referencia" type="text" placeholder="Entre que calles" required> </textarea>
                                                    <label id="referencia-error" class="error" for="referencia">Este es un campo obligatorio</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--END:DIRECCIONES -------------------------------->

                            </div>
                            <!--END:LEFT SIDE COL 8 -------------------------------->

                            <!--START:RIGHT SIDE COL 4 -------------------------------->
                            <div class="col-md-4 g-mb-30" id="resumen-checkout-envio">
                                @if($carrito)
                                    <!-- Accordion -->
                                    <div id="accordion-05" class="mb-4" role="tablist" aria-multiselectable="true">
                                        <div id="accordion-05-heading-05" class="g-brd-y g-brd-gray-light-v2 py-3" role="tab">
                                            <h5 class="g-font-weight-400 g-font-size-default mb-0">
                                                <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#accordion-05-body-05" data-toggle="collapse" data-parent="#accordion-05" aria-expanded="false" aria-controls="accordion-05-body-05">
                                                    {{ count($carrito->productos) }} productos
                                                    <span class="ml-3 fa fa-angle-down"></span>
                                                </a>
                                            </h5>
                                        </div>

                                        <div id="accordion-05-body-05" class="collapse" role="tabpanel" aria-labelledby="accordion-05-heading-05">
                                            <div class="g-py-15">
                                                <ul class="list-unstyled mb-3">
                                                    @foreach ($carrito->productos as $productos)
                                                    <!-- Product -->
                                                    <li class="d-flex justify-content-start mb-3">
                                                        <img class="g-width-100  mr-3" src="{{ $productos['image'] }}" alt="Image Description">
                                                        <div class="d-block">
                                                            <h4 class="h6 g-color-black">{{ $productos['name'] }}</h4>
                                                            <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_4 mb-1">
                                                                @if ($productos['size'])
                                                                <li>Talla: {{ $productos['size'] }}</li>
                                                                @endif
                                                                @if ($productos['type'])
                                                                <li>Tipo: {{ $productos['type'] }}</li>
                                                                @endif
                                                            </ul>
                                                            <span class="d-block g-color-black g-font-weight-400">{{ $productos['qty'] }}
                                                                x $ {{ $productos['total'] }}
                                                            </span>

                                                        </div>
                                                    </li>
                                                    <!-- End Product -->
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Accordion -->

                                    <!-- Order Summary -->
                                    <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                                        <div class="g-brd-bottom g-brd-gray-light-v3 g-mb-15">
                                            <h4 class="h6 text-uppercase mb-3">Resumen de la orden</h4>
                                        </div>

                                        <div class="d-flex justify-content-between mb-2">
                                            <span class="g-color-black">Subtotal</span>
                                            <span class="g-color-black g-font-weight-300">${{ number_format($carrito->subtotal, 2) }}</span>
                                        </div>

                                        @if ($carrito->subtotal >= $min_envio)
                                            <div class="d-flex justify-content-between mb-2">
                                                <?php $envio = 0 ?>
                                                <span class="g-color-black">Envío</span>
                                                <span class="g-color-black g-font-weight-300 envio_costo">${{ number_format($envio, 2) }}</span>
                                            </div>
                                        @else
                                            <?php session()->get('envio') ?>
                                            <div class="d-flex justify-content-between mb-2">
                                                <span class="g-color-black">Envío</span>
                                                <span class="g-color-black g-font-weight-300 envio_costo">${{ number_format($envio, 2) }}</span>
                                            </div>
                                        @endif

                                        <div class="d-flex justify-content-between mb-2">
                                            <span class="g-color-black">Descuento</span>
                                            <span class="g-color-black g-font-weight-300">$<span class="val_desc">0.00</span></span>
                                        </div>

                                        <div id="regalo_div_ult" class="justify-content-between mb-2">
                                            <span class="g-color-black">Regalo</span>
                                            <span class="g-color-black g-font-weight-300">$<span class="val_regalo">0.00</span></span>
                                        </div>

                                        <div class="d-flex justify-content-between">
                                            <span class="g-color-black">Total</span>
                                            <input id="input_total" type="hidden" value="<?= $carrito->total>= $min_envio ? $carrito->total : $carrito->total + $envio ?>">
                                            <span class="g-color-black g-font-weight-300">$<span class="total">{{ number_format($carrito->total>= $min_envio ? $carrito->total : $carrito->total + $envio, 2) }}</span> </span>
                                        </div>

                                    </div>
                                    
                                    <input type="hidden" id="url_verificar_ventas" value="<?= Route('verifySales', ['ID_CLIENTE' => 'null_idCliente', 'EMAIL' => 'null_email', 'SUBTOTAL' => $carrito->subtotal, 'IMPORTE' => $carrito->total, 'ENVIO' => $envio])?>">
                                    <input type="hidden" id="IdClient">

                                    <button id="btnSecondStep" class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover" type="button" data-next-step="#step3" onclick="veryfySalesUser('<?= Route('registerUser') ?>')">PROCEDER A PAGAR</button>
                                    <!-- End Order Summary -->
                                @endif
                            
                            </div>
                            
                            <!--END:RIGHT SIDE COL 4 -------------------------------->

                        </div>
                    </div>
                    <!-- END: PASO 02 - Envio -------------------------------------------------------------------->
                </div>
                <!-- START: PASO 03 - Pago -------------------------------------------------------------------->
                <div id="step3">
                    <div class="row">

                        <div class="col-sm-12 g-brd-bottom g-brd-gray-light-v3 txt-muletta-gris text-uppercase g-mb-50">
                            <h3>MÉTODO DE PAGO</h3>
                        </div>

                        <div class="col-12" style="margin-bottom: 15px;">
                            <div class="header_method">
                                <img src="{{ asset('assets/global/img/compras/PAGOSEGURO_MULETTA.png') }}" class="pago_seguro">
                                <div class="header-whats">
                                    &nbsp;¿TIENES ALGUNA DUDA EN LA QUE TE PODAMOS APOYAR? CONTÁCTANOS VÍA WHATSAPP &nbsp;
                                    <img src="{{ asset('assets/global/img/compras/whatsapp.png') }}" class="img_whatsapp">
                                    &nbsp;449 919 63 07
                                </div>
                            </div>
                        </div>


                        <div class="col-md-8 g-mb-30">
                            <!-- Payment Methods -->
                            <div class="accordion" id="accordionExample">

                                <div class="card">
                                    <div class="card-header header_method" id="headingFive">
                                        <h2 class="mb-0 title_method">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive" onclick="changeinput(5)">
                                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                                    <input id="inputOpenPayTransf" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="5">
                                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </span>
                                                    <img src="{{ asset('assets/global/img/compras/transferencia/transferencia.webp') }}" width="90">
                                                    TRANSFERENCIA O DEPOSITO
                                                </label>
                                            </button>
                                        </h2>
                                        <div class="img_openpay">
                                            <img src="{{ asset('assets/global/img/compras/debito/BBVA.webp') }}" width="120" style="margin-left: 1em">
                                            <ul style="padding: 1em 0 0 1em;">
                                                <li>
                                                    <b>BENEFICIARIO: </b> CREATMOS S.A.P.I DE C.V.
                                                </li>
                                                <li>
                                                    <b>TARJETA: </b> 4555 1130 0782 4610
                                                </li>
                                                <li>
                                                    <b>CUENTA: </b> 0111226258
                                                </li>
                                                <li>
                                                    <b>CLABE: </b> 012010001112262580
                                                </li>
                                                <li>
                                                    <b>BANCO: </b> BBVA
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-6 text-center">
                                                    <input type="file" class="inputfile" id="archivo1" name="archivo1"/>
                                                    <label for="archivo1"></label>
                                                    <br />
                                                    <button type="button" id="SubirArchivo" style="display: none;">Subir archivo</button>
                                                </div>
                                                <div class="col-6 text-center">
                                                    <button type="button" onclick="abrirCamara()" class="btnTomarFotos">
                                                        <img src="{{ asset('assets/global/img/compras/transferencia/camara.webp') }}" width="100">
                                                    </button>
                                                </div>
                                            </div>

                                            <div id="mensaje"></div>
                                            <div>
                                                *Una vez finalizada la compra verificaremos el comprobante de pago para procesar el pedido
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header header_method" id="headingTree">
                                        <h2 class="mb-0 title_method">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTree" aria-expanded="true" aria-controls="collapseTree" onclick="changeinput(3)">
                                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                                    <input id="inputOpenPay" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" checked value="3">
                                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </span>
                                                    <img src="{{ asset('assets/global/img/compras/openpay.png') }}" width="120">
                                                    <br>
                                                    <img src="{{ asset('assets/global/img/compras/openpay3DS.png') }}" width="50">
                                                </label>
                                            </button>
                                        </h2>
                                        <div class="img_openpay">
                                            <div style="display: flex; align-items: center;">
                                                Tarjetas de crédito
                                                <div style="width: 68%; border-bottom: 1px black solid;">
                                                    <img src="{{ asset('assets/global/img/compras/credito/visa.webp') }}" width="50" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/credito/masterCard.webp') }}" width="50" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/credito/americanExpress.webp') }}" width="50" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/credito/carnet.webp') }}" width="50" style="margin: 10px;">
                                                </div>
                                            </div>
                                            <div style="display: flex; align-items: center;">
                                                Tarjetas de débito
                                                <div style="margin-left: 5px; width: 68%;">
                                                    <img src="{{ asset('assets/global/img/compras/debito/BBVA.webp') }}" width="50" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/debito/banorte.webp') }}" width="65" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/debito/citibanamex.webp') }}" width="70" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/debito/ixe.webp') }}" width="50" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/debito/santander.webp') }}" width="65" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/debito/hsbc.webp') }}" width="50" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/debito/scotiabank.webp') }}" width="55" style="margin: 10px;">
                                                    <img src="{{ asset('assets/global/img/compras/debito/inbursa.webp') }}" width="65" style="margin: 10px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="collapseTree" class="collapse show" aria-labelledby="headingTree" data-parent="#accordionExample">
                                        <div class="card-body">
                                            Al dar "Finalizar compra" seras redirigido a nuestro sistema de cobro para completar tu compra de forma segura
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header header_method" id="headingOne">
                                        <h2 class="mb-0 title_method">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" onclick="changeinput(2)">
                                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                                    <input id="inputMercadopago" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="2">
                                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </span>
                                                    <div style="display: flex">
                                                        <img src="{{ asset('assets/global/img/compras/MercadoPago.webp') }}" width="60">
                                                        <img src="{{ asset('assets/global/img/compras/oxxo.webp') }}" width="60" style="margin-left: 4px;">
                                                    </div>
                                                </label>
                                            </button>
                                        </h2>
                                        <div class="img_openpay">
                                            <b>PUEDES PAGAR SIN NECESIDAD DE CREAR UNA CUENTA</b>
                                        </div>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                            Al dar "Finalizar compra" seras redirigido A MercadoPago para completar tu compra de forma segura
                                            <br>
                                            <br>
                                            <span style="color: #4c4c4c; font-size: 14px; font-weight: bold; text-decoration: underline;">*Recuerda que si pagas con Oxxo o deposito bancario tienes que imprimir el comprobante.</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header header_method" id="headingTwo">
                                        <h2 class="mb-0 title_method">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" onclick="changeinput(1)">
                                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                                    <input id="inputMercadopago" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="1">
                                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </span>
                                                    <img src="{{ asset('assets/global/img/compras/paypal.webp') }}" width="120">
                                                </label>
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            Al dar "Finalizar compra" seras redirigido a Paypal para completar tu compra de forma segura.
                                            <br>
                                            <br>
                                            <div id="paypal-button-container"></div>
                                        </div>
                                    </div>
                                </div>

                                <!--<div class="card">
                                    <div class="card-header header_method" id="headingFive">
                                        <h2 class="mb-0 title_method">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive" onclick="changeinput(5)">
                                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                                    <input id="inputOpenPayTransf" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="5">
                                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </span>
                                                    TRANSFERENCIA BANCARIA
                                                </label>
                                            </button>
                                        </h2>
                                        <img src="{{ asset('assets/global/img/compras/openpay.png') }}" class="img_openpay">
                                    </div>

                                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                        <div class="card-body">
                                            Al dar "Finalizar compra" proporcionaremos el numero de cuenta
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header header_method" id="headingfour">
                                        <h2 class="mb-0 title_method">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" onclick="changeinput(4)">
                                                <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                                                    <input id="inputWebPay" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio" value="4">
                                                    <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                        <i class="fa" data-check-icon="&#xf00c"></i>
                                                    </span>
                                                    WEBPAY
                                                </label>
                                            </button>
                                        </h2>
                                        <img src="{{ asset('assets/global/img/index/03_Logo_webpay_plus-300px.png') }}" class="img_openpay">
                                    </div>
                                    <div id="collapseFour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExample">
                                        <div class="card-body text-center">
                                            Al dar "Finalizar compra" seras redirigido a WebPay para completar tu compra  de forma segura.

                                            <input type="hidden" id="IDV">
                                            <input type="hidden" id="IDD">

                                            <br>
                                            <br>
                                         </div>
                                    </div>
                                </div>-->
                                
                            </div>
                        </div>

                        <div class="col-md-4 g-mb-30">
                            @if($carrito)
                                <!-- Accordion -->
                                <div id="accordion-05" class="mb-4" role="tablist" aria-multiselectable="true">
                                    <div id="accordion-05-heading-05" class="g-brd-y g-brd-gray-light-v2 py-3" role="tab">
                                        <h5 class="g-font-weight-400 g-font-size-default mb-0">
                                            <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#accordion-05-body-05" data-toggle="collapse" data-parent="#accordion-05" aria-expanded="false" aria-controls="accordion-05-body-05"> {{ count($carrito->productos) }} productos
                                                <span class="ml-3 fa fa-angle-down"></span></a>
                                        </h5>
                                    </div>
                                    <div id="accordion-05-body-05" class="collapse" role="tabpanel" aria-labelledby="accordion-05-heading-05">
                                        <div class="g-py-15">
                                            <ul class="list-unstyled mb-3">
                                                @foreach ($carrito->productos as $productos)
                                                    <!-- Product -->
                                                    <li class="d-flex justify-content-start mb-3">
                                                        <img class="g-width-100  mr-3" src="{{ $productos['image'] }}" alt="Image Description">
                                                        <div class="d-block">
                                                            <h4 class="h6 g-color-black">{{ $productos['name'] }}</h4>
                                                            <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_4 mb-1">
                                                                @if ($productos['size'])
                                                                <li>Talla: {{ $productos['size'] }}</li>
                                                                @endif
                                                                @if ($productos['type'])
                                                                <li>Tipo: {{ $productos['type'] }}</li>
                                                                @endif
                                                            </ul>
                                                            <span class="d-block g-color-black g-font-weight-400">{{ $productos['qty'] }}
                                                                x $ {{ $productos['total'] }}</span>

                                                        </div>
                                                    </li>
                                                    <!-- End Product -->
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Accordion -->

                                <!-- Order Summary -->
                                <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                                    <div class="g-brd-bottom g-brd-gray-light-v3 g-mb-15">
                                        <h4 class="h6 text-uppercase mb-3">Resumen de la orden</h4>
                                    </div>

                                    <div class="d-flex justify-content-between mb-2">
                                        <span class="g-color-black">Subtotal</span>
                                        <span id="subtotal" class="g-color-black g-font-weight-300">${{ number_format($carrito->subtotal, 2) }}</span>
                                    </div>

                                    @if ($carrito->subtotal >= $min_envio)
                                        <div class="d-flex justify-content-between mb-2">
                                            <?php $envio = 0 ?>
                                            <span class="g-color-black">Envío</span>
                                            <span class="g-color-black g-font-weight-300 envio_costo">${{ number_format($envio, 2) }}</span>
                                        </div>
                                    @else
                                        <?php session()->get('envio') ?>
                                        <div class="d-flex justify-content-between mb-2">
                                            <span class="g-color-black">Envío</span>
                                            <span class="g-color-black g-font-weight-300 envio_costo">${{ number_format($envio, 2) }}</span>
                                        </div>
                                    @endif

                                    <div class="d-flex justify-content-between mb-2">
                                        <span class="g-color-black">Descuento</span>
                                        <input name="id_desc" id="id_desc" type="hidden">
                                        <input name="monto_desc" id="monto_desc" type="hidden">
                                        <span class="g-color-black g-font-weight-300">$<span class="val_desc">0.00</span></span>
                                    </div>

                                    <div id="regalo_div_ult" class="justify-content-between mb-2">
                                        <span class="g-color-black">Regalo</span>
                                        <input type="hidden" name="regalo" id="regalo_input" value="0">
                                        <span class="g-color-black g-font-weight-300" style="float: right;">$<span class="val_regalo">0.00</span></span>
                                    </div>
                                    
                                    <div class="d-flex justify-content-between mb-2">
                                        <span class="g-color-black">Muletta Coins</span>
                                        <input type="hidden" name="coins" id="coins_input" value="0">
                                        <span class="g-color-black g-font-weight-300">$<span class="val_coins">0.00</span></span>
                                    </div>

                                    <div class="d-flex justify-content-between">
                                        <span class="g-color-black">Total</span>
                                        <input id="input_total" name="input_total" type="hidden" value="<?= $carrito->total ?>">
                                        <input id="id_venta" name="id_venta" type="hidden">
                                        <span class="g-color-black g-font-weight-300">$<span class="total">{{ number_format($carrito->total>= $min_envio ? $carrito->total : $carrito->total + $envio, 2) }}</span> </span>
                                    </div>

                                </div>
                                
                                <p class="text-center">
                                    Al dar click en Finalizar Compra acepto <a href="{{ route('terminos_condiciones') }}" class="txt-muletta-oro">Términos, Condiciones y Aviso de Privacidad</a>
                                </p>

                                <!-- End Order Summary -->
                                <button id="btn_finalizar" type="submit" class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover" style="display:none;">Finalizar compra</button>
                                <button type="button" class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover" id="fwebpay_url">Finalizar compra</button>
                            @endif
                        </div>
                    </div>
                </div><!-- #step3 -->
            </div>
        </form>
        <!-- END: CHECKOUT FORM -------------------------------------------------------------------->
    </div>

    <div id="error-modal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <span id="url_pedidos" style="display: none;" >{{ route('editar-cuenta') }} </span>
                    </div>
                    
                    <div>
                        <h1 class="text-center">OOPS!! <img src=""></h1>
                        <h2 class="text-center">ALGO SALIO MAL</h2>

                        <h4 class="text-center">NO TE PREOCUPES, NOSOTROS TE PODEMOS AYUDAR.</h4>

                        <P>La plataforma no pudo procesar tu pago por el siguiente motivo:</P>

                        <div class="text-center" id="error_tarjeta">
                            <img src="{{ asset('assets/global/img/compras/Incorrecto_.png') }}"  height="55px" style="margin-right: 5px;">
                            <span id="message-error" style="vertical-align: middle;"></span>
                        </div>

                        <p class="razon-why" id="errores_principales">
                            <span>
                                <img src="{{ asset('assets/global/img/compras/Incorrecto_.png') }}" width="50px" style="margin-right: 5px;"> Información incorrecta.
                            </span>
                            <br>
                            <span>
                                <img src="{{ asset('assets/global/img/compras/Billete_.png') }}" width="45px" style="margin-right: 10px;"> Fondos insuficientes.
                            </span>
                            <br>
                            <span>
                                <img src="{{ asset('assets/global/img/compras/Validacion_.png') }}" width="55px" style="margin-right: 2px;"> Validación de seguridad.
                            </span>
                        </p>

                        <p>Intenta hacer tu pedido con otro metodo de pago otra tarjeta ó validando que tus datos sean correctos </p>

                        <p>Ayuda personalizada vía <img src="{{ asset('assets/global/img/compras/whatsapp.png') }}" class="img_whatsapp"> Whatsapp (449 919 63 07), escribenos y te ayudaremos a completar tu compra</p>

                        <input id="url_reintentar_pago" type="hidden" value="{{ Route('reintentarPago', ['ID_VENTA' => 'cambio_venta']) }}">
                        <a onclick="reintentarPago()" class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4 g-bg-black--hover g-brd-black--hover">Reintentar Pago</a>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div id="security-modal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div id="security-body" class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <div id="camara-modal" class="modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div id="camara-body" class="modal-body">
                    <div>
                        <select id="select-camara" class="custom-select" onchange="cambiarCamara()">
                            <option value="">Select camera</option>
                        </select>
                    </div>
                    <div id="div-video-envivo" style="margin-top:5px">
                        <video id="video" playsinline autoplay></video>
                        <button id="startbutton" class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4">Tomar foto</button>
                    </div>
                    <div id="div-imagen-tomada" style="display: none">
                        <canvas id="canvas" style="width:100%;"></canvas>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-6">
                                <button id="save_image_cam" class="btn btn-block btn-success g-font-size-13 text-uppercase g-py-15 mb-4">Guradar imagen</button>
                            </div>
                            <div class="col-6">
                                <button id="cancel_image_cam" class="btn btn-block btn-danger g-font-size-13 text-uppercase g-py-15 mb-4">Sustituir imagen</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- PRODUCCION -->
    <script src="https://www.paypal.com/sdk/js?client-id=AVxnFcyFq0c_5Ixy-yo7X62IYwgu_etbtThdZ9sFjQWiRwDNB7fX0YwDnc8GjVdMpuOKN3PV487X3j2e&currency=MXN"></script>
    <!-- lOCAL -->
    <!-- <script src="https://www.paypal.com/sdk/js?client-id=AeIAjy-jFBp8X1NVOqeO65TCtoTDOTjcSLiRzNk6faahVOwgrL6PR9KCCYCrXw4dmMZjciluYwwvrztQ&currency=MXN"></script> -->
    <script src="{{ asset('assets/global/js/maindsoft/carrito-user.js') }}"></script>
@endsection