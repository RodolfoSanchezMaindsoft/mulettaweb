<!DOCTYPE html>
<html lang="es">
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-68032621-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-68032621-1');
    </script>

    <meta charset="UTF-8">
    <title> @yield('title', 'Muletta') | Muletta </title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! htmlScriptTagJsApi([ 'action' => 'homepage' ]) !!} 

    <!--Palabras clave de la pagina (Importante en el seo)-->
    <meta name="author" content="MAINDSOFT SISTEMAS INTEGRALES">
    <meta name="keywords" content="@yield('keywords', 'Muletta, Aguascalientes, Ropa')">
    <!--se ocupa para que funcionen los media quieries-->
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta property="og:site_name" content="Muletta">
    <meta property="og:locale" content="es_MX">
    <meta property="og:type" content="@yield('meta_type', 'website')" />
    <meta property="og:title" content="@yield('meta_title', 'Muletta | Expresa tu Esencia')" />
    <meta property="og:url" content="@yield('meta_url', 'https://www.muletta.com/')" />
    <meta property="og:image" content="@yield('meta_image', 'https://muletta.com/assets/global/img/logo/logo_metas.jpeg')" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/global/img/favicon/fav.ico') }}">
    
     <link rel="preload" href="{{ asset('assets/global/slick-carousel/slick/slick.min.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/slick-carousel/slick/slick.min.css') }}"></noscript>
    <link rel="preload" href="{{ asset('assets/global/slick-carousel/slick/slick-theme.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/slick-carousel/slick/slick-theme.css') }}"></noscript>

    <!-- Owl carrousel v3 -->
    <link rel="preload" href="{{ asset('assets/global/css/owl_carrousel/owl.carousel.min.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/owl_carrousel/owl.carousel.min.css') }}"></noscript>
    <!-- <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'> -->

    <!-- Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,600i,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rufina:400,700&display=swap" rel="stylesheet">
    
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css"> -->
    
    <link rel="stylesheet" href="{{ asset('assets/global/css/font-awesome/css/all.min.css') }}" >
    <!--<noscript><link rel="stylesheet" href="{{ asset('assets/global/css/font-awesome/css/all.min.css') }}"></noscript>-->
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--<noscript><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"></noscript>-->
    
    <!-- <link rel="preload" href="{{ asset('assets/global/css/bootstrap/bootstrap.min.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/bootstrap/bootstrap.min.css') }}"></noscript> -->
    
    <!-- CSS Unify -->
    <link rel="preload" href="{{ asset('assets/global/css/unify/unify-core.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/unify/unify-core.css') }}"></noscript>
    <link rel="stylesheet" href="{{ asset('assets/global/css/unify/unify-components.css') }}" as="style">
    <!--<noscript><link rel="stylesheet" href="{{ asset('assets/global/css/unify/unify-components.css') }}"></noscript>-->
    <link rel="stylesheet" href="{{ asset('assets/global/css/unify/unify-globals.css') }}" as="style">
    <!--<noscript><link rel="stylesheet" href="{{ asset('assets/global/css/unify/unify-globals.css') }}"></noscript>-->

    <!-- CSS Maindsoft -->
    <!-- <link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/muletta-header.css') }}"> -->
    <link rel="preload" href="{{ asset('assets/global/css/maindsoft/muletta-header.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/muletta-header.css') }}"></noscript>
    
    <link rel="preload" href="{{ asset('assets/global/css/maindsoft/muletta-footer.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/muletta-footer.css') }}"></noscript>
   
    <link rel="preload" href="{{ asset('assets/global/css/maindsoft/index.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/index.css') }}"></noscript>
    <link rel="preload" href="{{ asset('assets/global/css/maindsoft/index_old.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/index_old.css') }}"></noscript>
   
    <link rel="preload" href="{{ asset('assets/global/css/maindsoft/custom.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/custom.css') }}"></noscript>
    <link rel="preload" href="{{ asset('assets/global/css/maindsoft/countdown.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/countdown.css') }}"></noscript>

    <!-- Sweetalert2 -->
    <link rel="preload" href="{{ asset('assets/global/css/maindsoft/sweetalert2.min.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/sweetalert2.min.css') }}"></noscript>
    <link rel="preload" href="{{ asset('assets/global/css/maindsoft/animate.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="{{ asset('assets/global/css/maindsoft/animate.css') }}"></noscript>

    <link rel="preload" href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"></noscript>
    

    @yield('css')

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '357019004873341');
        fbq('track', 'PageView');
    </script>
    
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=357019004873341&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W7ZDC75');</script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for https://muletta.com/ -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:2197515,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    
    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/07a1c32da8c91d0c78953cafa/aadf427254741792d9f2bdf97.js");</script>

</head>
<body>
    
    

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7ZDC75"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    @include('template.header')

    @if( !empty( session()->get('countDown')['countDown'] ) )
        @include('componentes.countdown')
    @endif

    <main id="main">
        @yield('content')

        <a href="https://api.whatsapp.com/send?phone=5214499196307" class="whatsapp" target="_blank">
            <i class="fab fa-whatsapp whatsapp-icon"></i>
        </a>

        <button id="btn_top" class="btn-top" onclick="$('html, body').animate({ scrollTop: 0 }, 'slow');">
            <img src=" {{ asset('assets/global/img/index/flecha_web.png') }} " style="width: 100%;">
        </button>
    </main>

    <div id="fot" style="margin-top: 7em;">
        @include('template.footer')
    </div>
    

    <!-- Start of muletta Zendesk Widget script -->
    <!-- <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=81d399aa-6d84-49ec-bf1c-3c63e1809eda"> </script> -->
    <!-- End of muletta Zendesk Widget script -->

    
    <!-- jQuery -->
    <!-- <script src="{{ asset('assets/global/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/jquery/jquery-migrate.min.js') }}"></script> -->
    
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.3.2.min.js" integrity="sha256-Ap4KLoCf1rXb52q+i3p0k2vjBsmownyBTE1EqlRiMwA=" crossorigin="anonymous"></script>

    <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <script src="{{ asset('assets/global/js/unify/hs.core.js') }}"></script>

    <!-- OWL CAROUSEL v3 -->
    <script src="{{ asset('assets/global/js/owl-carousel/owl.carousel.min.js') }}"></script>
    <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script> -->
    <!-- Bootstrap -->
    <!-- <script src="{{ asset('assets/global/js/bootstrap/bootstrap.min.js') }}"></script>-->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- custome js -->
    <script type="text/javascript" src="{{ asset('assets/global/js/maindsoft/custome.js') }}"></script>
    <!-- header js - se ocupa para el acrodeon -->
    <script async type="text/javascript" src="{{ asset('assets/global/js/maindsoft/header.js') }}"></script>
    <!-- Sweetalert -->
    <!-- <script src="{{ asset('assets/global/js/maindsoft/sweetalert2.all.min.js') }}"></script> -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- LazyLoad image -->
    <!--<script src="{{ asset('assets/global/js/lazyload/lazyload.min.js') }}"></script>-->
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.4.0/dist/lazyload.min.js"></script>

    <!-- index js -->
    <script async type="text/javascript" src="{{ asset('assets/global/slick-carousel/slick/slick.min.js') }}"></script>
    <script async type="text/javascript" src="{{ asset('assets/global/js/maindsoft/index.js') }}"></script>
    
    <script async type="text/javascript" src="{{ asset('assets/global/js/maindsoft/eliminar_carrito.js') }}"></script>
    <script async type="text/javascript" src="{{ asset('assets/global/js/countdown/countdown.js') }}"></script>

    @include('sweetalert::alert')

    <script src="https://www.google.com/recaptcha/api.js"></script>

    <script>
        function onSubmit(token) {
            document.getElementById("demo-form").submit();
        }
    </script>

    @yield('script')

</body>
</html>