        <!-- Start: Footer -->
        <footer id="contact-section" class="container-flex g-font-nunito g-brd-bottom g-brd-top g-brd-gray-light-v2 g-mt-15">

            <!-- <center>

                <div class="col-lg-4 g-mt-30">
                    <h3 class="text-uppercase g-font-prompt">SUSCRIPCIÓN A NEWSLETTER</h3>
                    <form class="input-group g-pos-rel g-mb-50 g-mb-30--smg-font-prompt">
                        <input class="form-control g-font-prompt g-placeholder-text g-font-size-13 g-color-black g-pl-25 g-pr-40 g-py-15 g-brd-none g-brd-bottom g-brd-gray-light-v2 g-brd-primary--focus " type="email" placeholder="E-mail">
                        <span class="input-group-addon g-pos-abs g-top-0 g-right-0 h-100 g-brd-left-none">
                            <button class="btn w-100 h-100 g-bg-transparent g-color-black g-color-primary--hover g-font-size-12 g-brd-none g-pa-10" type="submit">
                                ->
                            </button>
                        </span>
                    </form>
                </div>
            </center> -->
            <!--/*footer acrodeon start*/-->
            <div class="container margin-top" >
                <div id="footer-accordeon" class="row g-pt-10 g-hidden-md-up-v2">


                    <div class="col-sm-6 ">
                        <button class="accordion">
                            AYUDA
                            <i class="ml-auto  fas fa-chevron-down"></i>
                        </button>
                        <div class="panel">
                            <ul class="list-unstyled g-font-prompt g-font-size-13 mb-3" style="letter-spacing: 1px; font-weight: normal !important;">
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{ Route('faq_menu') }}">Preguntas Frecuentes</a>
                                </li>
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="tel:4499196307">Teléfono (449) 919 6307
                                    </a>
                                </li>


                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{ Route('contacto') }}">Contacto
                                    </a>
                                </li>
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{ Route('facturacion') }}">Facturacion
                                    </a>
                                </li>
                                <!-- <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="http://tracking.muletta.com/" target="_blank">Rastrear tu Envío
                                    </a>
                                </li>
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{ Route('bolsa_trabajo') }}">Bolsa de trabajo
                                    </a>
                                </li> -->
                            </ul>

                        </div><!-- panel-->
                    </div><!-- col-sm-6 -->

                    <div class="col-sm-6 ">
                        <button class="accordion">
                            GUÍA DE COMPRA
                            <i class="ml-auto  fas fa-chevron-down"></i>
                        </button>
                        <div class="panel">
                            <ul class="list-unstyled g-font-prompt g-font-size-13 mb-3" style="letter-spacing: 1px; font-weight: normal !important;">
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{route('politicas_envio')}}">Pago y método de envío</a>
                                </li>
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{route('politicas_cambios_devoluciones')}}">Cambios y devoluciones
                                    </a>
                                </li>


                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{route('guia_tallas')}}">Guía de Tallas
                                    </a>
                                </li>
                                <!-- <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{route('catalogo_2018')}}">Catalogo 2018
                                    </a>
                                </li> -->
                            </ul>

                        </div><!-- panel-->
                    </div><!-- col-sm-6 -->

                    <div class="col-sm-6 ">
                        <button class="accordion">
                            MI CUENTA
                            <i class="ml-auto  fas fa-chevron-down"></i>
                        </button>
                        <div class="panel">

                            <?php
                        $carrito = session()->get('carrito');
                        $usuario = session()->get('perfilUsuario');
                      ?>
                            <!-- Links -->
                            <ul class="list-unstyled g-font-prompt g-font-size-13 mb-3" style="letter-spacing: 1px; font-weight: normal !important;">
                                <li class="g-py-6">
                                    @if (!$usuario)
                                    <a class="txt-muletta-gris-claro" href="{{route('login')}}">Mi cuenta
                                    </a>
                                    @else
                                    <a class="txt-muletta-gris-claro" href="{{route('mi-cuenta')}}">Mi cuenta
                                    </a>
                                    @endif
                                </li>
                                <li class="g-py-6">
                                    @if (!$usuario)
                                    <a class="txt-muletta-gris-claro" href="{{route('login')}}">Mis pedidos
                                    </a>
                                    @else
                                    <a class="txt-muletta-gris-claro" href="{{route('editar-cuenta')}}">Mis pedidos
                                    </a>
                                    @endif

                                </li>
                                <li class="g-py-6">
                                    @if (!$usuario)
                                    <a class="txt-muletta-gris-claro" href="{{route('login')}}">Mis direcciónes
                                    </a>
                                    @else
                                    <a class="txt-muletta-gris-claro" href="{{route('editar-direcciones')}}">Mis direcciónes
                                    </a>
                                    @endif

                                </li>
                            </ul>

                        </div><!-- panel-->
                    </div><!-- col-sm-6 -->

                    <div class="col-sm-6 ">
                        <button class="accordion">
                            POLÍTICAS
                            <i class="ml-auto  fas fa-chevron-down"></i>
                        </button>
                        <div class="panel">

                            <ul class="list-unstyled g-font-prompt g-font-size-13 mb-3" style="letter-spacing: 1px; font-weight: normal !important;">
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{route('politicas_envio')}}">Políticas de envío
                                    </a>
                                </li>
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{route('politicas_cambios_devoluciones')}}">Cambios y devoluciones
                                    </a>
                                </li>
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{route('terminos_condiciones')}}">Términos y Condiciones
                                    </a>
                                </li>
                                <li class="g-py-6">
                                    <a class="txt-muletta-gris-claro" href="{{route('aviso_privacidad')}}">Aviso de Privacidad
                                    </a>
                                </li>

                            </ul>

                        </div><!-- panel-->
                    </div><!-- col-sm-6 -->


                </div>
                <!--row g-pt-10 -->

                <!--/*

footer acrodeon end
    */-->
                <div class="row g-pt-10 g-hidden-md-down-v2">

                    <div class="col-sm-6 col-lg-3 g-mb-50">
                        <h3 class="text-uppercase g-font-prompt mb-2">AYUDA</h3>

                        <!-- Links -->
                        <ul class="list-unstyled   g-font-prompt g-font-size-13 mb-3" style="letter-spacing: 1px; font-weight: normal !important;">
                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{ Route('faq_menu') }}">Preguntas Frecuentes

                                </a>
                            </li>
                            <li class="g-py-6">
                                <a class=" txt-muletta-gris-claro" href="tel:4499196307">Teléfono (449) 919 6307</a>
                            </li>

                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{ Route('contacto') }}">Contacto

                                </a>
                            </li>
                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{ Route('facturacion') }}">Facturacion
                                </a>
                            </li>
                            
                            <!-- <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="http://tracking.muletta.com/" target="_blank">Rastrear tu Envío

                                </a>
                            </li> -->

                            <!-- <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{ Route('bolsa_trabajo') }}">Bolsa de trabajo

                                </a>
                            </li> -->

                        </ul>
                        <!-- End Links -->
                    </div>

                    <div class="col-sm-6 col-lg-3 g-mb-50">
                        <h3 class="text-uppercase g-font-prompt mb-2">GUÍA DE COMPRA</h3>

                        <!-- Links -->
                        <ul class="list-unstyled g-font-prompt g-font-size-13 mb-3" style="letter-spacing: 1px; font-weight: normal !important;">
                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{route('politicas_envio')}}">Pago y método de envío</a>
                            </li>
                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{route('politicas_cambios_devoluciones')}}">Cambios y devoluciones
                                </a>
                            </li>


                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{route('guia_tallas')}}">Guía de Tallas
                                </a>
                            </li>
                            <!-- <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{route('catalogo_2018')}}">Catalogo 2018
                                </a>
                            </li> -->
                        </ul>
                        <!-- End Links -->
                    </div>

                    <div class="col-sm-6 col-lg-3 g-mb-50">
                        <h3 class="text-uppercase g-font-prompt mb-2">MI CUENTA</h3>
                        <?php
                        $carrito = session()->get('carrito');
                        $usuario = session()->get('perfilUsuario');
                      ?>
                        <!-- Links -->
                        <ul class="list-unstyled g-font-prompt g-font-size-13 mb-3" style="letter-spacing: 1px; font-weight: normal !important;">
                            <li class="g-py-6">
                                @if (!$usuario)
                                <a class="txt-muletta-gris-claro" href="{{route('login')}}">Mi cuenta
                                </a>
                                @else
                                <a class="txt-muletta-gris-claro" href="{{route('mi-cuenta')}}">Mi cuenta
                                </a>
                                @endif
                            </li>
                            <li class="g-py-6">
                                @if (!$usuario)
                                <a class="txt-muletta-gris-claro" href="{{route('login')}}">Mis pedidos
                                </a>
                                @else
                                <a class="txt-muletta-gris-claro" href="{{route('editar-cuenta')}}">Mis pedidos
                                </a>
                                @endif

                            </li>
                            <li class="g-py-6">
                                @if (!$usuario)
                                <a class="txt-muletta-gris-claro" href="{{route('login')}}">Mis direcciónes
                                </a>
                                @else
                                <a class="txt-muletta-gris-claro" href="{{route('editar-direcciones')}}">Mis direcciónes
                                </a>
                                @endif

                            </li>
                        </ul>
                        <!-- End Links -->
                    </div>

                    <div class="col-sm-6 col-lg-3 g-mb-50">
                        <h3 class="text-uppercase g-font-prompt mb-2">POLÍTICAS</h3>

                        <!-- Links -->
                        <ul class="list-unstyled g-font-prompt g-font-size-13 mb-3" style="letter-spacing: 1px; font-weight: normal !important;">
                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{route('politicas_envio_politicas')}}">Políticas de envío
                                </a>
                            </li>
                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{route('politicas_cambios_devoluciones_politicas')}}">Cambios y devoluciones
                                </a>
                            </li>
                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{route('terminos_condiciones')}}">Términos y Condiciones
                                </a>
                            </li>
                            <li class="g-py-6">
                                <a class="txt-muletta-gris-claro" href="{{route('aviso_privacidad')}}">Aviso de Privacidad
                                </a>
                            </li>

                        </ul>
                        <!-- End Links -->
                    </div>

                </div>
            </div><!-- End .container -->
            <hr class="g-brd-secondary-light-v1 my-0">
            <div class="container">
                <!-- Copyright -->
                <div class="row align-items-center g-py-35">
                    <div class="col-4">
                        <a class="g-text-underline--none--hover" href="index.html" style="display: none">
                        </a>
                    </div>
                    <div class="col-8 text-right">
                        <li class="list-inline-item g-mx-2">
                            <a class="u-icon-v1 u-icon-size--sm  g-bg-white-opacity-0_1 g-font-size-25 g-color-primary--hover" href="https://www.instagram.com/mulettaoficial/" target="_blank">

                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li class="list-inline-item g-mx-2">
                            <a class="u-icon-v1 u-icon-size--sm  g-bg-white-opacity-0_1 g-font-size-25 g-color-primary--hover" href="https://es-la.facebook.com/MulettaOficial/" target="_blank">

                                <i class="fab fa-facebook-square"></i>
                            </a>
                        </li>
                    </div>
                </div>

                <!-- End Copyright -->
            </div><!-- End .container -->
        </footer>

        <!-- End: Footer -->
