<div id="top-nav-header">
    <meta charset="gb18030">
    <header>
        <div id="header-menu-navbar-links-left">
            <ul id="nav_menu">
                <?php
                    $menu_PrimerNivel = session()->get('menuPrimerNivel');
                    $segundoNivel = session()->get('menuSubNivel');
                ?>
                <input type="hidden" id="categoria_anterior" value="">
                <div class="row">
                    @if(count($menu_PrimerNivel) > 0)
                        @foreach($menu_PrimerNivel as $primer)
                            @foreach($primer as $details)
                                <div id="mujer-mega-menu" class="col-auto" onmouseenter="OpenNavCustom(<?=$details->ID_CATEGORIA_FLEXIBLE?>)">
                                    @if($details->NOMBRE != 'Rebajas')
                                        <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $details->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $details->URL_CATEGORIA])}}" class="cool-link">
                                        <!-- <a href="{{ route('indexPadre', ['NOMBRE' => $details->URL_CATEGORIA] )}}" class="cool-link"> -->
                                            <?=$details->NOMBRE?>
                                        </a>
                                    @else
                                        <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $details->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $details->URL_CATEGORIA])}}" class="cool-link" style="color: crimson;">
                                        <!-- <a href="{{ route('indexPadre', ['NOMBRE' => $details->URL_CATEGORIA] )}}" class="cool-link"> -->
                                            <?=$details->NOMBRE?>
                                        </a>
                                    @endif
                                </div>
                            @endforeach
                        @endforeach
                    @endif
                </ul>
            </ul>

            <div id="header-menu-icon-tools" class="col-auto col-movil">
                <!-- <label><input type="checkbox" id="menu" onchange="toggleCheckbox(this)">
                    <span for="menu" class="menu">
                        <img src="{{ asset('assets/global/img/icons/icono-menu.png') }}">
                    </span>
                </label> -->
                <label><input type="checkbox" id="menu" onchange="toggleCheckbox(this)"></label>
                <label for="menu" class="menu">
                    <span></span>
                    <span id="ham-mid"></span>
                    <span></span>
                </label>
            </div>
        </div>

        <div id="header-menu-brand" class="ma-logo">
            <h1 id="logo-muletta" class="ma-muletta">
                <a href="{{route('index')}}">
                    <div class="lrg-logo"></div>
                </a>
            </h1>
        </div>

        <?php
            $carrito = session()->get('carrito');
            $usuario = session()->get('perfilUsuario');
        ?>

        <div id="header-menu-text-tools">
            <div class="row">
                <div class="col-auto col-buscar" onclick="openNavOverlay('search-overlay-nav', '400px')">
                    <a class="menu-toggle">
                        <img src="{{ asset('assets/global/img/icons/icono-buscar.png') }}">
                    </a>
                </div>
                <div class="col-auto col-user">
                    @if(!$usuario)
                        <a href="{{route('login')}}" class="menu-toggle">
                            <span>
                                <img src="{{ asset('assets/global/img/icons/icono-perfil.png') }}">
                            </span>
                        </a>
                    @else
                        <a href="{{route('mi-cuenta')}}" class="menu-toggle" style="font-size: 17px; color:white;">
                            <?php
                                    $fullname = $usuario['usuario']->NOMBRE;
                                    $firstname = head(explode(' ', trim($fullname)));
                                ?>
                            &nbsp; {{ $firstname }}
                        </a>
                    @endif
                </div>
                <div class="col-auto col-carrito" onclick="openNavOverlay('carrito-overlay-nav','400px')">
                    <a class="menu-toggle">
                        <img src="{{ asset('assets/global/img/icons/icono-cart.png') }}">
                        <span class="badge">
                            {{ $carrito ? count($carrito->productos) : '0' }}
                        </span>
                    </a>
                </div>
                
            </div>
        </div>
    </header>

    @foreach($menu_PrimerNivel as $primer)
        @foreach($primer as $details)
            <?php
                $hijos = false;
                foreach($segundoNivel as $segundo){
                    foreach($segundo as $s){
                        if($details->ID_CATEGORIA_FLEXIBLE == $s->CATEGORIA_PADRE){
                            $hijos = true;
                        }
                    }
                }
            ?>
            @if($hijos)
                <div id="<?=$details->ID_CATEGORIA_FLEXIBLE?>" class="overlay overlays">
                    <div class="overlay-content" id="megamenu-mujer" onmouseleave="closeNavOverlay('<?=$details->ID_CATEGORIA_FLEXIBLE?>')">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNavOverlay('<?=$details->ID_CATEGORIA_FLEXIBLE?>')">&times;</a>
                        <div class="menu-wraper">
                            @foreach($segundoNivel as $segundo)
                                @foreach($segundo as $key => $s)
                                    @if($details->ID_CATEGORIA_FLEXIBLE == $s->CATEGORIA_PADRE)

                                        <?php
                                            $aux = 0;
                                            foreach($segundo as $t){
                                                if($s->ID_CATEGORIA_FLEXIBLE == $t->CATEGORIA_PADRE){
                                                    $aux++;
                                                }
                                            }
                                        ?>
                                        
                                        <div class="orden_<?=$s->SECUENCIA_MENU?> <?= $aux == 0 ? 'subir' : '' ?>">
                                            <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $s->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA])}}">
                                                <h3 class="{{ $s->ID_CATEGORIA_FLEXIBLE == 77 ? 'text-danger' : '' }} {{ $s->ID_CATEGORIA_FLEXIBLE == 90 || $s->ID_CATEGORIA_FLEXIBLE == 91 ? 'txt-muletta-oro' : '' }}"><?=$s->NOMBRE?></h3>
                                            </a>
                                            <hr class="hr-header">

                                            @foreach($segundo as $t)
                                                @if($s->ID_CATEGORIA_FLEXIBLE == $t->CATEGORIA_PADRE)
                                                    <div>
                                                        <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $t->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $t->URL_CATEGORIA])}}" class="{{ $t->ID_CATEGORIA_FLEXIBLE == 78 || $t->ID_CATEGORIA_FLEXIBLE == 79 ? 'text-danger' : '' }}">
                                                            <?=$t->NOMBRE?>
                                                        </a>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>

                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @endforeach
</div>




<div id="search-overlay-nav" class="overlay overlays_extend">
    <div class="overlay-content">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNavOverlayX('search-overlay-nav')">&times;</a>
        <div class="container mb-5 search-box g-max-width-445">

            <img id="search-icon" style="height: 30px;" src="https://muletta.com/assets/global/img/icons/icono-buscar.jpg">

            <form class="js-validate js-step-form ga-search-form" action="{{ Route('buscar_productos') }}" method="post">
                {{ csrf_field() }}
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <div class="ebx-search-box__inputs-wrapper"><input aria-label="Caja de búsqueda" maxlength="64" placeholder="Escribe lo que buscas" name="NOMBRE" inputmode="search" type="search" class="ebx-search-box__input ebx-search-box__input-query">
                </div>
                </br>
                </br>
                <button type="submit" class="btn btn-primary btn-sm ng-binding ng-isolate-scope">Buscar</button>
            </form>
        </div>
    </div>
</div>



<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="carrito-overlay-nav" class="overlay overlays_extend">
    <div class="overlay-content overlay-custom">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNavOverlayX('carrito-overlay-nav')">&times;</a>

        <div class="container scrolling-box">
            <br><br>
            <div class="shopping-cart">
                @if($carrito && $carrito->productos)
                    <?php $cantidad_total_productos = 0;?>
                    <ul class="shopping-cart-items">
                        @foreach($carrito->productos as $key => $producto)
                            <li class="clearfix ">
                                <button type="button" class="btn-remove-item" onclick="changedelete(<?=$key?>)">&times;</button>
                                <img data-src="{{ $producto['image'] }}" class="photo" alt="Image Description" width="100px;" />
                                <p class="item-name">{{ $producto['name'] }}</p>
                                <p class="item-quantity" style="margin-top: -10px;">Cantidad: {{ $producto['qty'] }}</p>
                                <p style="margin-top: -10px;">Talla: {{ $producto['talla'] }}</p>
                                <p style="margin-top: -10px;">Precio: ${{ $producto['total'] }} </p>
                            </li>
                            <?php
                                $cantidad_total_productos = $cantidad_total_productos + $producto['qty'];
                            ?>
                        @endforeach
                    </ul>
                    <div class="shopping-cart-total ">
                        <strong class="g-py-10 txt-muletta-gris-claro  g-py-10 g-px-5">{{$cantidad_total_productos }} Productos</strong>

                        <strong class="txt-muletta-gris g-px-5">TOTAL:</strong>
                        <strong class="txt-muletta-gris g-px-5">$<?=$carrito->total?></strong>
                        <br><br>
                        <a class="btn btn-block u-btn-black g-brd-black--hover g-bg-black--hover g-bg-primary g-font-size-12 text-uppercase rounded w-75" href="{{ Route('compras') }}">COMPRAR AHORA</a>
                    </div>
                    <br><br>
                @else
                    <h3>No tienes productos</h3>
                    <br><br>
                @endif
            </div>
        </div>
    </div>
</div>

<?php 
    foreach($menu_PrimerNivel as $primer){
        $cantidadCabeceras = count($primer); 
        if($cantidadCabeceras > 0){
            $widthHeaders = 100 / $cantidadCabeceras;
        }else{
            $widthHeaders = 25;
        }
    }
?>

<?php 
    $porcentajeMovimiento = 0;
    $nchil = 1;
    $stilo = '';
    foreach($menu_PrimerNivel as $primer){
        foreach($primer as $details){
            if($cantidadCabeceras > 4){
                $stilo = $stilo.'.radioInput:nth-of-type('.$nchil.'):checked ~ .tabContent-container {
                    transform: translateX(-'.$porcentajeMovimiento.'%);
                }
            
                .radioInput:nth-of-type('.$nchil.'):checked ~ .tabHeader-container .tabHeader:nth-of-type('.$nchil.') {
                    color: #b78b1e;
                }

                @media (max-width:768px){
                    .radioInput:nth-of-type('.$nchil.'):checked ~ .tabHeader-container .tabHeader:nth-of-type('.$nchil.'){
                        border-bottom: #b78b1e solid 2px;
                    }
                }
            
                .radioInput:nth-of-type('.$nchil.'):checked ~ .activeTab-indicator {
                    transform: translateX('.$porcentajeMovimiento.'%);
                }';
            }
            else{
                $stilo = $stilo.'.radioInput:nth-of-type('.$nchil.'):checked ~ .tabContent-container {
                    transform: translateX(-'.$porcentajeMovimiento.'%);
                }
            
                .radioInput:nth-of-type('.$nchil.'):checked ~ .tabHeader-container .tabHeader:nth-of-type('.$nchil.') {
                    color: #b78b1e;
                }
            
                .radioInput:nth-of-type('.$nchil.'):checked ~ .activeTab-indicator {
                    transform: translateX('.$porcentajeMovimiento.'%);
                }';
            }
            $porcentajeMovimiento = $porcentajeMovimiento + 100;
            $nchil = $nchil + 1;
        }
    }
    if($cantidadCabeceras > 4){
        $stilo = $stilo.'
        @media (max-width:768px){
            .activeTab-indicator{
                display:none;
            }
        }';
    }
?>

<style>
    <?=$stilo?>
</style>
    
<div id="mySidenav" class="{{ $cantidadCabeceras >= 9 ? 'demasiadasCabeceras' : '' }} sidenav hamburger_wrapper" style="{{ $cantidadCabeceras > 3 ? 'width: calc(100px * '.$cantidadCabeceras.');' : '' }}">
    <div class="sidenav-container">
        <?php
            $menu_PrimerNivel = session()->get('menuPrimerNivel');
        ?>
        @foreach($menu_PrimerNivel as $primer)
            @foreach($primer as $details)
                @if($loop->iteration == 1)
                    <input type="radio" name="radio" class="radioInput" id="tab{{$loop->iteration}}" checked hidden />
                @else
                    <input type="radio" name="radio" class="radioInput" id="tab{{$loop->iteration}}" hidden />
                @endif
            @endforeach
        @endforeach
        <div class="tabHeader-container">
            @foreach($menu_PrimerNivel as $primer)
                @foreach($primer as $details)
                    @if($details->NOMBRE != 'Rebajas')
                        <label for="tab{{$loop->iteration}}" class="tabHeader text-uppercase text-bold <?=$details->COLOR?>" style="width:<?=$widthHeaders?>%">
                            <?=$details->NOMBRE?>

                            <?php $tabcounter=$loop->iteration ?>

                        </label>
                    @endif
                    
                @endforeach
            @endforeach
        </div>

        <span class="activeTab-indicator" style="width: <?=$widthHeaders?>%"></span>

        <!-- @switch($tabcounter)
            @case(5)
                <span class="activeTab-indicator" style="width: 20%;"></span>
            @break
            @case(4)
                <span class="activeTab-indicator" style="width: 25%;"></span>
            @break
            @case(3)
                <span class="activeTab-indicator" style="width: 33%;"></span>
            @break
            @case(2)
                <span class="activeTab-indicator" style="width: 50%;"></span>
            @break
            @case(1)
                <span class="activeTab-indicator" style="width: 100%;"></span>
            @break
            @default
                <span class="activeTab-indicator"></span>
            @break
        @endswitch -->
        
        <div class="tabContent-container">
            
            @foreach($menu_PrimerNivel as $primer)
                @foreach($primer as $details)
                    <div class="tabContent tab{{$loop->iteration}}-content menu-wraper-mobile">
                        <div class="orden_Todo" style="width: 80%;">
                            <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $details->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $details->URL_CATEGORIA])}}">
                                <h3 class="{{ $details->ID_CATEGORIA_FLEXIBLE == 121 ? 'text-danger' : '' }} text-uppercase h3-header-mobile" style="font-weight: bold;">VER TODO</h3>
                            </a>
                            <hr class="hr-header-mobile">
                        </div>
                        <?php $iteradorSecueciaInexistente = 0; ?>
                       
                        @foreach($segundoNivel as $segundo)
                            
                            @foreach($segundo as $s)
                                @if($details->ID_CATEGORIA_FLEXIBLE == $s->CATEGORIA_PADRE)
                                    <?php
                                        $aux = 0;
                                        $aux2 = 0;
                                        foreach($segundoNivel as $tercer){
                                            foreach($segundo as $t){
                                                if($s->ID_CATEGORIA_FLEXIBLE == $t->CATEGORIA_PADRE){
                                                    $aux = 1;
                                                }
                                                if($t->SECUENCIA_MENU){
                                                    if($t->SECUENCIA_MENU > $iteradorSecueciaInexistente){
                                                        $iteradorSecueciaInexistente = $t->SECUENCIA_MENU;
                                                    }
                                                }
                                                
                                            }
                                        }
                                        
                                    ?>
                                    @if($s->SECUENCIA_MENU)
                                        @if($s->SECUENCIA_MENU > 7)
                                            <div class="orden_<?=$s->SECUENCIA_MENU?>" style="width: 80%; margin-left: 10%;">
                                                <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $s->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA])}}">
                                                    <h3 class="{{ $s->ID_CATEGORIA_FLEXIBLE == 121 ? 'text-danger' : '' }} text-uppercase h3-header-mobile text-bold <?=$s->COLOR?>"><?=$s->NOMBRE?></h3>
                                                </a>
                                                <hr class="hr-header-mobile">

                                                @foreach($segundoNivel as $tercer)
                                                    @foreach($segundo as $t)
                                                        @if($s->ID_CATEGORIA_FLEXIBLE == $t->CATEGORIA_PADRE)
                                                            <div>
                                                                <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $t->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $t->URL_CATEGORIA])}}" class="{{ $t->ID_CATEGORIA_FLEXIBLE == 78 || $t->ID_CATEGORIA_FLEXIBLE == 79 ? 'text-danger' : '' }}" style="color: #9c9b9b;">
                                                                    <?=$t->NOMBRE?>
                                                                </a>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </div>
                                        @else
                                            <div class="orden_<?=$s->SECUENCIA_MENU?>" style="width: 80%;">
                                                <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $s->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA])}}">
                                                    <h3 class="{{ $s->ID_CATEGORIA_FLEXIBLE == 121 || $s->ID_CATEGORIA_FLEXIBLE == 120 ? 'text-danger' : '' }} text-uppercase h3-header-mobile text-bold <?=$s->COLOR?>"><?=$s->NOMBRE?></h3>
                                                </a>
                                                <hr class="hr-header-mobile">

                                                @foreach($segundoNivel as $tercer)
                                                    @foreach($segundo as $t)
                                                        @if($s->ID_CATEGORIA_FLEXIBLE == $t->CATEGORIA_PADRE)
                                                            <div>
                                                                <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $t->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $t->URL_CATEGORIA])}}" class="{{ $t->ID_CATEGORIA_FLEXIBLE == 78 || $t->ID_CATEGORIA_FLEXIBLE == 79 ? 'text-danger' : '' }} <?=$t->COLOR?>" style="color: #9c9b9b;">
                                                                    <?=$t->NOMBRE?>
                                                                </a>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </div>
                                        @endif
                                        
                                    @else
                                        <?php $iteradorSecueciaInexistente = $iteradorSecueciaInexistente + 1; ?>
                                        <div class="orden_<?=$iteradorSecueciaInexistente?>" style="width: 80%; margin-left: 10%;">
                                            <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $s->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $s->URL_CATEGORIA])}}">
                                                <h3 class="{{ $s->ID_CATEGORIA_FLEXIBLE == 77 ? 'text-danger' : '' }} text-uppercase h3-header-mobile text-bold <?=$s->COLOR?>"><?=$s->NOMBRE?></h3>
                                            </a>
                                            <hr class="hr-header-mobile">

                                            @foreach($segundoNivel as $tercer)
                                                @foreach($segundo as $t)
                                                    @if($s->ID_CATEGORIA_FLEXIBLE == $t->CATEGORIA_PADRE)
                                                        <div>
                                                            <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $t->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $t->URL_CATEGORIA])}}" class="{{ $t->ID_CATEGORIA_FLEXIBLE == 78 || $t->ID_CATEGORIA_FLEXIBLE == 79 ? 'text-danger' : '' }} <?=$t->COLOR?>" style="color: #9c9b9b;">
                                                                <?=$t->NOMBRE?>
                                                            </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </div>
                                    @endif
                                    
                                @endif
                            @endforeach
                                
                        @endforeach
                        @foreach($primer as $details2)
                            @if($details2->NOMBRE == 'Rebajas')
                                <div class="orden_ultima" style="width: 80%;">
                                    <a href="{{ route('productos_por_categoria', ['ID_MODELO_TIPO' => $details2->ID_CATEGORIA_FLEXIBLE, 'URL_TIPO_PRODUCTO' => $details2->URL_CATEGORIA])}}" class="text-uppercase h3-header-mobile cool-link" style="color: crimson; margin-bottom: 0.5rem">
                                    <!-- <a href="{{ route('indexPadre', ['NOMBRE' => $details->URL_CATEGORIA] )}}" class="cool-link"> -->
                                        <?=$details2->NOMBRE?>
                                    </a>
                                    <hr class="hr-header-mobile">
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endforeach
            @endforeach
        </div>

    </div>
</div>
