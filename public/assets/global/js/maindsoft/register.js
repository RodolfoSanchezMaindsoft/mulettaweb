$('#error_email').hide();
$('#error_Fecha').hide();
$('#error_nombre').hide();
$('#error_telefono').hide();
$('#error_terminos').hide();
$('#error_contraseña').hide();
$('#error_Fecha_valid').hide();
$('#error_contraseña_min').hide();

$('#register_user').submit(function(event) {
    // event.preventDefault();
    var errores = false;
    if($('#terminos').prop('checked') == false){
        $('#error_terminos').show();
        errores = true;
    }

    if ($('#email').val() == '') {
        $('#error_email').show();
        errores = true;
    }

    if ($('#nombre').val() == '' || ($('#apellidoP').val() == '' && $('#apellidoM').val() == '')) {
        $('#error_nombre').show();
        errores = true;
    }

    if ($('#telefono').val() == '') {
        $('#error_telefono').show();
        errores = true;
    }

    var day = $('#dia').val();
    var month = $('#mes').val();
    var year = $('#anio').val();

    if (day != '' || month != '' || year != '') {
        if( (year <= 1000) || (year >= 3000) || (month == 0) || (month > 12) ){
            $('#error_Fecha_valid').show();
            errores = true;
        }
        
        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Ajustar para los años bisiestos
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)){
            monthLength[1] = 29;
        }
        // Revisar el rango del dia
        if(day < 0 || day > monthLength[month - 1]){
            $('#error_Fecha_valid').show();
            errores = true;
        }
    }


    if ($('#password-field').val() == '') {
        $('#error_contraseña').show();
        errores = true;
    }else if ($('#password-field').val().length <= 7){
        $('#error_contraseña').hide();
        $('#error_contraseña_min').show();
        errores = true;
    }

    if (errores == true) {
        return false;
    }else{
        $('#error_terminos').hide();
        return true;
    }
});