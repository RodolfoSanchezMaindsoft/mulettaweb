$(document).ready(function() {
    rangeNew(0, 2501);

    window.addEventListener('scroll', function(evt) {
        if (window.scrollY >= 100) {
            $('#menu-categoria').addClass('scrollMenu');
        }else{
            $('#menu-categoria').removeClass('scrollMenu');
        }
    }, false);
});

function mostrarFiltros(){
    if ($("#menu-categoria").is(':hidden')) {
        $('#menu-categoria').slideDown();
    } else {
        $('#menu-categoria').slideUp();
    }
}

function changeTipo(){
    var url_tallas = $('#url_tallas').val();

    var marca = '';
    var genero = '';
    var productos = '';
    var tipo = '';
    var tallas = '';
    var precio_min = $("#slider-3").slider("values", 0);
    var precio_max = $("#slider-3").slider("values", 1);

    $( "input[name='marca']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            marca = marca + $(this).val() + ', ';
        }
    });

    $( "input[name='genero']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            genero = genero + $(this).val() + ', ';
        }
    });

    $( "input[name='productos']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            productos = productos + $(this).val() + ', ';
        }
    });

    $( "input[name='tipo']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            tipo = tipo + $(this).val() + ', ';
        }
    });

    $( "input[name='tallas']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            if ($(this).val() > 0) {
                tallas = tallas + $(this).val() + ', ';
            }
        }
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url_tallas,
        data: {
            marca: marca.substring(0, marca.length - 2),
            genero: genero.substring(0, genero.length - 2),
            productos: productos.substring(0, productos.length - 2),
            tipo: tipo.substring(0, tipo.length - 2),
            tallas: tallas.substring(0, tallas.length - 2),
            precio_min: precio_min,
            precio_max: precio_max
        },
        dataType: 'json',
        success: function( msg ) {
            $("#menu-categoria").empty();
            $("#menu-categoria").append(msg.html);
            rangeNew(precio_min, precio_max);
        }
    });
}

function changeCheckbox(position){
    if (position == 1) {
        $('#tallas-todas').prop("checked", false);
    }else{
        $('input[name="tallas"]').prop("checked", false);
        $('#tallas-todas').prop("checked", true);
    }
}

$('#NOMBRE').keypress(function(e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    if (keycode == '13') {
        buscar();
        return false;
    }
});

function buscar(){
    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    var genero = '';
    var productos = '';
    var tipo = '';
    var tallas = '';
    var nombre = '';
    var banner = '';
    var precio_min = $("#slider-3").slider("values", 0);
    var precio_max = $("#slider-3").slider("values", 1);

    var regreso = false;
    if ($('#NOMBRE').val()) {
        nombre = $('#NOMBRE').val()
        regreso = true;
    }

    if ($('#fltro-categoria').val()){
        banner = $('#fltro-categoria').val()
        regreso = true;
    }

    $( "input[name='genero']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            genero = genero + $(this).val() + ', ';
            regreso = true;
        }
    });

    $( "input[name='productos']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            productos = productos + $(this).val() + ', ';
            regreso = true;
        }
    });

    $( "input[name='tipo']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            tipo = tipo + $(this).val() + ', ';
            regreso = true;
        }
    });

    $( "input[name='tallas']" ).each(function() {
        var checked = $(this).is(':checked');
        if (checked) {
            if ($(this).val() > 0) {
                tallas = tallas + $(this).val() + ', ';
                regreso = true;
            }
        }
    });

    if (precio_min >= 0 && precio_max <= 2501) {
        regreso = true;
    }

    if (regreso) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: url + '/productos/filtros',
            data: {
                genero: genero.substring(0, genero.length - 2),
                productos: productos.substring(0, productos.length - 2),
                tipo: tipo.substring(0, tipo.length - 2),
                tallas: tallas.substring(0, tallas.length - 2),
                precio_min: precio_min,
                precio_max: precio_max,
                nombre: nombre,
                banner: banner
            },
            dataType: 'json',
            success: function( msg ) {
                $("#div_productos").empty();
                $("#div_productos").append(msg.html);
                lazyLoadInstance.update();

                cerrar_menu();
            }
        });
    } else {
        location.reload();
    }
}

function cerrar_menu(){
    $('#menu-categoria').slideUp();
}
function abrir_menu(){
    $('#menu-categoria').slideDown();
}

function rangeNew(precio_min, precio_max){
    $( "#slider-3" ).slider({
        range:true,
        min: 0,
        max: 2501,
        values: [ precio_min, precio_max ],
        slide: function( event, ui ) {
            var valor;
            if (ui.values[1] == 2501) {
                valor = "+$2500";
            }else{
                valor = "$" + ui.values[1];
            }
           $( "#price_min" ).text("$" + ui.values[0]);
           $( "#price_max" ).text(valor);
        }
    });
    
    $("#price_min").text( "$" + $("#slider-3").slider("values", 0) );
    if ($("#slider-3").slider("values", 1) == 2501) {
        $( "#price_max" ).text( "+$2500");
    }else{
        $( "#price_max" ).text( "$" + $("#slider-3").slider("values", 1) );
    }
}

function ocultarDiv(){
    $('#div_primero').hide();
}