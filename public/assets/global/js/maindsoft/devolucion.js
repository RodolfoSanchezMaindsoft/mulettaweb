/*Esconde elementos de paso 2 - direcciones al cargar pagina y manda llamar PayPal*/
$(document).on('ready', function () {
    $('.loader').hide();

    initStepForm();
});



/*Esconde pestañas 2 y 3  y mensajes de error del step form - */
function initStepForm() {
    $("#firstStep").addClass("active");
    $("#step2").hide();
    $("#step3").hide();
    $("#alias_user-error").hide();
    $("#street_user-error").hide();
    $("#num_ext_user-error").hide();
    $("#col_user-error").hide();
    $("#code_postal_user-error").hide();
    $("#mun_user-error").hide();
    $("#state_user-error").hide();

}

function changeup(e) {
    var total = $('#total_'+e).val();
    var cantidad = $('#cantidad_'+e).val();
    var precio_unitario = $('#precio_unitario_'+e).val();

    if (total > cantidad) {
        cantidad++;
    }
    $('#cantidad_'+e).val(cantidad);
    $('#precio_total_label_'+e).text((precio_unitario * cantidad).toFixed(2));
    $('#precio_total_'+e).val((precio_unitario * cantidad).toFixed(2));
}

function changedown(e) {
    var cantidad = $('#cantidad_'+e).val();
    var precio_unitario = $('#precio_unitario_'+e).val();

    if (cantidad > 0) {
        cantidad--;
    }
    $('#cantidad_'+e).val(cantidad);
    $('#precio_total_label_'+e).text((precio_unitario * cantidad).toFixed(2));
    $('#precio_total_'+e).val((precio_unitario * cantidad).toFixed(2));
}



$("#guardar").on( 'change', function() {
    if( $(this).is(':checked') ) {
        // Hacer algo si el checkbox ha sido seleccionado
        $(this).val(1);
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $(this).val(0)
    }
});




/*Boton Paso 1 - PROCESAR COMPRA - Esconde primer pestaña y muestra segunda pestaña*/
$("#procesar").on("click", function () {
 
    $("#secondStep").addClass("active");
    $("#firstStep").removeClass("active");
    $("#firstStep").addClass("g-checked");
    $("#step1").fadeOut(200);
    $("#step2").fadeIn(200);


});

/*Boton Paso 2 - PROCEDER A PAGAR - Esconde segunda pestaña y muestra tercer pestaña*/
function secondStep() {
    var venta = $('#id_venta').text();
    var total = 0;

    var productos = [];
    
    $.each($('.no_parte'), function(index) {

        var cantidad = $('#cantidad_'+index).val();

        var producto = {
            'ID_VENTA' : venta,
            'ID_DEVOLUCION' : '',
            'NO_PARTE': $(this).val() ,
            'CANTIDAD' : cantidad,
        };

        if ( $('#devolucion_'+index).prop('checked') && cantidad >= 1 ) {  
            productos.push(producto);
        }
    });

    $.each($('.total'), function() {
      var sub = $(this).val();
      total = Number(total) + Number(sub);
    });

    var motivo = $('#motivo_user').val();

    if (motivo == null) {
        Swal.fire({
            title: 'Error',
            text: 'Favor de seleccionar un motivo',
            icon: 'error',
            confirmButtonColor: '#b78b1e',
            confirmButtonText: 'Aceptar'
        });
        return;
    }

    if (productos.length > 0) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: "POST",
            url: "insertReturn",
            dataType: 'json',
            data: {
                'ID_VENTA': venta,
                'RAZONES': '',
                'ID_MOTIVO': motivo,
                'TOTAL': total,
                'PRODUCTOS': productos
            },
            success: function (msg) {
                var tableBody = '';
                
                for (let index = 0; index < msg.length; index++) {
                    const element = msg[index];

                    tableBody = tableBody + `<tr class="g-brd-bottom g-brd-gray-light-v3 g-mb-25 text-uppercase">
                        <td class="text-center g-py-25">
                            <img src="${ element.IMAGEN }" width="150" height="150" alt="Logo-Muletta">
                        </td>
                        <td class="text-left g-py-25 txt-muletta-oro" style="font-size: 2vh;">
                            <div> ${ element.DESCRIPCION }</div>
                            <div  style="font-size: 1.5vh;">${ element.ID_MODELO }</div>
                        </td>
                        <td class="text-center g-py-25" style="font-size: 2vh;">
                            ${ element.DESCRIPCION_TALLA }
                        </td>
                        <td class="text-center g-py-25" style="font-size: 2vh;">
                            ${ element.CANTIDAD_VENDIDA }
                        </div>
                        </td>
                        <td class="text-center g-py-25" style="font-size: 2vh;">
                            $${ element.PRECIO_SIN_DESC.toFixed(2) }
                        </td>
                        <td class="text-center g-py-25 text-danger" style="font-size: 2vh;">
                            $${ (element.PRECIO_SIN_DESC - element.PRECIO_ORIGINAL).toFixed(2) }
                        </td>
                        <td class="text-center g-py-25" style="font-size: 2vh;">
                            $${ (element.PRECIO_ORIGINAL * element.CANTIDAD_VENDIDA).toFixed(2) }
                        </td>
                    </tr>`;
                }

                $('#tableBody').append(tableBody);

                $("#finaltStep").addClass("active");
                $("#secondStep").removeClass("active");
                $("#secondStep").addClass("g-checked");
                $("#step2").fadeOut(200);
                $("#step3").fadeIn(200);
            },
            error: function(error){
                console.log(error);
                Swal.fire({
                    title: 'Error',
                    text: 'Ha sucedido un error favor de intentar mas tarde',
                    icon: 'error',
                    confirmButtonColor: '#b78b1e',
                    confirmButtonText: 'Aceptar'
                });
            }
        });
    }else{
        Swal.fire({
            title: 'Error',
            text: 'Favor de seleccionar los productos a devolver',
            icon: 'error',
            confirmButtonColor: '#b78b1e',
            confirmButtonText: 'Aceptar'
        });
    }
}

$('#terminar').click(function() {
    var url = $('#generarPDF').val();
    var urlPedidos = $('#urlPedidos').val();
    window.open(url, '_blank');
    window.location.href = urlPedidos;
});
    

$( "#motivo_user" ).change(function() {
    var motivo = $('#motivo_user option:selected').text();
    $('#motivo_cambio').text( motivo );
});