$(document).ready(function() {

    // PRODUCCION
    OpenPay.setId('miklqroudmb4oylrs8zd');
    OpenPay.setApiKey('pk_ec2498d31c4a4b4982bbfda55c3dffa2');
    OpenPay.setSandboxMode(false);
    
    // SANDBOX
    // OpenPay.setId('mjyjulr6kq82w7ibjujz');
    // OpenPay.setApiKey('pk_544ed717bf7040a28e1ffb4d802a7394');
    // OpenPay.setSandboxMode(true);

    //Se genera el id de dispositivo
    var deviceSessionId = OpenPay.deviceData.setup("payment-form");
    $('#device_id').val(deviceSessionId);

    $('#pay-button').on('click', function(event) {
        event.preventDefault();
        $("#pay-button").prop( "disabled", true);
        OpenPay.token.extractFormAndCreate('payment-form', success_callbak, error_callbak);
    });
    
    var success_callbak = function(response) {
        var token_id = response.data.id;
        $('#token_id').val(token_id);
        $('#payment-form').submit();
    };
    
    var error_callbak = function(response) {
        var desc = response.data.description != undefined ?
           response.data.description : response.message;

        console.log(response);
           
        Swal.fire({
            title: "ERROR [" + response.status + "] ",
            html: desc,
            icon: 'error',
            confirmButtonColor: '#b78b1e',
            confirmButtonText: 'Aceptar'
        });

        $("#pay-button").prop("disabled", false);
    };

    if ( $("#error-modal") ) {
        $('#error-modal').modal('show');
    }
});

function closeModal(){
    $('#error-modal').modal('hide');
}