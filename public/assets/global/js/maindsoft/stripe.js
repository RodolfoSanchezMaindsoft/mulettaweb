var stripe = Stripe('pk_test_51Ha9s4LJcz9vhBCvhDgZ09v38yJaiiSAmMMl4nBpogVYXRL20qXmrw3B0MZsGxFut4ZMQvofGfJ1MnarGiuXlOE4007BkPymrS');
// var stripe = Stripe('pk_live_51Ha9s4LJcz9vhBCvukD42qr788rNB5XVSUYofvKGCe4K4Ad6CYpC4z7ccoRdfLSLtTgHdC9fpbr621g5WhDCW2Qb00Nirj55qq');

var elements = stripe.elements();


// Custom styling can be passed to options when creating an Element.
var style = {
    base: {
      iconColor: '#666EE8',
      color: '#31325F',
      lineHeight: '40px',
      fontWeight: 300,
      fontFamily: 'Helvetica Neue',
      fontSize: '15px',
      widht: '200px',
      '::placeholder': {
        color: '#CFD7E0',
      },
    },
};
  
// Create an instance of the card Element.
// var card = elements.create('card', {style: style});
// Add an instance of the card Element into the `card-element` <div>.
// card.mount('#card-element');

var cardNumberElement = elements.create('cardNumber', {
  style: style,
  placeholder: 'NÚMERO DE TARJETA',
});
cardNumberElement.mount('#card-number-element');

var cardExpiryElement = elements.create('cardExpiry', {
  style: style,
  placeholder: 'MM/AA',
});
cardExpiryElement.mount('#card-expiry-element');

var cardCvcElement = elements.create('cardCvc', {
  style: style,
  placeholder: 'CVC',
});
cardCvcElement.mount('#card-cvc-element');

var SecurityId = '';

function stripePago(){
    $('#btn_stripe').attr("disabled", true);

    var custData = {
        name: $('#cardName').val(),
        address_line1: $('input[name="street_user"]').val(),
        address_city: $('[name="mun_user"]').val(),
        address_state: $('[name="state_user"]').val(),
        address_zip: $('input[name="code_postal_user"]').val(),
        address_country: 'MX'
    };

    stripe.createToken(cardNumberElement, custData).then(function(result) {
        if (result.error) {
          // Inform the customer that there was an error.
          var errorElement = document.getElementById('card-errors');
          errorElement.innerHTML = result.error.message;
          $('#btn_stripe').attr("disabled", false);
        } else {
            // Send the token to your server.
            var id_venta = $('#id_venta').val();
            if (id_venta && id_venta != null) {
                stripeTokenHandler(id_venta, result.token);
            }else{
                completarPago(result.token);
            }
        }
    });
}

function completarPago(token) {
    var ID_CLIENTE = $('input[name="id_user"]').val();
    var EMAIL = $('#email').val();
    var street_user = $('input[name="street_user"]').val();
    var num_int_user = $('input[name="num_int_user"]').val();
    var num_ext_user = $('input[name="num_ext_user"]').val();
    var col_user = $('input[name="col_user"]').val();
    var code_postal_user = $('input[name="code_postal_user"]').val();
    var mun_user = $('[name="mun_user"]').val();
    var state_user = $('[name="state_user"]').val();
    var id_desc = $('input[name="id_desc"]').val();
    var monto_desc = $('input[name="monto_desc"]').val();
    var regalo = $('input[name="regalo"]').val();
    var alias = $('input[name="alias_user"]').val();
    var referencia = $('textarea[name="referencia"]').val();
    var guardar = $('input[name="guardar"]').val();

    $('.loader').show();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: "CompletarPago",
        dataType: 'json',
        data: {
            ID_CLIENTE: ID_CLIENTE,
            EMAIL: EMAIL,
            street_user: street_user,
            num_int_user: num_int_user,
            num_ext_user: num_ext_user,
            col_user: col_user,
            code_postal_user: code_postal_user,
            mun_user: mun_user,
            state_user: state_user,
            id_desc: id_desc,
            monto_desc: monto_desc,
            regalo: regalo,
            alias: alias,
            guardar: guardar,
            referencia: referencia,
            metodo_pago: 15
        },
        success: function(order){
            if (order.id_venta) {
                $('#id_venta').val(order.id_venta);
                setTimeout(() => {
                    stripeTokenHandler(order, token);
                }, 500);
            }else{
                $('#error_tarjeta').hide();
                $('#errores_principales').show();
                $('#error-modal').modal('show');
            }
        },
        error: function(request, status, error){
            $('#error_tarjeta').hide();
            $('#errores_principales').show();
            $('#error-modal').modal('show');

            console.log( 'jqXHR', jqXHR );
            console.log( 'textStatus', textStatus );
            console.log( errorThrown );
        }
    });
}



function stripeTokenHandler(order, token) {
    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }
    
    var id_venta = $('#id_venta').val();
    var id_user = $('input[name="id_user"]').val();
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: url + "/compras/realizarPagoStripeSecure/"+id_venta+"/"+id_user,
        dataType: 'json',
        data: { TOKEN: token.id },
        success: function( msg ) {
            $('.loader').hide();

            SecurityId = msg.client_secret;

            var custData = {
                name: $('#cardName').val()
            };

            stripe.confirmCardPayment(
                SecurityId, 
                {
                    payment_method: {
                        card: cardNumberElement,
                        billing_details: custData
                    },
                    return_url: url + "/compras/finalizarStripeSecure/"+id_venta+"/"+id_user,
                },
                // Disable the default next action handling.
                {handleActions: false}
            ).then(function(result) {
                // Handle result.error or result.paymentIntent
                // More details in Step 2.
                if(result.error){
                    $('#error_tarjeta').show();
                    $('#errores_principales').hide();
                    $('#message-error').text(result.error.message);
                    $('#error-modal').modal('show');
                    return;
                }

                if(result.paymentIntent['status'] == 'requires_action'){
                    var iframe = document.createElement('iframe');
                    iframe.src = result.paymentIntent.next_action.redirect_to_url.url;
                    iframe.width = 700;
                    iframe.height = 800;
                    document.getElementById('security-body').appendChild(iframe);
                    $('#security-modal').modal('show');
                }else if(result.paymentIntent['status'] == 'succeeded'){
                    Swal.fire({
                        title: 'Listo',
                        text: 'Pago procesado',
                        icon: 'success',
                        confirmButtonColor: '#b78b1e',
                        confirmButtonText: 'Aceptar'
                    });

                    window.location.href = url + "/compras/detailsVenta/"+id_venta;
                }
            }).catch(function(error) {
                // MANEJO DE ERRORES STRIPE
                $('#error_tarjeta').hide();
                $('#errores_principales').show();
                $('#error-modal').modal('show');
            });

        },
        error: function( jqXHR, textStatus, errorThrown ) {
            console.log( 'jqXHR', jqXHR );
            console.log( 'textStatus', textStatus );
            console.log( errorThrown );

            $('#error_tarjeta').hide();
            $('#errores_principales').show();
            $('#error-modal').modal('show');
        }
    });
}

function on3DSComplete() {
    // Hide the 3DS UI
    document.getElementById('security-body').remove();
    $('#security-modal').modal('hide');

    // Check the PaymentIntent
    stripe.retrievePaymentIntent(SecurityId)
    .then(function(result) {
        
        if (result.error) {
            // PaymentIntent client secret was invalid
            $('#error_tarjeta').show();
            $('#errores_principales').hide();
            $('#message-error').text(result.error.message);
            $('#error-modal').modal('show');
        } else {
            var protocol = window.location.protocol;
            var host = window.location.hostname;
            var port = window.location.port;
            var url = protocol+"//" + host
            if (port) {
                url = url + ":" + port
            }

            if (result.paymentIntent.status == 'succeeded') {
                // Show your customer that the payment has succeeded
                Swal.fire({
                    title: 'Listo',
                    text: 'Pago procesado',
                    icon: 'success',
                    confirmButtonColor: '#b78b1e',
                    confirmButtonText: 'Aceptar'
                });

                var id_venta = $('#id_venta').val();
                window.location.href = url + "/compras/detailsVenta/"+id_venta;

            } else if (result.paymentIntent.status == 'requires_payment_method') {
                // Authentication failed, prompt the customer to enter another payment method
                $('#error_tarjeta').show();
                $('#errores_principales').hide();
                $('#message-error').text(result.paymentIntent.last_payment_error.message);
                $('#error-modal').modal('show');
            }
        }
    }).catch(function(err){
        $('#error_tarjeta').hide();
        $('#errores_principales').show();
        $('#error-modal').modal('show');
    });
}

window.addEventListener('message', function(ev) {
    if (ev.data == '3DS-authentication-complete') {
        on3DSComplete();
    }
}, false);
