$(document).on('ready', function() {

    $('.icon-arrow-left').addClass('fa');
    $('.icon-arrow-left').addClass('fa-chevron-left');
    $('.icon-arrow-left').removeClass('icon-arrow-left');
    $('.icon-arrow-right').addClass('fa');
    $('.icon-arrow-right').addClass('fa-chevron-right');
    $('.icon-arrow-right').removeClass('icon-arrow-right');
    $('#error_Fecha_valid').hide();

    $('#tel').prop("readonly", true);
    $('#nombre').prop("readonly", true);
    $('#apellidoP').prop("readonly", true);
    $('#apellidoM').prop("readonly", true);
    $('#email').prop("readonly", true);
    $('#btn-actualizar').prop("disabled", true);
    
    $('#dia').prop("readonly", true);
    $('#mes').prop("readonly", true);
    $('#anio').prop("readonly", true);

    if ($('#tel').val() == '') {
        $('#error_telefono').show();
    }else{
        $('#error_telefono').hide();
    }
});

$( "#from_user" ).submit(function( event ) {
    event.preventDefault();
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var day = $('#dia').val();
    var month = $('#mes').val();
    var year = $('#anio').val();
    var nombre = $('#nombre').val();
    var apellidoP = $('#apellidoP').val();
    var apellidoM = $('#apellidoM').val();

    if (regex.test($('#email').val().trim())) {

        if (apellidoP == '' && apellidoM == '') {
            if (apellidoP == '') {
                $('#apellidoP').css('border-bottom-color', 'red');
            }
            if (apellidoM == '') {
                $('#apellidoM').css('border-bottom-color', 'red');
            }
        }

        if (day == '' || month == '' || year == '' || nombre == '' || (apellidoP == '' && apellidoM == '')) {
            Swal.fire({
                title: 'Favor de completar la información faltante',
                icon: 'error',
                confirmButtonColor: '#b78b1e',
                confirmButtonText: 'Aceptar'
            });
            return false;
        }

        if ($('#tel').val() == '') {
            $('#error_telefono').show();
            return false;
        }

        $(this).off('submit').submit();
        return true;
    } else {
        return false;
    }
});


$('#btn-tel').click(function() {
    $('#tel').prop("readonly", false);
    $('#btn-actualizar').prop("disabled", false);
});

$('#btn-nombre').click(function() {
    $('#nombre').prop("readonly", false);
    $('#apellidoP').prop("readonly", false);
    $('#apellidoM').prop("readonly", false);
    $('#btn-actualizar').prop("disabled", false);
});

$('#btn-email').click(function() {
    $('#email').prop("readonly", false);
    $('#btn-actualizar').prop("disabled", false);
});

$('#btn-datepicker').click(function() {
    $('#dia').prop("readonly", false);
    $('#mes').prop("readonly", false);
    $('#anio').prop("readonly", false);
    
    $('#btn-actualizar').prop("disabled", false);
});

$('#btn-contrasenia').click(function() {
    $('#form-pass')[0].reset();
    $('#modalCheckout').modal('show');
    $('#txt-contrasenia').hide();
    $('#txt-password_ant').hide();
    $('#txt-password_valid').hide();
    $('#txt-password_ant_valid').hide();
    $('#txt-password_valid_length').hide()
    $('#txt-repeat_password_valid').hide();
});

$('#repeat_password').on('input',function() {
    var pass = $('#password').val();
    var r_pass = $('#repeat_password').val();
    
    if (pass == r_pass) {
        $('#txt-contrasenia').hide();
    }else{
        $('#txt-contrasenia').show();
    }
});

$('#apellidoP').on('input',function() {
    $('#apellidoP').css('border-bottom-color', '#9c9b9b');
    $('#apellidoM').css('border-bottom-color', '#9c9b9b');
});
$('#apellidoM').on('input',function() {
    $('#apellidoP').css('border-bottom-color', '#9c9b9b');
    $('#apellidoM').css('border-bottom-color', '#9c9b9b');
});


var auxiliar = false;
$('#btn_contrasenia_update').click(function() {
    var ant = $('#password_ant').val();
    var pass = $('#password').val();
    var r_pass = $('#repeat_password').val();
    if (ant == null || ant == '' ) {
        auxiliar = true;
        $('#txt-password_ant_valid').show()
    }
    if (pass == null || pass == '' ) {
        auxiliar = true;
        $('#txt-password_valid').show()
    }
    if (r_pass == null || r_pass == '' ) {
        auxiliar = true;
        $('#txt-repeat_password_valid').show()
    }

    if (!auxiliar) {
        enviarFormulario();
    }
});

$('#password').on('input',function() {
    var pass = $('#password').val();
    var r_pass = $('#repeat_password').val();

    if (pass.length < 8) {
        auxiliar = true;
        $('#txt-password_valid_length').show()
    }else{
        auxiliar = false;
        $('#txt-password_valid_length').hide()
    }

    
    if (pass == r_pass) {
        $('#txt-contrasenia').hide();
    }else{
        $('#txt-contrasenia').show();
    }
});

function enviarFormulario(){
    var protocol = window.location.protocol;
    var host = window.location.hostname;
    var port = window.location.port;
    var ant = $('#password_ant').val();
    var pass = $('#password').val();
    var id = $('#id_cliente').val();
    
    var url = protocol+"//" + host
    if (port) {
        url = url + ":" + port
    }

    url = url + '/usuario/contrasenia';
    
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "post",
        url: url,
        dataType: 'json',
        data: {
            "id": id,
            "pass_ant": ant,
            "pass": pass
        },
        success: function(msg) {            
            if (msg[0]["ID_CLIENTE"]) {
                $('#modalCheckout').modal('hide');
                Swal.fire({
                    title: 'Contraseña actualizada',
                    icon: 'success',
                    confirmButtonColor: '#b78b1e',
                    confirmButtonText: 'Aceptar'
                });

            }else{
                $('#txt-password_ant').show()
            }
        },
        error: function() {
        }
    });
}

$("#dia").keyup(function(event){
    validardia();
});
$("#mes").keyup(function(event){
    var month = $('#mes').val();
    if (month != '' ) {
        if( (month > 0) && (month <= 12) ){
            $('#error_Fecha_valid').hide();
        }else{
            $('#error_Fecha_valid').show();
        }
        validardia();
    }else{
        $('#error_Fecha_valid').show();
    }
});
$("#anio").keyup(function(event){
    var year = $('#anio').val();

    if (year != '') {
        if( (year >= 1000) && (year <= 3000) ){
            $('#error_Fecha_valid').hide();
        }else{
            $('#error_Fecha_valid').show();
        }
        validardia();
    }else{
        $('#error_Fecha_valid').show();
    }
});

function validardia(){
    var day = $('#dia').val();
    var month = $('#mes').val();
    var year = $('#anio').val();
    var errores = false;

    if (day != '') {
        if( (year <= 1000) || (year >= 3000) || (month == 0) || (month > 12) ){
            $('#error_Fecha_valid').show();
            errores = true;
        }
        
        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

        // Ajustar para los años bisiestos
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)){
            monthLength[1] = 29;
        }
        // Revisar el rango del dia
        if (!errores) {
            $('#error_Fecha_valid').hide();
            if(day < 0 || day > monthLength[month - 1]){
                $('#error_Fecha_valid').show();
            }
        }
    }else{
        $('#error_Fecha_valid').show();
    }
}