function changedelete(index) {

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: "POST",
        url: "<?= Route('deleteProd')?>",
        dataType: 'json',
        data: {
            position: index
        },
        success: function(msg) {
            location.reload(true);
        }
    });
}

$(function() {
    var logo = $(".lrg-logo");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 50) {
            if (!logo.hasClass("sml-logo")) {
                logo.hide();
                logo.removeClass('lrg-logo').addClass("sml-logo").fadeIn("slow");
            }
        } else {
            if (!logo.hasClass("lrg-logo")) {
                logo.hide();
                logo.removeClass("sml-logo").addClass('lrg-logo').fadeIn("slow");
            }
        }

    });
});

function openNavOverlay(id, wd) {
    var idToOpen = id;
    var widthToOpen = wd;

    if(id === "carrito-overlay-nav"){
        gtag("event", "search-open", {
            "event_category" : "product",
            "event_label" : "Búsqueda de producto abierta"
        });
        fbq("track", "Búsqueda de producto abierta");

    }

    document.getElementById(idToOpen).style.width = widthToOpen;
}

function OpenNavCustom(id) {
    var idToOpen = id;
    var heigthToOpen = 'auto';
    var anterior = $('#categoria_anterior').val();
    if (anterior) {
        document.getElementById(anterior).style.height = "0%";
    }
    document.getElementById(idToOpen).style.height = heigthToOpen;
    $('#categoria_anterior').val(idToOpen);
}

function closeNavOverlay(menuIdtoClose) {
    var idToClose = menuIdtoClose;
    document.getElementById(idToClose).style.height = "0%";
}

function closeNavOverlayX(menuIdtoClose) {
    var idToClose = menuIdtoClose;
    document.getElementById(idToClose).style.width = "0%";
}

function toggleCheckbox(element) {
    if (element.checked) {
        openNav();
    } else if (!element.checked) {
        closeNav();
    }

}

function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
    // document.getElementById("main").style.marginRight = "100%";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    // document.getElementById("main").style.marginRight = "0";

}
/*acordeon js*/
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "list-item") {
            panel.style.display = "none";
        } else {
            panel.style.display = "list-item";
        }
    });
}

var on_submit_function = function(evt){
    evt.preventDefault();

    gtag("event", "search-submit", {
        "event_category" : "product",
        "event_label" : "Búsqueda de producto realizada"
    });
    fbq("track", "Búsqueda de producto realizada");


    $(this).off("submit", on_submit_function); // Remove this handle and submit the form again.
    $(this).submit();
};
$(".ga-search-form").on("submit", on_submit_function);

function capitalizeString(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}