  $(function() {
    $(window).scroll(function() {
        let anchuraVentana = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        const a = $('.sidenab');
        let scrollTriggerLvl = 1500;

        if(anchuraVentana > 700 && anchuraVentana < 849){
            scrollTriggerLvl = 1500;
        }else if(anchuraVentana > 850 && anchuraVentana < 1023){
            scrollTriggerLvl = 2000;
        }else if(anchuraVentana > 1024 && anchuraVentana < 1279){
            scrollTriggerLvl = 2200;
        }else if(anchuraVentana > 1280 && anchuraVentana < 1365){
            scrollTriggerLvl = 2500;
        }else if(anchuraVentana > 1366 && anchuraVentana < 1499){ 
                scrollTriggerLvl = 2800;
        }else if(anchuraVentana > 1500 && anchuraVentana < 1899){ 
                scrollTriggerLvl = 3000;
        }else if(anchuraVentana > 1900 && anchuraVentana < 2499){ 
                scrollTriggerLvl = 4000;
        }else if(anchuraVentana > 2500){ 
                scrollTriggerLvl = 5000;
        }

        if ($(this).scrollTop() > (scrollTriggerLvl - 1000)) {
            // a.addClass("sticky-bottom");
            a.hide();
        } else {
            a.show();
            // a.removeClass("sticky-bottom");
        }
    });

});
