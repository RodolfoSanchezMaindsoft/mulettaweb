/*toggle clases viewport*/

    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".photo"
    });
    
    // $(document).on('load', function () {
    //     lazyLoadInstance.update();
    // });

    
    $('#primer-modal').slick({
        lazyLoad: 'ondemand',
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        fade: true,
        cssEase: 'linear'
    });


    /* owl carousel */
    $('.owl-carousel.envios').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: [
            "",
            ""
        ],
        autoplay: true,
        autoplayHoverPause: true,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
        }
    })


    $('.owl-carousel.product-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: [
            " <i class='fa fa-chevron-left'></i>",
            "<i class='fa fa-chevron-right'></i>"
        ],
        autoplay: true,
        autoplayHoverPause: true,
        lazyLoad: true
    });

    $('#promociones').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow:"<button type='button' class='slick-prev2 pull-left'><i class='fa fa-chevron-left' aria-hidden='true'></i></button>",
        nextArrow:"<button type='button' class='slick-next2 pull-right'><i class='fa fa-chevron-right' aria-hidden='true'></i></button>",
        responsive: [
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 3,
                centerPadding: '40px'
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                centerPadding: '40px'
              }
            }
        ]
    });

    $('#product-index-carousel1').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow:"<button type='button' class='slick-prev2 pull-left'><i class='fa fa-chevron-left' aria-hidden='true'></i></button>",
        nextArrow:"<button type='button' class='slick-next2 pull-right'><i class='fa fa-chevron-right' aria-hidden='true'></i></button>",
        responsive: [
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 3,
                centerPadding: '40px'
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                centerPadding: '40px'
              }
            }
        ]
    });

    $('#product-index-carousel2').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow:"<button type='button' class='slick-prev2 pull-left'><i class='fa fa-chevron-left' aria-hidden='true'></i></button>",
        nextArrow:"<button type='button' class='slick-next2 pull-right'><i class='fa fa-chevron-right' aria-hidden='true'></i></button>",
        responsive: [
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 3,
                centerPadding: '40px'
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                centerPadding: '40px'
              }
            }
        ]
    });

     $(window).scroll(function() {
        let anchuraVentana = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        const a = $('#sidebar-inner');
        let scrollTriggerLvl = 1500;

        if(anchuraVentana > 700 && anchuraVentana < 849){
            scrollTriggerLvl = 1500;
        }else if(anchuraVentana > 850 && anchuraVentana < 1023){
            scrollTriggerLvl = 2000;
        }else if(anchuraVentana > 1024 && anchuraVentana < 1279){
            scrollTriggerLvl = 2200;
        }else if(anchuraVentana > 1280 && anchuraVentana < 1365){
            scrollTriggerLvl = 2500;
        }else if(anchuraVentana > 1366 && anchuraVentana < 1499){
                scrollTriggerLvl = 2800;
        }else if(anchuraVentana > 1500 && anchuraVentana < 1899){
                scrollTriggerLvl = 3000;
        }else if(anchuraVentana > 1900 && anchuraVentana < 2499){
                scrollTriggerLvl = 4000;
        }else if(anchuraVentana > 2500){
                scrollTriggerLvl = 5000;
        }

        if ($(this).scrollTop() > scrollTriggerLvl) {
            a.addClass("sticky-bottom");
        } else {
            a.removeClass("sticky-bottom");
        }
    });
        
    var accordionToggles = document.querySelectorAll(".accordion-ctm a");
    accordionToggles.forEach(function (toggle) {
        return toggle.addEventListener("click", function () {
            toggle.classList.toggle("active");
            toggle.nextElementSibling.classList.toggle("active");
        });
    });
    /*ajax request send back "unauthorized" status response from when spends too much time on page and login expires
    */
    $.ajaxSetup({ statusCode: { 401: function() { window.location.href = '/usuario/login'; } } });