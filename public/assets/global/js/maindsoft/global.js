// $(document).on('ready', function () {
//     // initialization of header
//     $.HSCore.components.HSHeader.init($('#js-header'));
//     $.HSCore.helpers.HSHamburgers.init('.hamburger');

//     // initialization of HSMegaMenu component
//     $('.js-mega-menu').HSMegaMenu({
//         event: 'hover',
//         pageContainer: $('.container'),
//         breakpoint: 991
//     });
// });

var accordionToggles = document.querySelectorAll(".accordion-ctm a");
accordionToggles.forEach(function (toggle) {
  return toggle.addEventListener("click", function () {
    toggle.classList.toggle("active");
    toggle.nextElementSibling.classList.toggle("active");
  });
});