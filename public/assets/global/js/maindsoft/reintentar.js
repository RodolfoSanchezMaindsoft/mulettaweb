$(document).on('ready', function () {
    $('.loader').hide();

    $('#fwebpay_url').hide();
    $('#btn_finalizar').show();
});

$("input[name='radInline1_1']").on( 'change', function() {
    changeinput($(this).val());
});

function changeinput(value){
    if(value == 2){
        $('#btn_finalizar').show();
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", true );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#webpay" ).prop( "checked", false );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        payment = "MercadoPago";
    }else if(value == 4){
        $('#btn_finalizar').hide();
        $('#fwebpay_url').show();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#webpay" ).prop( "checked", true );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        payment = "Webpay";
    }else if(value == 5){
        $('#btn_finalizar').show();
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#inputOpenPayTransf" ).prop( "checked", true );
        $( "#webpay" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        payment = "OpenPay";
    }else if(value == 6){
        $('#btn_finalizar').show();
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", false );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", true );
        $( "#webpay" ).prop( "checked", false );
        payment = "OpenPay";
    }else{
        $('#btn_finalizar').show();
        $('#fwebpay_url').hide();
        $( "#inputPaypal" ).prop( "checked", false );
        $( "#inputMercadopago" ).prop( "checked", false );
        $( "#inputOpenPay" ).prop( "checked", true );
        $( "#inputOpenPayTransf" ).prop( "checked", false );
        $( "#inputOpenPayPayNet" ).prop( "checked", false );
        $( "#webpay" ).prop( "checked", false );
        payment = "OpenPay";
    }

    // gtag('event', 'set_checkout_option', {
    //     "checkout_step": 3,
    //     "checkout_option": value,
    //     "value": payment,
    // });
}

function closeModal(){
    $('#error-modal').modal('hide');
    window.location.href = $('#url_pedidos').text();
}


function MercadoPago(){
    $('.loader').show();
    window.location.href = $('#url_mercado_pago').val();
}