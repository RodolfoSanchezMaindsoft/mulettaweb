
const getRemainTime = () => {
    var fecha = $('#fecha_vencimiento').val();
    let now = new Date(),
        remainTime = (new Date(fecha) - now + 1000) / 1000,
        remainSeconds = ('0' + Math.floor(remainTime % 60) ).slice(-2),
        remainMinutes = ('0' + Math.floor(remainTime / 60 % 60) ).slice(-2),
        remainHours = ('0' + Math.floor(remainTime / 3600 % 24) ).slice(-2),
        remainDays = ('0' + Math.floor(remainTime / (3600 * 24) ) ).slice(-2);

    return {
        remainTime,
        remainSeconds,
        remainMinutes,
        remainHours,
        remainDays
    }

}

const constTimerUpdate = setInterval(() => {
    let t = getRemainTime();

    $('.num').removeClass('div_numeros');

    $('.days').html('<span class="div_numeros izquierda">' + t.remainDays.split('')[0] + '</span>' + '<span class="div_numeros">' + t.remainDays.split('')[1] + '</span>');
    $('.hours').html('<span class="div_numeros izquierda">' + t.remainHours.split('')[0] + '</span>' + '<span class="div_numeros">' + t.remainHours.split('')[1] + '</span>');
    $('.minutes').html('<span class="div_numeros izquierda">' + t.remainMinutes.split('')[0] + '</span>' + '<span class="div_numeros">' + t.remainMinutes.split('')[1] + '</span>');
    $('.seconds').html('<span class="div_numeros izquierda">' + t.remainSeconds.split('')[0] + '</span>' + '<span class="div_numeros">' + t.remainSeconds.split('')[1] + '</span>');

    if (t.remainTime <= 1) {
        clearInterval(constTimerUpdate);
        $('#countDown_time').html(`<div class="col-md-12 countdown_information text-center">
                                    <span class="countdown_slogan">FINALIZO LA OFERTA</span>
                                   </div>`);
    }

}, 1000);