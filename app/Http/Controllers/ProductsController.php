<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Librería donde se encuentran nuestros métodos de productos
use App\Repositories\Products;

require_once '../vendor/autoload.php';
// require_once '/home/muletta/pruebaMuletta/vendor/autoload.php';
// require_once '/home/muletta/muletta/vendor/autoload.php';

use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\ServerSide\ActionSource;
use FacebookAds\Object\ServerSide\Content;
use FacebookAds\Object\ServerSide\CustomData;
use FacebookAds\Object\ServerSide\DeliveryCategory;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;

class ProductsController extends Controller{
    //Inyectamos la clase Products por medeio del contructor e importamos la clase
    protected $products;

    protected $access_token = 'EAAQLYcQafWoBAF0xf4G0LqmCb1A7frtYUrREnmpiJaqFpzZBRaKM6xy0FHeSj3FNOGzXqfoBAud0VsLaTYWAOXO0mNDZAdGZCl4HlsT3RIMRTuplY5RQYEfioik0axfEKFQGxOSVdpFauCXezZCaoauAwEk5VwSqZBz3AZCqcA0CAHfobC0ipsVVSSPqcGyM4ZD';
    protected $pixel_id = '357019004873341';

    //Creamos el constructor de la clase el cual nos creara una instancia de la clase Products
    public function __construct(Products $products){
        $this->products = $products;

        $api = Api::init(null, null, $this->access_token, false);
        $api->setLogger(new CurlLogger());
    }

    //Metodo para obtener todos los productos por categoría
    public function index(){
        $id_tipo_producto = 0;
        //En la variable productos guardamos el resultado que nos retorna el método getProductsCategories de la clase products
        $productos = $this->products->getProductsCorouselDestacados($id_tipo_producto);
        $imagenes = $this->products->getImagenes(0, 5);
        $img_categorias = $this->products->getImagenes(0, 6);
        $new_arrivals = $this->products->getImagenes(0, 7);
        $promociones = $this->products->getImagenes(0, 8);
        $demand = $this->products->getImagenes(0, 9);
        $showroom = $this->products->getImagenes(0, 10);
        $emprendedor = $this->products->getImagenes(0, 11);
        $comunidad = $this->products->getImagenes(0, 12);
        
        //Retornamos los resultados de los productos de manera de arreglo por medio del método compact
        return view('index', compact('productos', 'imagenes', 'img_categorias', 'new_arrivals', 'promociones', 'demand', 'showroom', 'emprendedor', 'comunidad'));
    }

    // Metodo para obtener el primer nivel del menu del header
    public function getMenuPrimerNivel(){

        $menuPrimerNivel = $this->products->getMenuPrimerNivel();

        $perfil_usuario = session('perfilUsuario') ? session('perfilUsuario')['usuario'] : null;
        
        $user_data = (new UserData())
            ->setFbc('fb.1.'.time().'.AbCdEfGhIjKlMnOpQrStUvWxYz1234567890')
            ->setFbp('fb.1.'.time().'.1098115397');

        if ($perfil_usuario) {
            $user_data->setEmail($perfil_usuario->EMAIL)
            ->setFirstName($perfil_usuario->NOMBRE)
            ->setLastName($perfil_usuario->APELLIDO_PATERNO)
            ->setCountryCode('mx');

            if (isset($perfil_usuario->FECHA_NACIMIENTO) && $perfil_usuario->FECHA_NACIMIENTO != '') {
                $user_data->setDateOfBirth(str_replace('-','',substr($perfil_usuario->FECHA_NACIMIENTO, 0, 10)));
            }
            
            if (isset($perfil_usuario->TELEFONO) && $perfil_usuario->TELEFONO != ''){
                $user_data->setPhone($perfil_usuario->TELEFONO);
            }

            if (isset($perfil_usuario->ESTADO) && $perfil_usuario->ESTADO != ''){
                $user_data->setState($perfil_usuario->ESTADO);
            }

            if (isset($perfil_usuario->MUNICIPIO) && $perfil_usuario->MUNICIPIO != ''){
                $user_data->setCity($perfil_usuario->MUNICIPIO);
            }

            if (isset($perfil_usuario->CODIGO) && $perfil_usuario->CODIGO != ''){
                $user_data->setZipCode($perfil_usuario->CODIGO);
            }
        }

        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

        $event = (new Event())
            ->setEventName('PageView')
            ->setActionSource("email")
            ->setEventSourceUrl($url)
            ->setEventTime(time())
            ->setUserData($user_data);

        $events = array();
        array_push($events, $event);

        $request = (new EventRequest($this->pixel_id))
            ->setEvents($events);
        //$response = $request->execute();

        session(['menuPrimerNivel' => compact('menuPrimerNivel')]);

    }

    // Metodo para obtener el segundo y tercer nivel del menu del header
    public function getGenero(){

        $genero = $this->products->getGenero();

        session(['genero' => compact('genero')]);
    }

    // Metodo para obtener el segundo y tercer nivel del menu del header
    public function getTipo(){

        $tipoProducto = $this->products->getProducto();

        session(['tipoProducto' => compact('tipoProducto')]);
    }
    
    // Metodo para obtener el segundo y tercer nivel del menu del header
    public function getClasificacion(){

        $clasificacionProductos = $this->products->getTipo();

        session(['clasificacionProductos' => compact('clasificacionProductos')]);
    }

        
    // Metodo para obtener el segundo y tercer nivel del menu del header
    public function getTallas(){

        $tallas = $this->products->getTallas();

        session(['tallas' => compact('tallas')]);
    }

    // Metodo para obtener la barra de descuentos
    public function getCountDown(){

        $countDown = $this->products->getCountDown();
        
        session(['countDown' => compact('countDown')]);

    }

    // Metodo para obtener el segundo y tercer nivel del menu del header
    public function getMenuSubNivel(){

        $menuSubNivel = $this->products->getMenuSubNivel();

        session(['menuSubNivel' => compact('menuSubNivel')]);
    }

    //Metodo para obtener todos los productos por categoría
    public function getProductsCategories($id_tipo_producto, $url_tipo_producto){

        $primera_vez = session()->get('primera_vez');
        if (!isset($primera_vez)) {
            session(['primera_vez' => 'grid']);
        }else{
            session(['primera_vez' => 'none']);
        }

        $padres = session()->get('menuPrimerNivel')['menuPrimerNivel'];
        $subNivel = session()->get('menuSubNivel')['menuSubNivel'];

        $sub = 0;
        $subSeg = 0;

        foreach ($subNivel as $value) {
            if ($value->ID_CATEGORIA_FLEXIBLE == $id_tipo_producto) {
                foreach ($subNivel as $val) {
                    if ($val->ID_CATEGORIA_FLEXIBLE == $value->CATEGORIA_PADRE) {
                        $sub = $val->CATEGORIA_PADRE;
                        $subSeg = $val->ID_CATEGORIA_FLEXIBLE;
                    }
                }
            }
        }

        if($sub == 0){
            foreach ($subNivel as $value) {
                if ($value->ID_CATEGORIA_FLEXIBLE == $id_tipo_producto) {
                    foreach ($padres as $padre) {
                        if ($padre->ID_CATEGORIA_FLEXIBLE === $value->CATEGORIA_PADRE) {
                            $sub = $padre->ID_CATEGORIA_FLEXIBLE;
                            $subSeg = $val->ID_CATEGORIA_FLEXIBLE;
                        }
                    }
                }
            }
        }

        if ($sub == 0) {
            $sub = $id_tipo_producto;
        }

        //En la variable productos guardamos el resultado que nos retorna el método getProductsCategories de la clase products
        $productos = $this->products->getProductsCategories($id_tipo_producto, 1);
        $tallas = [];
        
        //Retornamos los resultados de los productos de manera de arreglo por medio del método compact
        return view('categoria', compact('productos', 'sub', 'id_tipo_producto', 'subSeg', 'url_tipo_producto', 'tallas'));
    }

    //Metodo para obtener todos los productos por categoría
    public function indexPadre($nombre){
        $id_tipo_producto = null;
        $padres = session()->get('menuPrimerNivel');
        foreach ($padres as $key => $primero) {
            foreach ($primero as $key => $value) {
                if ($value->URL_CATEGORIA == $nombre) {
                    $id_tipo_producto = $value->ID_CATEGORIA_FLEXIBLE;
                }
            }
        }

        if ($id_tipo_producto) {
            //En la variable productos guardamos el resultado que nos retorna el método getProductsCategories de la clase products
            $productos = $this->products->getProductsCorouselDestacados($id_tipo_producto);
            $imagenes = $this->products->getImagenes($id_tipo_producto, $nombre);

            //Retornamos los resultados de los productos de manera de arreglo por medio del método compact
            return view('index-padre', compact('productos', 'imagenes', 'id_tipo_producto', 'nombre'));
        }else{
            return redirect('/');
        }
    }

    public function infiniteProducts($id_categoria, $page){
        // $page++;
        $productos = $this->products->getProductsCategories($id_categoria, $page);

        return json_encode($productos);
    }
    
    //Método para obtener todos los campos de un producto individual
    public function getProduct($descripcion = null, $id){
        //En la variable productos guardamos el resultado que nos retorna el método getProduct de la clase products
        $productos = $this->products->getProduct($id);

        $padres = session()->get('menuPrimerNivel')['menuPrimerNivel'];
        $hijos = session()->get('menuSubNivel')['menuSubNivel'];
        $cadena_de_texto = '';
        $cat_flex;
        $aux = false;

        foreach ($padres as $primero) {
            if ($primero->ID_CATEGORIA_FLEXIBLE == $productos[0]->ID_CATEGORIA_FLEXIBLE) {
                $cadena_de_texto = $primero->URL_CATEGORIA;
                $cadena_buscada   = 'hombre';
                $posicion_coincidencia = strrpos($cadena_de_texto, $cadena_buscada);
                
                $cat_flex = $productos[COUNT($productos) - 1]->ID_CATEGORIA_FLEXIBLE;
                $aux = true;
            }else{
                $cadena_de_texto = '2';
            }

        }
        
        foreach ($hijos as $primero) {
            if (!$aux && $primero->ID_CATEGORIA_FLEXIBLE == $productos[0]->ID_CATEGORIA_FLEXIBLE) {
                $cadena_de_texto = $primero->URL_CATEGORIA;
                $cat_flex = $productos[0]->ID_CATEGORIA_FLEXIBLE;
                $cadena_buscada   = 'hombre';
                $posicion_coincidencia = strrpos($cadena_de_texto, $cadena_buscada);
            }
        }

        if (!isset($cat_flex)) {
            if (count($productos) > 1) {
                $cat_flex = $productos[1]->ID_CATEGORIA_FLEXIBLE;
            }else{
                $cat_flex = $productos[0]->ID_CATEGORIA_FLEXIBLE;
            }
        }

        $categoria_bradcrumbs = [
            'id_tipo_producto' => $cat_flex,
            'url_tipo_producto' => $cadena_de_texto
        ];

        //En la variable $variantes_productos guardamos el resultado que nos retorna las variantes de los productos
        $variantes_productos = $this->products->getProductSize($id);
        $productos_interesar = $this->products->getProductsCorouselDestacados($productos[0]->ID_CATEGORIA_FLEXIBLE);
        $productos_accesorios = $this->products->getProductsCorousel($id);

        $perfil_usuario = session('perfilUsuario') ? session('perfilUsuario')['usuario'] : null;
        
        $user_data = (new UserData())
            ->setFbc('fb.1.'.time().'.AbCdEfGhIjKlMnOpQrStUvWxYz1234567890')
            ->setFbp('fb.1.'.time().'.1098115397');
        if ($perfil_usuario) {
            $user_data->setEmail($perfil_usuario->EMAIL)
            ->setFirstName($perfil_usuario->NOMBRE)
            ->setLastName($perfil_usuario->APELLIDO_PATERNO)
            //->setCity($perfil_usuario->APELLIDO_PATERNO)
            ->setCountryCode('mx')
            ->setDateOfBirth(str_replace('-','',substr($perfil_usuario->FECHA_NACIMIENTO, 0, 10)))
            ->setPhone($perfil_usuario->TELEFONO)
            ->setState($perfil_usuario->ESTADO)
            ->setZipCode($perfil_usuario->CODIGO);
        }

        $content = (new Content())
            ->setProductId($id)
            ->setDescription($descripcion);

        $custom_data = (new CustomData())
            ->setContentType('product')
            ->setContentIds(array($id))
            ->setContentName($descripcion)
            ->setContentCategory($productos[0]->DESCRIPCION_TIPO_PRODUCTO);

        $url = route('producto_individual', ['DESCRIPCION_MODELO' => str_replace(' ','-',str_replace('/','-',$descripcion)), 'ID_MODELO' => $id]);

        $event = (new Event())
            ->setEventName('ViewContent')
            ->setEventTime(time())
            ->setEventSourceUrl($url)
            ->setUserData($user_data)
            ->setCustomData($custom_data);

        $events = array();
        array_push($events, $event);

        $request = (new EventRequest($this->pixel_id))
            ->setEvents($events);
            //->setTestEventCode('TEST61134');
        //$response = $request->execute();

        //Retornamos los resultados de los productos de manera de arreglo por medio del método compact
        return view('producto.producto', compact('productos', 'variantes_productos', 'productos_accesorios', 'productos_interesar', 'id', 'descripcion', 'categoria_bradcrumbs'));
    }

    public function getProductsWithVariants(Request $request){
        $precio = $request->s;
        $vista = view('componentes.precio' , compact('precio'))->render();
        return response()->json(['html'=>$vista]);
    }

    public function getProductsWithVariantsName(Request $request){
        $descripcion_talla = $request->d;
        $vista = view('componentes.nombre' , compact('descripcion_talla'))->render();
        return response()->json(['html'=>$vista]);
    }

    public function addcar(Request $request){
        $carrito = session('carrito');
        $name = $request->name;
        $talla = $request->talla;
        $price = floatval($request->price);
        $price_origin = floatval($request->price_origin);
        $color = $request->color;
        $hexadecimal = $request->hexadecimal;
        $cantidad = IntVal($request->cantidad);
        $producto = $request->producto;
        $imagen = $request->imagen;
        $id = $request->id;
        $category = $request->category;

        if (!$carrito) {
            $carrito = (object)[
                'productos' => [
                    [
                        'id' => $id,
                        'image' => $imagen,
                        'name' => $name,
                        'color' => $color,
                        'hexadecimal' => $hexadecimal,
                        'talla' => $talla,
                        'type' => $producto,
                        'size' => '',
                        'qty' => $cantidad,
                        'price' => $price,
                        'price_origin' => $price_origin,
                        'category' => $category,
                        'total' => ($price * $cantidad)
                    ]
                ],
                'subtotal' => ($price * $cantidad),
                'descuento' => '',
                'total' => ($price * $cantidad),
            ];

        } else {
            $existe = false;
            for ($i = 0; $i < count($carrito->productos); $i++) {
                if ($carrito->productos[$i]['id'] == $id) {
                    $carrito->productos[$i]['qty'] = $carrito->productos[$i]['qty'] + $cantidad;
                    $carrito->productos[$i]['total'] = $carrito->productos[$i]['price'] * $carrito->productos[$i]['qty'];
                    $existe = true;
                }
            }
            if ($existe == false) {
                $carrito->productos[] = [
                    'id' => $id,
                    'talla' => $talla,
                    'image' => $imagen,
                    'name' => $name,
                    'color' => $color,
                    'hexadecimal' => $hexadecimal,
                    'type' => $producto,
                    'size' => '',
                    'qty' => $cantidad,
                    'price' => $price,
                    'price_origin' => $price_origin,
                    'category' => $category,
                    'total' => ($price * $cantidad)
                ];
            }

            $total = 0;
            foreach ($carrito->productos As $producto) {
                $total = $total + $producto['total'];
            }

            $carrito->subtotal = $total;
            $carrito->total = $total;

        }

        $perfil_usuario = session('perfilUsuario') ? session('perfilUsuario')['usuario'] : null;
        
        $user_data = (new UserData())
            ->setFbc('fb.1.'.time().'.AbCdEfGhIjKlMnOpQrStUvWxYz1234567890')
            ->setFbp('fb.1.'.time().'.1098115397');
        if ($perfil_usuario) {
            $user_data->setEmail($perfil_usuario->EMAIL)
            ->setFirstName($perfil_usuario->NOMBRE)
            ->setLastName($perfil_usuario->APELLIDO_PATERNO)
            //->setCity($perfil_usuario->APELLIDO_PATERNO)
            ->setCountryCode('mx')
            ->setDateOfBirth(str_replace('-','',substr($perfil_usuario->FECHA_NACIMIENTO, 0, 10)))
            ->setPhone($perfil_usuario->TELEFONO)
            ->setState($perfil_usuario->ESTADO)
            ->setZipCode($perfil_usuario->CODIGO);
        }

        $content = (new Content())
            ->setProductId($id)
            ->setTitle($name)
            ->setQuantity($cantidad);

        $custom_data = (new CustomData())
            ->setContentIds(array($id))
            ->setContents(array($content))
            ->setCurrency('MXN')
            ->setContentType('product')
            ->setValue(floatval($price));

        $url = route('producto_individual', ['DESCRIPCION_MODELO' => str_replace(' ','-',str_replace('/','-',$name)), 'ID_MODELO' => $id]);

        $event = (new Event())
            ->setEventName('AddToCart')
            ->setActionSource("email")
            ->setEventSourceUrl($url)
            ->setEventTime(time())
            ->setUserData($user_data)
            ->setCustomData($custom_data);

        $events = array();
        array_push($events, $event);

        $request = (new EventRequest($this->pixel_id))
            ->setEvents($events);
            //->setTestEventCode('TEST61134');
        //$response = $request->execute();
        $response = NULL;

        session(['carrito' => $carrito]);

        return json_encode([ 'carrito' => $carrito, 'response' => $response ] );
    }

    public function searchProducts(){
        $sub = 0;
        $tallas = [];

        if (isset($_POST['NOMBRE'])) {
            $name = $_POST['NOMBRE'];
            $filtro = " AND MP.DESCRIPCION_MODELO LIKE ''%".$name."%''";

            $productos = $this->products->searchModelo($filtro);
    
            return view('producto.search-products', compact('productos', 'name', 'tallas', 'sub'));
        } else {
            $name = '';
            $productos = $this->products->searchModelo($name);
    
            return view('producto.search-products', compact('productos', 'name', 'tallas', 'sub'));
        }
    }

    public function Banners($url_tipo_producto){
        $tallas = [];

        $productos = $this->products->getProductsBanners($url_tipo_producto, 1);

        return view('producto.banners-categorias', compact('productos', 'name', 'tallas', 'url_tipo_producto'));
    }

    public function addNotify(Request $request){
        $no_parte = $request->NO_PARTE;
        $correo = $request->CORREO;
        $nombre = $request->NOMBRE;

        $finalizado = $this->products->insertarProductoNegado($nombre, $no_parte, $correo);

        return json_encode(['fin' => $finalizado]);
    }

    public function getTallasMenu(Request $request){

        $todasTallas = session()->get('tallas')['tallas'];

        $tallas = [];

        $id_padre = explode(', ', $request->marca);
        $id_hijo = explode(', ', $request->genero);
        $id_nieto = explode(', ', $request->productos);
        $tipo = $request->tipo ? explode(', ', $request->tipo) : [];
        $tallas_seleccionadas = $request->tallas ? explode(', ', $request->tallas) : [];
        $precio = $request->precio;

        foreach($id_nieto as $productos){
            foreach($todasTallas as $talla){
                if ($talla->ID_MODELO_TIPO == $productos ) {
                    array_push($tallas, $talla);
                }
            }
        }

        $returnHTML = view('componentes.filtros_productos')
                        ->with('tallas', $tallas)
                        ->with('genero_seleccionado', $id_hijo)
                        ->with('producto_seleccionado', $id_nieto)
                        ->with('tipo_seleccionado', $tipo)
                        ->with('tallas_seleccionadas', $tallas_seleccionadas)
                        ->with('precio', $precio)
                        ->render();
        
        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }

    public function buscadorProductos(Request $request){
        $filtro = '';

        if ($request->nombre) {
            $filtro = $filtro." AND MP.DESCRIPCION_MODELO LIKE ''%".$request->nombre."%''";
        }

        if ($request->banner) {
            $filtro = $filtro." AND CB.URL = ''".$request->banner."''";
        }

        if ($request->genero) {
            $filtro = $filtro.' AND MP.ID_GENERO IN ('.$request->genero.')';
        }

        if ($request->productos) {
            $filtro = $filtro.' AND MP.ID_TIPO IN ('.$request->productos.')';
        }

        if ($request->precio_min > 0) {
            $filtro = $filtro.' AND (MP.PRECIO_VENTA >= '.$request->precio_min.' OR MP.PRECIO_VENTA_DESCUENTO >= '.$request->precio_min.')';
        }

        if ($request->precio_max <= 2500) {
            $filtro = $filtro.' AND (MP.PRECIO_VENTA <= '.$request->precio_max.' OR MP.PRECIO_VENTA_DESCUENTO <= '.$request->precio_max.')';
        }

        if ($request->tallas) {
            $filtro = $filtro.' AND I.NO_PARTE IN ( SELECT INV_.NO_PARTE  
                                                    FROM INVENTARIO INV_ 
                                                    JOIN INVENTARIO_PUNTO_VENTA INVV_ ON INV_.NO_PARTE = INVV_.NO_PARTE 
                                                    JOIN MODELOS_PRODUCTOS MP_ ON MP_.ID_MODELO = INV_.ID_MODELO  
                                                    JOIN TALLAS T_ ON T_.ID_TALLA =  INV_.ID_TALLA   
                                                    JOIN PUNTOS_DE_VENTA PV_ ON INVV_.ID_PUNTO_VENTA = PV_.ID_PUNTO_VENTA AND PV_.ACTIVO_INACTIVO = 1 AND PV_.EN_APP = 1
                                                    WHERE INVV_.EXISTENCIA - CEILING((INVV_.EXISTENCIA * ISNULL(MP_.STOCK_SEGURIDAD, 1)) / 100) > 0
                                                    AND T_.ID_TALLA IN ('.$request->tallas.')
                                                )';
        }

        if ($request->tipo) {
            $filtro = $filtro.' AND MP.ID_CLASIFICACION_DISENIO IN ('.$request->tipo.')';
        }

        $productos = $this->products->searchModelo($filtro);

        if ($productos != 'Sin resultados') {
            $returnHTML = view('componentes.productos')
                ->with('productos', $productos)
                ->with('name', $request->nombre)
                ->with('sub', 0)
                ->render();
        }else{
            $returnHTML = '<div class="row g-pt-30 g-mb-50">
                <div class="no-results text-center" style="min-height:50vw;">
                    <h1>
                        No hay productos que coincidan con tu búsqueda.
                    </h1>
                </div>
            </div>';
        }

        return response()->json(array('success' => true, 'html'=>$returnHTML));
    }
}