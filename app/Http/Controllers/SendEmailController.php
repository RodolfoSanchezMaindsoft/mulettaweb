<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class SendEmailController extends Controller
{
    //This method will load resources/views/ayuda/contacto.blade.php
     function getFormContacto()
    {
     return view('contacto');
    }
    
    //This method will load resources/views/ayuda/facturacion.blade.php
     function getFormFacturacion()
    {
     return view('facturacion');
    }
    //This method will load resources/views/ayuda/bolsa_trabajo.blade.php
     function getFormBolsaTrabajo()
    {
     return view('bolsa_trabajo');
    }
    
    function send(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'message' =>  'required'
     ]);

        $data = array(
            'name'      =>  $request->name,
            'email'   =>   $request->email,
            'message'   =>   $request->message
        );

     Mail::to('contacto@muletta.com')->send(new SendMail($data));
     return back()->with('exito', 'Tu correo para contacto fue enviado');

    }
    //enviar correo de facturación
    function sendFacturacion(Request $request)
    {
        $this->validate($request, [
            'name'     =>  'required',
            'email'  =>  'required|email',
            'rfc'  =>  'required',
            'empresa'  =>  'required',
            'tipo'  =>  'required',
            'colonia'  =>  'required',
            'calle'  =>  'required',
            'numero'  =>  'required',
            'estado'  =>  'required',
            'ciudad'  =>  'required',
            'cp'  =>  'required',
            'nodepedido'  =>  'required',
            'totaldecompra'  =>  'required'
        ]);

        $dataFact = array(
            
            'name'      =>  $request->name,
            'email'     =>  $request->email,
            'rfc'       =>  $request->rfc,
            'empresa'   =>  $request->empresa,
            'tipo'      =>  $request->tipo,
            'colonia'   =>  $request->colonia,
            'calle'     =>  $request->calle,
            'numero'    =>  $request->numero,
            'estado'    =>  $request->estado,
            'ciudad'    =>  $request->ciudad,
            'cp'        =>  $request->cp,
            'nodepedido'  =>  $request->nodepedido,
            'totaldecompra' =>  $request->totaldecompra
        );

        
     Mail::to('facturacion@muletta.com')->send(new SendMail($dataFact));
     return back()->with('exito', 'Tu correo para facturación fue enviado');

    }
        //enviar correo de BolsaTrabajo
    function sendBolsaTrabajo(Request $request)
    {
     $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'telefono'  =>  'required',
      'direccion'  =>  'required',
      'cv'  =>  'required',
      'message'  =>  'required'
      
     ]);

        $dataFact = array(
            
            'name'        =>  $request->name,
            'email'       =>  $request->email,
            'telefono'    =>  $request->telefono,
            'direccion'   =>  $request->direccion,
            'cv'          =>  $request->cv,
            'message'     =>  $request->message
        );

    
        
     Mail::to('contacto@muletta.com')->send(new SendMail($dataFact));
     return back()->with('exito', 'Tu correo para Bolsa de Trabajo fue enviado');

    }
}
