<?php

namespace App\Http\Controllers\Auth;

use App\Mail\Recuperar;
use App\Repositories\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $user;

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Users $user)
    {
        $this->user = $user;
        $this->middleware('guest');
    }
     public function showForgotPswdForm(Request $request)
    {
        if ($request->Email) {
            $email = $request->Email;
            $client = $this->user->resetPassword($request->Email);
            
            if(empty($client)){
                return redirect()->route('restablecer')->with('warning', 'No existe tal usuario');
            }else{
                $datos = [
                    'nombre' => $client->NOMBRE,
                    'password' => $client->CONTRASENA,
                    'email' => $request->Email
                ];
                if($datos['password']==null){
                    return view('usuario.nohaycontrasena');
                }
                else if ($client) {
                    Mail::send(new Recuperar($datos));
                    return redirect('/')->with('success', 'Revisa tu correo para recuperar tu contraseña');
                }
            }

        }
        return view('usuario.restablecer'); 

    }
}
