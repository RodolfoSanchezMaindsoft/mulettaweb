<?php

namespace App\Http\Controllers;

use App\Repositories\Users;
use App\Repositories\Orders;
use App\Repositories\Products;
use App\Repositories\Shopping;
use App\Repositories\AESCrypto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use MercadoPago\Item;
use MercadoPago\MerchantOrder;
use MercadoPago\Payer;
use MercadoPago\Payment;
use MercadoPago\Preference;
use MercadoPago\SDK;

use App\Mail\OrderPlaced;
use App\Mail\OrderMuletta;
use App\Mail\DevolucionMuletta;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

use RealRashid\SweetAlert\Facades\Alert;

use Openpay;
use Exception;
use OpenpayApiError;
use OpenpayApiAuthError;
use OpenpayApiRequestError;
use OpenpayApiConnectionError;
use OpenpayApiTransactionError;
use Illuminate\Http\JsonResponse;

use GuzzleHttp\Client;

use Barryvdh\DomPDF\Facade as PDF;

use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\ServerSide\ActionSource;
use FacebookAds\Object\ServerSide\Content;
use FacebookAds\Object\ServerSide\CustomData;
use FacebookAds\Object\ServerSide\DeliveryCategory;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;

require_once '../vendor/autoload.php';
// require_once '/home/muletta/pruebaMuletta/vendor/autoload.php';
// require_once '/home/muletta/muletta/vendor/autoload.php';


class ComprasController extends Controller
{

    protected $products;
    protected $shopping;
    protected $orders;
    protected $user;

    protected $access_token = 'EAAQLYcQafWoBAF0xf4G0LqmCb1A7frtYUrREnmpiJaqFpzZBRaKM6xy0FHeSj3FNOGzXqfoBAud0VsLaTYWAOXO0mNDZAdGZCl4HlsT3RIMRTuplY5RQYEfioik0axfEKFQGxOSVdpFauCXezZCaoauAwEk5VwSqZBz3AZCqcA0CAHfobC0ipsVVSSPqcGyM4ZD';
    protected $pixel_id = '357019004873341';

    public function __construct(Products $products, Shopping $shopping, Orders $orders, Users $user){
        $this->middleware('guest');
        $this->products = $products;
        $this->shopping = $shopping;
        $this->orders = $orders;
        $this->user = $user;
        
        // LOCAL
        // SDK::setAccessToken('TEST-757646614191839-040818-4e7733aad3ded63a95cb5c959c1013f6-487206732');
        
        // Openpay::setId(env('OPENPAY_SANDBOX_ID'));
        // Openpay::setApiKey(env('OPENPAY_SANDBOX_SK'));
        // Openpay::setProductionMode(env('OPENPAY_SANDBOX_PRODUCTION_MODE'));

        // PRODUCCIÓN
        SDK::setAccessToken('APP_USR-4883081200660401-092700-32b90df057030c42e0ec1ec671bdde3d__LA_LC__-273149135');

        Openpay::setId(env('OPENPAY_ID'));
        Openpay::setApiKey(env('OPENPAY_SK'));
        Openpay::setProductionMode(env('OPENPAY_PRODUCTION_MODE'));

        $api = Api::init(null, null, $this->access_token, false);
        $api->setLogger(new CurlLogger());
    }

    public function verifySales($id_cliente,$email,$subtotal,$importe,$envio){
        $ventas = $this->shopping->verifySales($id_cliente,$email,$subtotal,$importe,$envio);

        $carrito = session('carrito');
        $compra = [];
        
        for ($i = 0; $i < count($carrito->productos); $i++ ){
            $compra [] = ['ID_CLIENTE' => $id_cliente,
                'NO_PARTE' => $carrito->productos[$i]['id'],
                'CANTIDAD' => $carrito->productos[$i]['qty'],
                'PRECIO' => $carrito->productos[$i]['price'],
                'PRECIO_ORIGINAL' => $carrito->productos[$i]['price_origin'],
                'ORIGEN' => 1];
        }

        $compra = json_encode($compra);

        $productos = $this->shopping->insertProductSale($compra);

        $aux = [
            'ventas' => $ventas,
            'productos' => $productos
        ];

        return json_encode($aux);
    }

    public function checkout(){
        $carrito = session('carrito');

        $perfil_usuario = session('perfilUsuario') ? session('perfilUsuario')['usuario'] : [];
        $estados = [];
        $municipios = [];

        $client = (object)[
            'id' => $perfil_usuario ? $perfil_usuario->ID_CLIENTE : '',
            'name' => $perfil_usuario ? $perfil_usuario->NOMBRE : '',
            'address' => $perfil_usuario ? $perfil_usuario->CALLE : '',
            'email' => $perfil_usuario ? $perfil_usuario->EMAIL : '',
            'num_int' => $perfil_usuario ? $perfil_usuario->NUM_INT : '',
            'num_ext' => $perfil_usuario ? $perfil_usuario->NUM_EXT : '',
            'state' => $perfil_usuario ? $perfil_usuario->ESTADO : '',
            'postalCode' => $perfil_usuario ? $perfil_usuario->CODIGO : '',
            'col' => $perfil_usuario ? $perfil_usuario->COLONIA : '',
            'province' => $perfil_usuario ? $perfil_usuario->MUNICIPIO : '',
            'phoneNumber' => $perfil_usuario ? $perfil_usuario->TELEFONO : ''
        ];

        $direcciones = [];
        if($perfil_usuario){
            $direcciones = $this->user->GetClientAddresses($perfil_usuario->EMAIL);
            $estados = $this->user->GetEstates();
            

            if (count($direcciones) > 0) {
                $municipios = $this->user->getMunicipios($direcciones[0]->ESTADO);
            }
            

            $user_data = (new UserData())
                ->setFbc('fb.1.'.time().'.AbCdEfGhIjKlMnOpQrStUvWxYz1234567890')
                ->setFbp('fb.1.'.time().'.1098115397')
                ->setEmail($perfil_usuario->EMAIL)
                ->setFirstName($perfil_usuario->NOMBRE)
                ->setLastName($perfil_usuario->APELLIDO_PATERNO)
                //->setCity($perfil_usuario->APELLIDO_PATERNO)
                ->setCountryCode('mx')
                ->setDateOfBirth(str_replace('-','',substr($perfil_usuario->FECHA_NACIMIENTO, 0, 10)))
                ->setPhone($perfil_usuario->TELEFONO)
                ->setState($perfil_usuario->ESTADO)
                ->setZipCode($perfil_usuario->CODIGO);

            $content = [];
            $ids = [];

            foreach($carrito->productos as $prod){
                $ids[] = $prod['id'];
                $content[] = (new Content())
                    ->setProductId($prod['id'])
                    ->setTitle($prod['name'])
                    ->setQuantity($prod['qty'])
                    ->setDescription($prod['talla'])
                    ->setItemPrice($prod['price']);
            }

            $custom_data = (new CustomData())
                ->setContentIds($ids)
                ->setContents($content)
                ->setCurrency('MXN')
                ->setContentType('product')
                ->setValue(floatval($carrito->total));

            $url = route('compras');

            $event = (new Event())
                ->setEventName('InitiateCheckout')
                ->setEventSourceUrl($url)
                ->setEventTime(time())
                ->setUserData($user_data)
                ->setCustomData($custom_data);

            $events = array();
            array_push($events, $event);

            $request = (new EventRequest($this->pixel_id))
             ->setEvents($events);
            //     ->setTestEventCode('TEST61134');
            //$response = $request->execute();
        }

        $costos = $this->shopping->getCostSend();

        $envio = $costos[0]->VALOR_COSTO;
        $min_envio = $costos[0]->MINIMO;
        $regalo = $costos[1]->VALOR_COSTO;
        session(['envio' => $envio]);
        session(['min_envio' => $min_envio]);
        session(['regalo' => $regalo]);
     
        if($carrito && $carrito->productos){
            
            for ($i = 0; $i < count($carrito->productos); $i++){

                $dobleEspacio = explode(' ', $carrito->productos[$i]['id']); 

                if ($dobleEspacio[0] == "MT9C1-006"){
                    
                    //$dobleEspacio = str_replace($searchString, $replaceString, $carrito->productos[$i]['id']);
                    $carrito->productos[$i]['id'] = $dobleEspacio[0].'  '.substr($carrito->productos[$i]['id'], -1);
                }

            }
            
            $existencias = $this->shopping->VerifyExist(json_encode($carrito->productos));
            $productos = [];

            foreach($carrito->productos as $prod){
                foreach($existencias as $existencia){
                    if ($prod['id'] == $existencia->NO_PARTE) {
                        $prod['existencia'] = $existencia->EXISTENCIA;
                    }
                }
                $productos[] = $prod;
            }


            return view('compras.checkout', compact('client', 'carrito', 'productos', 'direcciones', 'estados', 'municipios'));
        }   
        else{
            return redirect()->route('index');
        }     

    }

    public function checkoutUser(){
        $estados = [];

        $client = [];

        $direcciones = [];
        $estados = $this->user->GetEstates();

        $carrito = session('carrito');

        $costos = $this->shopping->getCostSend();

        $envio = $costos[0]->VALOR_COSTO;
        $min_envio = $costos[0]->MINIMO;
        $regalo = $costos[1]->VALOR_COSTO;
        session(['envio' => $envio]);
        session(['min_envio' => $min_envio]);
        session(['regalo' => $regalo]);
     
        if($carrito && $carrito->productos){
            $existencias = $this->shopping->VerifyExist(json_encode($carrito->productos));
            $productos = [];

            foreach($carrito->productos as $prod){
                foreach($existencias as $existencia){
                    if ($prod['id'] == $existencia->NO_PARTE) {
                        $prod['existencia'] = $existencia->EXISTENCIA;
                    }
                }
                $productos[] = $prod;
            }

            return view('compras.checkout-user', compact('client', 'carrito', 'productos', 'direcciones', 'estados'));
        }   
        else{
            return redirect()->route('index');
        }     

    }

    public function completarPago(Request $request){
        $productos = session('carrito');
        $envio = session('envio');
        $id_cliente = $request->ID_CLIENTE;
        $nombre = $request->NOMBRE;
        $telefono = $request->TELEFONO;
        $email = $request->EMAIL;
        $calle = $request->street_user;
        $numero_interior = $request->num_int_user;
        $numero_exterior = $request->num_ext_user;
        $colonia = $request->col_user;
        $codigo_postal = $request->code_postal_user;
        $municipio = $request->mun_user;
        $estado = $request->state_user;
        $id_desc = $request->id_desc;
        $monto_desc = $request->monto_desc;
        $regalo = $request->regalo;
        $alias = $request->alias;
        $referencia = $request->referencia;
        $pago_coins = $request->coins;

        $id_pago = null;
        $id_orden = null;
            
        $pais = "México";
        $metodo_pago = $request->metodo_pago;
        $descripcion_pago = '';
        $nueva_direccion = $request->guardar;

        $pago = $this->shopping->updateFinalSale($id_cliente,$alias,$email,$calle,$numero_interior,$numero_exterior,$colonia,$codigo_postal,$municipio,$estado,$pais,$metodo_pago,$descripcion_pago,$nueva_direccion, $id_desc, $monto_desc, $regalo, $id_pago, $referencia, $pago_coins);
        $inv = $this->shopping->updateSalesInventory($id_cliente);

        //Enviar correo
        $id_venta = $pago[0]->ID_VENTA;
        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos_correo = $this->orders->GetOrderProducts($id_venta);
        $perfil_usuario = session()->get('perfilUsuario') ? session()->get('perfilUsuario')['usuario'] : null;

        if ($perfil_usuario && $pago_coins > 0) {
            $this->shopping->restarDineroUsuario($id_cliente, $ordenes->NO_TICKET, $pago_coins);
        }

        $text_metodo_pago = "";

        if($metodo_pago == 13){
            $text_metodo_pago = 'MercadoPago';
        }else if($metodo_pago == 18){
            $text_metodo_pago = 'Tarjeta de debito/credito';
        }else if($metodo_pago == 24){
            $text_metodo_pago = 'Transferencia BBVA';
        }else{
            $text_metodo_pago = 'Tarjeta de debito/credito';
        }

        $correos = ['adrian@muletta.com', 'ventas@muletta.com'];
        
        $orden = array(
            'titulo'            => 'Compra generada como pendiente de pago',
            'nombre'            => $ordenes->NOMBRE,
            'telefono'          => $ordenes->TELEFONO,
            'email'             => $ordenes->EMAIL,
            'FECHA_VENTA'       => $ordenes->FECHA_VENTA,
            'HORA_VENTA'        => $ordenes->HORA_VENTA,
            'metodo_pago'       => $text_metodo_pago,
            'calle'             => $ordenes->CALLE,
            'numero_interior'   => $ordenes->NUM_INT,
            'numero_exterior'   => $ordenes->NUM_EXT,
            'colonia'           => $ordenes->COLONIA,
            'codigo_postal'     => $ordenes->CODIGO,
            'municipio'         => $ordenes->MUNICIPIO,
            'estado'            => $ordenes->ESTADO,
            'monto_desc'        => $ordenes->DESCUENTO,
            'regalo'            => $ordenes->REGALO,
            'id_orden'          => $id_venta,
            'no_ticket'         => $ordenes->NO_TICKET,
            'correosMuletta'    => $correos,
            'productos'         => $productos_correo
        );

        /*try {
            Mail::send(new OrderMuletta($orden));
        } catch (\Throwable $th) {
            //throw $th;
        }
        try {
            Mail::send(new OrderPlaced($orden));
        } catch (\Throwable $th) {
            //throw $th;
        }*/

        session()->forget('carrito');

        return json_encode(['id_venta' => $pago[0]->ID_VENTA, 'productos' => $productos->productos, 'envio' => $envio]);
    }

    public function mercadoOrder($id_cliente, $id_venta, Request $request){

        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);

        //$correos = ['carlos.ibarra@maindsoft.net'];
        $correos = ['adrian@muletta.com', 'ventas@muletta.com'];
        
        $orden = array(
            'titulo'            => 'Compra realizada',
            'nombre'            => $ordenes->NOMBRE,
            'telefono'          => $ordenes->TELEFONO,
            'email'             => $ordenes->EMAIL,
            'FECHA_VENTA'       => $ordenes->FECHA_VENTA,
            'HORA_VENTA'        => $ordenes->HORA_VENTA,
            'metodo_pago'       => 'MercadoPago',
            'calle'             =>  $ordenes->CALLE,
            'numero_interior'   =>  $ordenes->NUM_INT,
            'numero_exterior'   =>  $ordenes->NUM_EXT,
            'colonia'           =>  $ordenes->COLONIA,
            'codigo_postal'     =>  $ordenes->CODIGO,
            'municipio'         =>  $ordenes->MUNICIPIO,
            'estado'            =>  $ordenes->ESTADO,
            'monto_desc'        =>  $ordenes->DESCUENTO,
            'regalo'            =>  $ordenes->REGALO,
            'id_orden'          =>  $id_venta,
            'no_ticket'         =>  $ordenes->NO_TICKET,
            'correosMuletta'    =>  $correos,
            'productos'         =>  $productos
        );

        /*try {
            Mail::send(new OrderMuletta($orden));
        } catch (\Throwable $th) {
            //throw $th;
        }
        try {
            Mail::send(new OrderPlaced($orden));
        } catch (\Throwable $th) {
            //throw $th;
        }*/

        $track_items = '[';
        $subtotal = 0;

        foreach($productos as $key => $product) {
            $track_items = $track_items.'{
                id: "'.$product->ID_MODELO.'",
                name: "'.$product->DESCRIPCION.'",
                variant: "'.$product->TALLA.'",
                quantity: '.$product->CANTIDAD_VENDIDA.',
                price: '.$product->PRECIO.'';

            if (count($productos) == ($key + 1)) {
                $track_items = $track_items.'}';
            }else{
                $track_items = $track_items.'},';
            }
        }

        $track_items = $track_items.']';

        $pos = strrpos($_SERVER["HTTP_HOST"], "me");
        $pos_prueba = strrpos($_SERVER["HTTP_HOST"], "prueba");

        if ($pos === false && $pos_prueba === false) {
        //     echo '<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68032621-1"></script>
        //             window.dataLayer = window.dataLayer || [];
        //             function gtag(){dataLayer.push(arguments);}
        //             gtag("js", new Date());
                    
        //             gtag("config", "UA-68032621-1");

        //             gtag(function() {
        //                 console.log(gtag.getAll());
        //             });
        
        //             gtag("event", "purchase", {
        //                 "transaction_id": '.$id_venta.',
        //                 "value": '.$ordenes->TOTAL.',
        //                 "currency": "MXN",
        //                 "checkout_option": "MercadoPago",
        //                 "shipping": 0,
        //                 "items": '.$track_items.'
        //             });
        
            echo ' <script>
                     !function(f,b,e,v,n,t,s)
                     {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                         n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                         if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
                         n.queue=[];t=b.createElement(e);t.async=!0;
                         t.src=v;s=b.getElementsByTagName(e)[0];
                         s.parentNode.insertBefore(t,s)}(window, document,"script",
                         "https://connect.facebook.net/en_US/fbevents.js");
                     fbq("init", "357019004873341");

                 </script>';
            echo '<script type="text/javascript">        
                     fbq("track", "Purchase", {
                         value: '.$ordenes->TOTAL.',
                         currency: "MXN",
                         contents: "0",
                         content_type: "product"
                    });
                 </script>';
         }

        $perfil_usuario = session('perfilUsuario')['usuario'];
        $user_data = (new UserData())
            ->setFbc('fb.1.'.time().'.AbCdEfGhIjKlMnOpQrStUvWxYz1234567890')
            ->setFbp('fb.1.'.time().'.1098115397');
        
        if ($perfil_usuario) {
            $user_data->setEmail($perfil_usuario->EMAIL)
            ->setFirstName($perfil_usuario->NOMBRE)
            ->setLastName($perfil_usuario->APELLIDO_PATERNO);
            // ->setCity($perfil_usuario->APELLIDO_PATERNO)
            // ->setCountryCode('mx')
            // ->setDateOfBirth(str_replace('-','',substr($perfil_usuario->FECHA_NACIMIENTO, 0, 10)))
            // ->setPhone($perfil_usuario->TELEFONO)
            // ->setState($perfil_usuario->ESTADO)
            // ->setZipCode($perfil_usuario->CODIGO);
        }

        $custom_data = (new CustomData())
            ->setCurrency('MXN')
            ->setValue(floatval($ordenes->TOTAL));

        $url = 'http://'.$_SERVER["HTTP_HOST"].'/compras/mercadoOrder/'.$id_cliente.'/'.$id_venta;

        $event = (new Event())
            ->setEventName('Purcharse')
            ->setActionSource("website")
            ->setEventSourceUrl($url)
            ->setEventTime(time())
            ->setUserData($user_data)
            ->setCustomData($custom_data);

        $events = array();
        array_push($events, $event);

        $request2 = (new EventRequest($this->pixel_id))
         ->setEvents($events);
        //     ->setTestEventCode('TEST61134');
        //$response = $request2->execute();

        $this->orders->UpdatePay($id_venta, $request->collection_id, 13);
        $this->shopping->updateStatusToProcessing($id_cliente);

        $ordenes = $this->orders->GetOrderInfo($id_venta);

        $perfil_usuario = session()->get('perfilUsuario')['usuario'];
        $REAL = null;
        $ID_VENTA = $id_venta;

        return view('compras.detailsProd', compact('ordenes','productos', 'perfil_usuario', 'REAL', 'ID_VENTA'));
        
    }

    public function initMercadoOrder(Request $request){
        $total = floatval(Input::get('input_total'));
        $descuento = floatval(Input::get('monto_desc'));
        $coins = floatval(Input::get('coins'));
        $preference = new Preference();
        $regalo = intval(Input::get('regalo'));
        $envio = floatval($total) < session()->get('min_envio') ? session()->get('envio') : 0;
        // $envio = floatval($total) < 999 ? session()->get('envio') : 0;

        $item = new Item();
        $item->id = 1;
        $item->title = 'Compra en tienda web';
        $item->quantity = 1;
        $item->currency_id = 'MXN';
        $item->unit_price = ($total - $descuento) - $coins + $regalo + $envio;

        $payer = new Payer();
        $payer->email = Input::get('email');

        # Setting preference properties
        $preference->items = [$item];
        $preference->payer = $payer;
        // $preference->payment_methods = array(
        //     'excluded_payment_types' => array(
        //         array('id' => 'ticket')
        //     )
        // );

        $id_venta = Input::get('id_venta');
        $id_cliente = Input::get('id_user');

        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        $preference->back_urls = [
            "success" => 'http://'.$_SERVER["HTTP_HOST"].'/compras/mercadoOrder/'.$id_cliente.'/'.$id_venta,
            "pending" => 'http://'.$_SERVER["HTTP_HOST"].'/compras/sendEmail/'.$id_venta,
            "failure" => 'http://'.$_SERVER["HTTP_HOST"],
        ];

        $preference->auto_return = "approved";
        $preference->expires = false;

        # Save and POST preference
        $preference->save();

        header('Location: ' . $preference->init_point, true);
        exit;
    }

    public function initMercadoOrderReintentar($id_venta){
        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);

        $total = $ordenes->TOTAL;
        $descuento = $ordenes->DESCUENTO;
        $coins = isset($ordenes->PAGO_COINS) ? $ordenes->PAGO_COINS : 0;
        $preference = new Preference();
        $regalo = $ordenes->REGALO;
        $envio = $ordenes->ENVIO;

        $item = new Item();
        $item->id = 1;
        $item->title = 'Compra en tienda web';
        $item->quantity = 1;
        $item->currency_id = 'MXN';
        $item->unit_price = $ordenes->TOTAL;

        $payer = new Payer();
        $payer->email = Input::get('email');

        # Setting preference properties
        $preference->items = [$item];
        $preference->payer = $payer;
        $preference->payment_methods = array(
            'excluded_payment_types' => array(
                array('id' => 'ticket')
            )
        );

        
        $perfil_usuario = session()->get('perfilUsuario')['usuario'];
        $id_cliente = $perfil_usuario->ID_CLIENTE;

        $preference->back_urls = [
            "success" => 'http://'.$_SERVER["HTTP_HOST"].'/compras/mercadoOrder/'.$id_cliente.'/'.$id_venta,
            "pending" => 'http://'.$_SERVER["HTTP_HOST"].'/compras/sendEmail/'.$id_venta,
            "failure" => 'http://'.$_SERVER["HTTP_HOST"],
        ];

        $preference->auto_return = "approved";
        $preference->expires = false;

        # Save and POST preference
        $preference->save();

        header('Location: ' . $preference->init_point, true);
        exit;
    }

    public function paypalOrder($id_cliente, $id_venta, Request $request){

        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);
        session()->forget('carrito');

        $correos = ['carlos.ibarra@maindsoft.net'];
        // $correos = ['adrian@muletta.com', 'ventas@muletta.com'];
        
        $orden = array(
            'titulo'            => 'Compra realizada',
            'nombre'            => $ordenes->NOMBRE,
            'telefono'          => $ordenes->TELEFONO,
            'email'             => $ordenes->EMAIL,
            'FECHA_VENTA'       => $ordenes->FECHA_VENTA,
            'HORA_VENTA'        => $ordenes->HORA_VENTA,
            'metodo_pago'       => 'Paypal',
            'calle'             =>  $ordenes->CALLE,
            'numero_interior'   =>  $ordenes->NUM_INT,
            'numero_exterior'   =>  $ordenes->NUM_EXT,
            'colonia'           =>  $ordenes->COLONIA,
            'codigo_postal'     =>  $ordenes->CODIGO,
            'municipio'         =>  $ordenes->MUNICIPIO,
            'estado'            =>  $ordenes->ESTADO,
            'monto_desc'        =>  $ordenes->DESCUENTO,
            'regalo'            =>  $ordenes->REGALO,
            'id_orden'          =>  $id_venta,
            'correosMuletta'    =>  $correos,
            'no_ticket'         =>  $ordenes->NO_TICKET,
            'productos'         =>  $productos
        );
        
        /*try {
            Mail::send(new OrderMuletta($orden));
        } catch (\Throwable $th) {
            //throw $th;
        }

        try {
            Mail::send(new OrderPlaced($orden));
        } catch (\Throwable $th) {
            //throw $th;
        }*/
        
        $this->orders->UpdatePay($id_venta, $request->collection_id, 4);
        $this->shopping->updateStatusToProcessing($id_cliente);

        $track_items = '[';
            $subtotal = 0;

            foreach($productos as $key => $product) {
                $track_items = $track_items.'{
                    id: "'.$product->ID_MODELO.'",
                    name: "'.$product->DESCRIPCION.'",
                    variant: "'.$product->TALLA.'",
                    quantity: '.$product->CANTIDAD_VENDIDA.',
                    price: '.$product->PRECIO.'';

                if (count($productos) == ($key + 1)) {
                    $track_items = $track_items.'}';
                }else{
                    $track_items = $track_items.'},';
                }
            }

            $track_items = $track_items.']';

            $pos = strrpos($_SERVER["HTTP_HOST"], "me");
            $pos_prueba = strrpos($_SERVER["HTTP_HOST"], "prueba");

            if ($pos === false && $pos_prueba === false) {
            //     echo '<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68032621-1"></script>';
            //             window.dataLayer = window.dataLayer || [];
            //             function gtag(){dataLayer.push(arguments);}
            //             gtag("js", new Date());
                        
            //             gtag("config", "UA-68032621-1");

            //             gtag(function() {
            //                 console.log(gtag.getAll());
            //             });
                echo '<script>
                    !function(f,b,e,v,n,t,s)
                    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
                        n.queue=[];t=b.createElement(e);t.async=!0;
                        t.src=v;s=b.getElementsByTagName(e)[0];
                        s.parentNode.insertBefore(t,s)}(window, document,"script",
                        "https://connect.facebook.net/en_US/fbevents.js");
                    fbq("init", "357019004873341");
                </script>';

            //         gtag("event", "purchase", {
            //             "transaction_id": '.$id_venta.',
            //             "value": '.$ordenes->TOTAL.',
            //             "currency": "MXN",
            //             "checkout_option": "MercadoPago",
            //             "shipping": 0,
            //             "items": '.$track_items.'
            //         });
                    
                echo '<script type="text/javascript">
                    fbq("track", "Purchase", {
                        value: '.$ordenes->TOTAL.',
                        currency: "MXN",
                        contents: "0",
                        content_type: "product"
                    });
                 </script>';
            }

        return $id_venta;
        
    }

    public function detailsVenta(Request $request){
        $ID_VENTA = $request['ID_VENTA'];
        $REAL = $request['REALIZADO'];

        $ordenes = $this->orders->GetOrderInfo($ID_VENTA);
        $productos = $this->orders->GetOrderProducts($ID_VENTA);

        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        return view('compras.detailsProd', compact('ordenes','productos', 'perfil_usuario', 'REAL', 'ID_VENTA'));

    }
    
    public function changeup(Request $request){

        $carrito = session('carrito');
        $cantidad =  $carrito->productos[$request->position]['qty'] + 1;
        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        $carrito->productos[$request->position]['qty']++;
        $carrito->productos[$request->position]['total'] = $carrito->productos[$request->position]['qty'] * $carrito->productos[$request->position]['price'];

        $total = 0;
        foreach ($carrito->productos As $producto) {
            $total = $total + $producto['total'];
        }

        $carrito->subtotal = $total;
        $carrito->total = $total;

        session(['carrito' => $carrito]);

        return json_encode(1);

    }

    public function changedown(Request $request){

        $carrito = session('carrito');

        if ($carrito->productos[$request->position]['qty'] > 1) {
            $carrito->productos[$request->position]['qty']--;
            $carrito->productos[$request->position]['total'] = $carrito->productos[$request->position]['qty'] * $carrito->productos[$request->position]['price'];

            $total = 0;
            foreach ($carrito->productos As $producto) {
                $total = $total + $producto['total'];
            }

            $carrito->subtotal = $total;
            $carrito->total = $total;

            session(['carrito' => $carrito]);
        }

        return json_encode($carrito->productos[$request->position]);
    }

    public function changedelete(Request $request){

        $carrito = session('carrito');

        $removed_item = $carrito->productos[$request->position];

        \array_splice($carrito->productos, $request->position, 1);

        $total = 0;
        foreach ($carrito->productos As $producto) {
            $total = $total + $producto['total'];
        }

        $carrito->subtotal = $total;
        $carrito->total = $total;


        session(['carrito' => $carrito]);

        return json_encode(['removed_item' => $removed_item]);
    }

    public function hideModal(){
        if (session()->has('modalPago')) {
            session()->forget('carrito');
            session()->forget('modalPago');
            return redirect('/');
        }

    }

    public function hideModalOrders(){
        if (session()->has('modalPago')) {
            session()->forget('carrito');
            session()->forget('modalPago');
            return redirect('/usuario/cuenta/mi-cuenta');
        }

    }
    
    public function carrito_vacio(){
        //Retornamos la vista de carrito vacio
        return view('compras/carrito_vacio');
    }

   public function faq_menu()
    {
        //Retornamos la vista de faq menu
        return view('ayuda/faq/faq_menu');
    }

    public function faq_cuenta()
    {
        $contenido = $this->products->getFooterContent(12);
        //Retornamos la vista de faq cuenta
        return view('ayuda/faq/faq_cuenta', compact('contenido'));
    }

    public function faq_pedidos()
    {
        $contenido = $this->products->getFooterContent(13);
        //Retornamos la vista de faq pedidos
        return view('ayuda/faq/faq_pedidos', compact('contenido'));
    }

    public function faq_proceso_envio()
    {
        $contenido = $this->products->getFooterContent(15);
        //Retornamos la vista de faq proceso_envio
        return view('ayuda/faq/faq_proceso_envio', compact('contenido'));
    }

    public function faq_metodo_pago()
    {
        $contenido = $this->products->getFooterContent(14);
        //Retornamos la vista de faq metodo_pago
        return view('ayuda/faq/faq_metodo_pago', compact('contenido'));
    }

    public function faq_producto()
    {
        $contenido = $this->products->getFooterContent(17);
        //Retornamos la vista de faq producto
        return view('ayuda/faq/faq_producto', compact('contenido'));
    }

    public function faq_distribuidores()
    {
        $contenido = $this->products->getFooterContent(18);
        //Retornamos la vista de faq distribuidores
        return view('ayuda/faq/faq_distribuidores', compact('contenido'));
    }

    public function faq_cambios()
    {
        $contenido = $this->products->getFooterContent(16);
        //Retornamos la vista de faq cambios
        return view('ayuda/faq/faq_cambios', compact('contenido'));
    }

    public function faq_info_gen()
    {
        $contenido = $this->products->getFooterContent(19);
        //Retornamos la vista de faq info_gen
        return view('ayuda/faq/faq_info_gen', compact('contenido'));
    }

     public function facturacion()
    {
        //Retornamos la vista de facturacion
        return view('ayuda/facturacion');
    }

    public function bolsa_trabajo()
    {
        //Retornamos la vista de facturacion
        return view('ayuda/bolsa_trabajo');
    }
    public function contacto()
    {
        //Retornamos la vista de facturacion
        return view('ayuda/contacto');
    }

    public function politicas_envio()
    {
        $contenido = $this->products->getFooterContent(6);
        if ($contenido) {
            $contenido = $contenido[0];
        }else{
            $contenido = null;
        }
        //Retornamos la vista de politicas_envio
        return view('ayuda/general', compact('contenido'));
    }

    public function politicas_cambios_devoluciones()
    {
        $contenido = $this->products->getFooterContent(7);
        if ($contenido) {
            $contenido = $contenido[0];
        }else{
            $contenido = null;
        }
        //Retornamos la vista de politicas_cambios_devoluciones
        return view('ayuda/general', compact('contenido'));
    }

    public function politicas_envio_politicas()
    {
        $contenido = $this->products->getFooterContent(8);
        if ($contenido) {
            $contenido = $contenido[0];
        }else{
            $contenido = null;
        }
        //Retornamos la vista de politicas_envio
        return view('ayuda/general', compact('contenido'));
    }

    public function politicas_cambios_devoluciones_politicas()
    {
        $contenido = $this->products->getFooterContent(9);
        if ($contenido) {
            $contenido = $contenido[0];
        }else{
            $contenido = null;
        }
        //Retornamos la vista de politicas_cambios_devoluciones
        return view('ayuda/general', compact('contenido'));
    }

    public function terminos_condiciones()
    {
        $contenido = $this->products->getFooterContent(10);
        if ($contenido) {
            $contenido = $contenido[0];
        }else{
            $contenido = null;
        }
        //Retornamos la vista de terminos_condiciones
        return view('ayuda/general', compact('contenido'));
    }
    
    public function aviso_privacidad()
    {
        $contenido = $this->products->getFooterContent(11);
        if ($contenido) {
            $contenido = $contenido[0];
        }else{
            $contenido = null;
        }
        //Retornamos la vista de aviso_privacidad
        return view('ayuda/general', compact('contenido'));
    }
    public function catalogo_2018()
    {
        //Retornamos la vista de catalogo_2018
        return view('ayuda/catalogo_2018');
    }
    
    public function guia_tallas()
    {
        $contenido = $this->products->getFooterContent(20);

        //Retornamos la vista de guia_tallas
        return view('ayuda/guia_tallas', compact('contenido'));
    }

    public function getDesc(Request $request){
        $desc = $this->shopping->getDesc($request->promocion);
        return json_encode($desc);
    }

    public function SendEmail($id_venta, Request $request){

        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);
        $this->orders->UpdatePay($id_venta, $request->collection_id, $ordenes->ID_METODO_PAGO);

        $correos = ['carlos.ibarra@maindsoft.net'];
        // $correos = ['adrian@muletta.com', 'ventas@muletta.com'];

        $orden = array(
            'titulo'            => 'Compra generada como pendiente de pago',
            'nombre'            => $ordenes->NOMBRE,
            'telefono'          => $ordenes->TELEFONO,
            'email'             => $ordenes->EMAIL,
            'FECHA_VENTA'       => $ordenes->FECHA_VENTA,
            'HORA_VENTA'        => $ordenes->HORA_VENTA,
            'metodo_pago'       => 'MercadoPago',
            'calle'             =>  $ordenes->CALLE,
            'numero_interior'   =>  $ordenes->NUM_INT,
            'numero_exterior'   =>  $ordenes->NUM_EXT,
            'colonia'           =>  $ordenes->COLONIA,
            'codigo_postal'     =>  $ordenes->CODIGO,
            'municipio'         =>  $ordenes->MUNICIPIO,
            'estado'            =>  $ordenes->ESTADO,
            'monto_desc'        =>  $ordenes->DESCUENTO,
            'regalo'            =>  $ordenes->REGALO,
            'id_orden'          =>  $id_venta,
            'no_ticket'         =>  $ordenes->NO_TICKET,
            'correosMuletta'    =>  $correos,
            'productos'         =>  $productos
        );
        
        /*try {
            Mail::send(new OrderMuletta($orden));
        } catch (\Throwable $th) {
        }
        try {
            Mail::send(new OrderPlaced($orden));
        } catch (\Throwable $th) {
        }*/

        // return view('mail.compras', compact('orden'));

        return redirect()->route('detailsVenta', $id_venta);
    }

    public function detailsDevolucion($ID_VENTA){
        
        $motivos = $this->orders->getMotivos();

        $ordenes = $this->orders->GetOrderInfo($ID_VENTA);
        $productos = $this->orders->GetOrderProducts($ID_VENTA);

        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        return view('producto.proceso-devolucion', compact('ordenes','productos', 'motivos', 'perfil_usuario', 'ID_VENTA'));

    }

    public function insertRetorno(Request $request){
        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        $id_user = $perfil_usuario->ID_CLIENTE;
        $id_venta = $request->ID_VENTA;
        $razones = $request->RAZONES;
        $id_motivo = $request->ID_MOTIVO;
        $total = $request->TOTAL;
        $productos = $request->PRODUCTOS;

        $id_devolucion = $this->orders->insertReturn($id_user, $id_venta, $razones, $total, $id_motivo);

        $array = array();
        foreach ($productos as $key => $value) {
            $value['ID_DEVOLUCION'] = $id_devolucion->ID_DEVOLUCION_WEB;

            array_push($array, $value);
        }

        $productos = $this->orders->InsertReturnProduct(json_encode($array));

        /*try {
            Mail::send(new DevolucionMuletta($id_devolucion, $productos, $perfil_usuario));
        } catch (\Throwable $th) {
            //throw $th;
        }*/

        return json_encode($productos);

    }


    public function MulettaPago($id_venta){

        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);
        session()->forget('carrito');

        $correos = ['adrian@muletta.com', 'ventas@muletta.com'];
        
        $orden = array(
            'titulo'            => 'Compra realizada',
            'nombre'            => $perfil_usuario->NOMBRE,
            'telefono'          => $perfil_usuario->TELEFONO,
            'email'             => $perfil_usuario->EMAIL,
            'FECHA_VENTA'       => $ordenes->FECHA_VENTA,
            'HORA_VENTA'        => $ordenes->HORA_VENTA,
            'metodo_pago'       => 'Muletta Cash',
            'calle'             =>  $ordenes->CALLE,
            'numero_interior'   =>  $ordenes->NUM_INT,
            'numero_exterior'   =>  $ordenes->NUM_EXT,
            'colonia'           =>  $ordenes->COLONIA,
            'codigo_postal'     =>  $ordenes->CODIGO,
            'municipio'         =>  $ordenes->MUNICIPIO,
            'estado'            =>  $ordenes->ESTADO,
            'monto_desc'        =>  $ordenes->DESCUENTO,
            'regalo'            =>  $ordenes->REGALO,
            'id_orden'          =>  $id_venta,
            'correosMuletta'    =>  $correos,
            'no_ticket'         =>  $ordenes->NO_TICKET,
            'productos'         =>  $productos
        );
        
        /*try {
            Mail::send(new OrderMuletta($orden));
        } catch (\Throwable $th) {
            //throw $th;
        }

        try {
            Mail::send(new OrderPlaced($orden));
        } catch (\Throwable $th) {
            //throw $th;
        }*/
        
        $this->shopping->updateStatusToProcessing($perfil_usuario->ID_CLIENTE);

        return redirect()->route('detailsVenta', ['ID_VENTA' => $id_venta]);
        
    }

    public function payTarjeta($id_venta, $id_client = null){

        try {

            $usuario = null;
            if ($id_client != null) {
                $usuario = $this->user->getUser($id_client);
                session(['perfilUsuario' => compact('usuario')]);
            }else{
                $usuario = session()->get('perfilUsuario')['usuario'];
            }

            
            $openpay = Openpay::getInstance(env('OPENPAY_ID'), env('OPENPAY_SK'), env('OPENPAY_CURRENCY'));
            // $openpay = Openpay::getInstance(env('OPENPAY_SANDBOX_ID'), env('OPENPAY_SANDBOX_SK'), env('OPENPAY_CURRENCY'));

            $id_cliente = '';

            if (!$usuario->ID_OPENPAY) {
                $customerData = array(
                    'external_id' => $usuario->ID_CLIENTE,
                    'name' => $usuario->NOMBRE,
                    'last_name' => '',
                    'email' => $usuario->EMAIL,
                    'requires_account' => false,
                    'phone_number' => $usuario->TELEFONO,
                );
                
                $customer = $openpay->customers->add($customerData);
                
                $id_cliente = $customer->id;
                $usuario->ID_OPENPAY = $customer->id;
                $this->user->updateOpenPay($usuario->ID_CLIENTE, $customer->id);
                if ($id_client == null) {
                    session(['perfilUsuario' => compact('usuario')]);
                }

            }else{
                $id_cliente = $usuario->ID_OPENPAY;
            }


            return view('compras.PagoTarjeta', compact('id_cliente', 'id_venta', 'id_client'));

        } catch (Exception $e) {
            echo '<pre>';
            var_dump($e);
        }

        // Alert::error('Error', 'Ha ocurrido un error al procesar la compra');
        // return redirect()->route('index');

    }

    public function transferenciaPago($id_venta, Request $request){
        $openpay = Openpay::getInstance(env('OPENPAY_ID'), env('OPENPAY_SK'), env('OPENPAY_CURRENCY'));
        // $openpay = Openpay::getInstance(env('OPENPAY_SANDBOX_ID'), env('OPENPAY_SANDBOX_SK'), env('OPENPAY_CURRENCY'));

        $ordenes = $this->orders->GetOrderInfo($id_venta);

        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        $cliente = array(
            'name' => $perfil_usuario->NOMBRE,
            'last_name' => '',
            'email' => $perfil_usuario->EMAIL,
            'phone_number' => $perfil_usuario->TELEFONO,
        );

        $chargeData = array(
            'method' => 'bank_account',
            'amount' => number_format(floatval($ordenes->TOTAL), 2, '.', ''),
            'description' => 'Cargo con banco '.$id_venta,
            'order_id' => $id_venta,
            'customer' => $cliente);

        $charge = $openpay->charges->create($chargeData);

        // Alert::error('Success', 'Ha ocurrido un error al procesar la compra');
        return view('compras.transferencia', compact('charge', 'id_venta', 'ordenes', 'perfil_usuario'));
    }

    public function paynetPago($id_venta, Request $request){
        $openpay = Openpay::getInstance(env('OPENPAY_ID'), env('OPENPAY_SK'), env('OPENPAY_CURRENCY'));
        // $openpay = Openpay::getInstance(env('OPENPAY_SANDBOX_ID'), env('OPENPAY_SANDBOX_SK'), env('OPENPAY_CURRENCY'));

        $ordenes = $this->orders->GetOrderInfo($id_venta);

        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        $cliente = array(
            'name' => $perfil_usuario->NOMBRE,
            'last_name' => '',
            'email' => $perfil_usuario->EMAIL,
            'phone_number' => $perfil_usuario->TELEFONO,
        );

        $chargeData = array(
            'method' => 'store',
            'amount' => number_format(floatval($ordenes->TOTAL), 2, '.', ''),
            'description' => 'Cargo a tienda '.$id_venta,
            'customer' => $cliente);

        $charge = $openpay->charges->create($chargeData);

        // $customer = $openpay->customers->get($perfil_usuario->ID_OPENPAY);
        // $charge = $customer->charges->create($chargeData);

        $pdf = app('dompdf.wrapper');
        $html = view('compras.tienda', compact('charge', 'id_venta', 'ordenes', 'perfil_usuario'));

        return $html;
        // return PDF::loadHTML($html)->stream($id_venta.'.pdf');
        // return PDF::loadHTML($html)->download($id_venta.'.pdf');
    }


    public function realizarPago($id_venta, $id_client = null, Request $request){

        $perfil_usuario = null;
        if ($id_client) {
            $perfil_usuario = $this->user->getUser($id_client);
            session()->forget('perfilUsuario');
        }else{
            $perfil_usuario = session()->get('perfilUsuario')['usuario'];
        }

        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);
        $error = null;

        try {

            $openpay = Openpay::getInstance(env('OPENPAY_ID'), env('OPENPAY_SK'), env('OPENPAY_CURRENCY'));
            // $openpay = Openpay::getInstance(env('OPENPAY_SANDBOX_ID'), env('OPENPAY_SANDBOX_SK'), env('OPENPAY_CURRENCY'));
            
            $id_cliente = $perfil_usuario->ID_OPENPAY;

            $cliente = array(
                'name' => $perfil_usuario->NOMBRE,
                'last_name' => '',
                'email' => $perfil_usuario->EMAIL,
                'phone_number' => $perfil_usuario->TELEFONO,
            );

            $url = Route('finalizarOpenpaySecure', ['ID_VENTA' => $id_venta, 'ID_CLIENTE' => $perfil_usuario->ID_CLIENTE]);

            if (!$perfil_usuario->ID_OPENPAY) {
                $chargeRequest = array(
                    'method' => 'card',
                    'source_id' => $request->token_id,
                    'amount' => number_format(floatval($ordenes->TOTAL), 2, '.', ''),
                    'currency' => 'MXN',
                    'description' => 'Pago de cuenta: '.$id_venta,
                    'order_id' => $id_venta,
                    'device_session_id' => $request->device_id,
                    'use_3d_secure' => true,
                    'redirect_url' => $url,
                    'customer' => $cliente);

                $charge = $openpay->charges->create($chargeRequest);
            }else{
                $chargeRequest = array(
                    'method' => 'card',
                    'source_id' => $request->token_id,
                    'amount' => number_format(floatval($ordenes->TOTAL), 2, '.', ''),
                    'currency' => 'MXN',
                    'description' => 'Pago de cuenta: '.$id_venta,
                    'order_id' => $id_venta,
                    'use_3d_secure' => true,
                    'redirect_url' => $url,
                    'device_session_id' => $request->device_id);

                $customer = $openpay->customers->get($perfil_usuario->ID_OPENPAY);
                $charge = $customer->charges->create($chargeRequest);
            }

            $this->orders->UpdatePay($id_venta, $charge->id, 18);

            header('Location: ' . $charge->payment_method->url, true);
            exit;

        } catch (OpenpayApiTransactionError $e) {
            $error = [
                'category' => $e->getCategory(),
                'error_code' => $e->getErrorCode(),
                'description' => $e->getMessage(),
                'http_code' => $e->getHttpCode(),
                'request_id' => $e->getRequestId()
            ];
        } catch (OpenpayApiRequestError $e) {
            $error = [
                'category' => $e->getCategory(),
                'error_code' => $e->getErrorCode(),
                'description' => $e->getMessage(),
                'http_code' => $e->getHttpCode(),
                'request_id' => $e->getRequestId()
            ];
        } catch (OpenpayApiConnectionError $e) {
            $error = [
                'category' => $e->getCategory(),
                'error_code' => $e->getErrorCode(),
                'description' => $e->getMessage(),
                'http_code' => $e->getHttpCode(),
                'request_id' => $e->getRequestId()
            ];
        } catch (OpenpayApiAuthError $e) {
            $error = [
                'category' => $e->getCategory(),
                'error_code' => $e->getErrorCode(),
                'description' => $e->getMessage(),
                'http_code' => $e->getHttpCode(),
                'request_id' => $e->getRequestId()
            ];
        } catch (OpenpayApiError $e) {
            $error = [
                'category' => $e->getCategory(),
                'error_code' => $e->getErrorCode(),
                'description' => $e->getMessage(),
                'http_code' => $e->getHttpCode(),
                'request_id' => $e->getRequestId()
            ];
        } catch (Exception $e) {
            $error = [
                'category' => $e->getCategory(),
                'error_code' => $e->getErrorCode(),
                'description' => $e->getMessage(),
                'http_code' => $e->getHttpCode(),
                'request_id' => $e->getRequestId()
            ];
        }

        
        if ($error) {
            $message = $this->shopping->ErrorCodeMessage($error);
            
            Alert::error($error['error_code'], $message);
            return redirect()->route('OpenPayRegistro', ['ID_VENTA' => $id_venta, 'ID_CLIENT' => $id_client]);
        }
        
    }

    public function finalizarOpenpaySecure($id_venta, $id_cliente, Request $request){

        $perfil_usuario = $this->user->getUser($id_cliente);

        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);

        $correos = ['carlos.ibarra@maindsoft.net'];
        // $correos = ['adrian@muletta.com', 'ventas@muletta.com'];
            
        $orden = array(
            'titulo'            => 'Compra realizada',
            'nombre'            => $perfil_usuario->NOMBRE,
            'telefono'          => $perfil_usuario->TELEFONO,
            'email'             => $perfil_usuario->EMAIL,
            'FECHA_VENTA'       => $ordenes->FECHA_VENTA,
            'HORA_VENTA'        => $ordenes->HORA_VENTA,
            'metodo_pago'       => 'Tarjeta de debito/credito',
            'calle'             =>  $ordenes->CALLE,
            'numero_interior'   =>  $ordenes->NUM_INT,
            'numero_exterior'   =>  $ordenes->NUM_EXT,
            'colonia'           =>  $ordenes->COLONIA,
            'codigo_postal'     =>  $ordenes->CODIGO,
            'municipio'         =>  $ordenes->MUNICIPIO,
            'estado'            =>  $ordenes->ESTADO,
            'monto_desc'        =>  $ordenes->DESCUENTO,
            'regalo'            =>  $ordenes->REGALO,
            'id_orden'          =>  $id_venta,
            'correosMuletta'    =>  $correos,
            'no_ticket'         =>  $ordenes->NO_TICKET,
            'productos'         =>  $productos
        );

        $openpay = Openpay::getInstance(env('OPENPAY_ID'), env('OPENPAY_SK'), env('OPENPAY_CURRENCY'));
        // $openpay = Openpay::getInstance(env('OPENPAY_SANDBOX_ID'), env('OPENPAY_SANDBOX_SK'), env('OPENPAY_CURRENCY'));

        $chargeStatus = $openpay->charges->get($request->id);

        if ($chargeStatus->status === 'completed' || $chargeStatus->status == 'completed') {
            $this->shopping->updateStatusToProcessing($perfil_usuario->ID_CLIENTE);

            /*try {
                Mail::send(new OrderMuletta($orden));
            } catch (\Throwable $th) {
                //throw $th;
            }

            try {
                Mail::send(new OrderPlaced($orden));
            } catch (\Throwable $th) {
                //throw $th;
            }*/

            $track_items = '[';
            $subtotal = 0;

            foreach($productos as $key => $product) {
                $track_items = $track_items.'{
                    id: "'.$product->ID_MODELO.'",
                    name: "'.$product->DESCRIPCION.'",
                    variant: "'.$product->TALLA.'",
                    quantity: '.$product->CANTIDAD_VENDIDA.',
                    price: '.$product->PRECIO.'';

                if (count($productos) == ($key + 1)) {
                    $track_items = $track_items.'}';
                }else{
                    $track_items = $track_items.'},';
                }
            }

            $track_items = $track_items.']';

            $pos = strrpos($_SERVER["HTTP_HOST"], "me");
            $pos_prueba = strrpos($_SERVER["HTTP_HOST"], "prueba");

            if ($pos === false && $pos_prueba === false) {
            //     echo '<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68032621-1"></script>';
            //             window.dataLayer = window.dataLayer || [];
            //             function gtag(){dataLayer.push(arguments);}
            //             gtag("js", new Date());
                        
            //             gtag("config", "UA-68032621-1");

            //             gtag(function() {
            //                 console.log(gtag.getAll());
            //             });
            
            //         gtag("event", "purchase", {
            //             "transaction_id": '.$id_venta.',
            //             "value": '.$ordenes->TOTAL.',
            //             "currency": "MXN",
            //             "checkout_option": "MercadoPago",
            //             "shipping": 0,
            //             "items": '.$track_items.'
            //         });
            
                 echo '<script>
                        !function(f,b,e,v,n,t,s)
                        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
                            n.queue=[];t=b.createElement(e);t.async=!0;
                            t.src=v;s=b.getElementsByTagName(e)[0];
                            s.parentNode.insertBefore(t,s)}(window, document,"script",
                            "https://connect.facebook.net/en_US/fbevents.js");
                        fbq("init", "357019004873341");
                     </script>';

                 echo '<script type="text/javascript">
                    fbq("track", "Purchase", {
                        value: '.$ordenes->TOTAL.',
                        currency: "MXN",
                        contents: "0",
                        content_type: "product"
                    });
                 </script>';
            }

            $perfil_usuario = session('perfilUsuario')['usuario'];
            $user_data = (new UserData())
                ->setFbc('fb.1.'.time().'.AbCdEfGhIjKlMnOpQrStUvWxYz1234567890')
                ->setFbp('fb.1.'.time().'.1098115397');
            
            if ($perfil_usuario) {
                $user_data->setEmail($perfil_usuario->EMAIL)
                    ->setFirstName($perfil_usuario->NOMBRE)
                    ->setLastName($perfil_usuario->APELLIDO_PATERNO);
                    //->setCity($perfil_usuario->APELLIDO_PATERNO)
                    // ->setCountryCode('mx')
                    // ->setDateOfBirth(str_replace('-','',substr($perfil_usuario->FECHA_NACIMIENTO, 0, 10)))
                    // ->setPhone($perfil_usuario->TELEFONO)
                    // ->setState($perfil_usuario->ESTADO)
                    // ->setZipCode($perfil_usuario->CODIGO);
            }

            $custom_data = (new CustomData())
                ->setCurrency('MXN')
                ->setValue(floatval($ordenes->TOTAL));

            $url = 'http://'.$_SERVER["HTTP_HOST"].'/compras/PagoTarjeta/'.$id_venta.'/'.$id_cliente;

            $event = (new Event())
                ->setEventName('Purcharse')
                ->setActionSource("website")
                ->setEventSourceUrl($url)
                ->setEventTime(time())
                ->setUserData($user_data)
                ->setCustomData($custom_data);

            $events = array();
            array_push($events, $event);

            $request2 = (new EventRequest($this->pixel_id))
             ->setEvents($events);
            //     ->setTestEventCode('TEST61134');
            //$response = $request2->execute();
            
            Alert::success('Completado', 'El pago se ha realizado satisfactoriamente');
            // return redirect()->route('detailsVenta', ['ID_VENTA' => $id_venta]);

            $REAL = null;
            $ID_VENTA = $id_venta;
            return view('compras.detailsProd', compact('ordenes','productos', 'perfil_usuario', 'REAL', 'ID_VENTA'));
        }else {
            $id_client = $id_cliente;
            $id_cliente = $perfil_usuario->ID_OPENPAY;

            // Alert::error($chargeStatus->serializableData['error_code'], $chargeStatus->serializableData['error_message']);
            // return redirect()->route('OpenPayRegistro', ['ID_VENTA' => $id_venta, 'ID_CLIENT' => $id_cliente]);
            $errorMessage = $chargeStatus->serializableData['error_message'];

            if($chargeStatus->serializableData['error_code'] == 3001 || $chargeStatus->serializableData['error_code'] == 3004 || $chargeStatus->serializableData['error_code'] == 3005){
                $errorMessage = "Tarjeta declinada";
            }else if($chargeStatus->serializableData['error_code'] == 3002) {
                $errorMessage = "La tarjeta ha expirado";
            } elseif ($chargeStatus->serializableData['error_code'] == 3003) {
                $errorMessage = "Fondos insuficientes";
            }

            return view('compras.PagoTarjeta', compact('id_cliente', 'id_venta', 'id_client', 'errorMessage'));
        }

    }

    //Funiones proporcionadadas por el banco para encriptar
    public static function encriptar($plaintext, $key128)
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-128-cbc'));
        $cipherText = openssl_encrypt($plaintext, 'AES-128-CBC', hex2bin($key128), 1, $iv);
        return base64_encode($iv . $cipherText);
    }
    //Funiones proporcionadadas por el banco para desencriptar
    public static function desencriptar($encodedInitialData, $key128)
    {
        $encodedInitialData = base64_decode($encodedInitialData);
        $iv = substr($encodedInitialData, 0, 16);
        $encodedInitialData = substr($encodedInitialData, 16);
        return openssl_decrypt($encodedInitialData, 'aes-128-cbc', hex2bin($key128), 1, $iv);
    }
    //Esta es la función del redirect - Procesa el pedido despues si la transaccion es correcta  - Utilizar el ID de direccion 
    public function webpay_confirmacion(Request $request)
    {   //En el Redirec se obtienen los parametros edl Request
        $request_nbResponse = $request['nbResponse'];
        $request_idLiga = $request['idLiga'];
        $request_referencia = $request['referencia'];
        $request_importe = $request['importe'];
        $request_email = $request['email'];
        $request_nuAut = $request['nuAut'];
        $request_cdResponse = $request['cdResponse'];
        $request_nb_error = $request['nb_error'];
        $request_nbMoneda = $request['nbMoneda'];
        //Si fue aprobada se procesa la orden de compra
        if ($request_nbResponse == "Aprobado") {
            //Se obtienen los datos del usuario
            $perfil_usuario = session()->get('perfilUsuario')['usuario'];
            // IMPORTANTE :  El id del envio se tiene que obtner  por medio de la referencia para poder ser obtenido
            $direccion = explode(" ", $request['referencia']);
            echo $origen = 1;
            //Se le realiza un split para obtner el ID de la direecion
            echo $id_direccion = $direccion[1];
            
            $id_cliente = $perfil_usuario->ID_CLIENTE;
            $nombre = $request->NOMBRE;
            $telefono = $request->TELEFONO;
            $email = $request->EMAIL;
            //Se obtienen la dirección del del envio
            $address = (array)$this->user->getAddressData($id_direccion, $origen, $email);
            
            $alias = $address["ALIAS"];
            $calle = $address["CALLE"];
            $numero_interior = $address["NUM_INT"];
            $numero_exterior = $address["NUM_EXT"];
            $colonia = $address["COLONIA"];
            $codigo_postal = $address["CODIGO"];
            $municipio = $address["MUNICIPIO"];
            $estado = $address["ESTADO"];
            
            $monto_desc = $request->monto_desc;
            $regalo = $request->regalo;
            $id_desc = $request->id_desc;
            $referencia = $request->referencia;
            $pago_coins = $request->coins;
            
            $id_pago = null;
            $id_orden = null;
            
            $pais = "México";
            $metodo_pago = '16'; // Este es el ID de webpay
            $descripcion_pago = '';
            $nueva_direccion = $request->guardar;
            
            $pago = $this->shopping->updateFinalSale($id_cliente, $alias, $email, $calle, $numero_interior, $numero_exterior, $colonia, $codigo_postal, $municipio, $estado, $pais, $metodo_pago, $descripcion_pago, $nueva_direccion, $id_desc, $monto_desc, $regalo, $id_pago, $referencia, $pago_coins);
            $inv = $this->shopping->updateSalesInventory($id_cliente);
            
            $id_venta = $pago[0]->ID_VENTA;
            //Se obtienen los el ID de la Orden y de los produuctos
            $ordenes = $this->orders->GetOrderInfo($id_venta);
            $productos = $this->orders->GetOrderProducts($id_venta);
            session()->forget('carrito');
            
            $correos = ['adrian@muletta.com', 'ventas@muletta.com'];
            //Se obtienen los datos de envio por correo
            $orden = array(
                'titulo' => 'Compra realizada',
                'nombre' => $ordenes->NOMBRE,
                'telefono' => $ordenes->TELEFONO,
                'email' => $ordenes->EMAIL,
                'FECHA_VENTA' => $ordenes->FECHA_VENTA,
                'HORA_VENTA' => $ordenes->HORA_VENTA,
                'metodo_pago' => 'WebPay',
                'calle' => $ordenes->CALLE,
                'numero_interior' => $ordenes->NUM_INT,
                'numero_exterior' => $ordenes->NUM_EXT,
                'colonia' => $ordenes->COLONIA,
                'codigo_postal' => $ordenes->CODIGO,
                'municipio' => $ordenes->MUNICIPIO,
                'estado' => $ordenes->ESTADO,
                'monto_desc' => $ordenes->DESCUENTO,
                'regalo' => $ordenes->REGALO,
                'id_orden' => $id_venta,
                'correosMuletta' => $correos,
                'no_ticket' => $ordenes->NO_TICKET,
                'productos' => $productos
            );
            //Se procesan y se mandan 
            /*try {
                Mail::send(new OrderMuletta($orden));
            } catch (\Throwable $th) {
                //throw $th;
            }
            
            try {
                Mail::send(new OrderPlaced($orden));
            } catch (\Throwable $th) {
                //throw $th;
            }*/
            
            $this->orders->UpdatePay($id_venta, $request->collection_id, 16);
            $this->shopping->updateStatusToProcessing($id_cliente);
            
        } else if ($request_nbResponse == "Rechazado") {
                //echo "Rechazado";
            if ($request_nb_error == "FONDOS INSUFICIENTES") {
                //echo  "FONDOS";
            } else {
                //echo "Rechazado 3Ds";
            }
        }
        //Si se requiere generar el archivo XML / Se tiene que crear el dir storage/app y utilizar lass sig lib y descomentar la ultima linea
        //use Illuminate\Support\Facades\Storage;
        //use Illuminate\Http\File;
        //Storage::disk('local')->put('C-' . date('h:i:s-d:m:Y-') . strtoupper($request['nbResponse']) . '.txt', $request_nbResponse);
        return view('compras.confirmation', compact('request_nbResponse', 'request_idLiga', 'request_referencia', 'request_importe', 'request_email', 'request_nuAut', 'request_cdResponse', 'request_nb_error', 'request_nbMoneda'));
    }
    //Funcion para procesar la url
    public function webpay_url(Request $request)
    {
        date_default_timezone_set('America/Mexico_City');
        //Se genera la conexion a al DB para otener los datos del banco
        $data1 = (array)$this->shopping->getConexion()[0];
        
        $carrito = session('carrito');
        $perfil_usuario = session('perfilUsuario')['usuario'];
        //Se recibe los parametros de ID_VENTA y ID_Direccion
        $operacion = $request->input('id_venta');
        $id_direccion = $request->input('id_direccion');

        $client = (object)[
            'email' => $perfil_usuario ? $perfil_usuario->EMAIL : '',
        ];
        //Se crea al fecha del pedido
        $email = $client->email;
        $fecha = date('d/m/Y');
        $client = new Client();
        
        $costos = $this->shopping->getCostSend();
        $envio = $costos[0]->VALOR_COSTO;
        //Se define si exite o no un envio
        if ($carrito->total >= 999) {
            $precio = $carrito->total;
        } else {
            $precio = $carrito->total + $envio;
        }
        //Se obtiene el Key de la DB
        $key128_AES = $data1['semilla_AES']; //PROD
        //Se genera el XML para procesar
        $plaintext = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<P>
<business>
<id_company>${data1['id_company']}</id_company>
<id_branch>${data1['id_branch']}</id_branch>
<user>${data1['user']}</user>
<pwd>${data1['password']}</pwd>
</business>
  <url>
    <reference>$operacion $id_direccion</reference>
    <amount>$precio</amount>
    <moneda>MXN</moneda>
    <canal>W</canal>
    <omitir_notif_default>1</omitir_notif_default>
    <promociones>C</promociones>s
    <st_correo>1</st_correo>
    <fh_vigencia>$fecha</fh_vigencia>
    <mail_cliente>$email</mail_cliente>
  </url>
</P>
XML;
        //Se otiene el Ddata0 de la DB
        $data0 = $data1['data0'];
        //Se encripta el XML 
        $encriptardata = $this->encriptar($plaintext, $key128_AES);
        //Se reencriopta con el pgs
        $data = '<pgs><data0>' . $data0 . '</data0><data>' . $encriptardata . '</data></pgs>';
        $options = [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
            'form_params' => ['xml' => $data],
        ];
        try {
            //Se obtiene la respuesta de la URL
            $response = $client->request('post', $data1['url'], $options);
            $responseText = $response->getBody()->getContents();
            //Se desencriipta y se procesa para el envio
            $responseText = self::desencriptar($responseText, $key128_AES);

        } catch (\Exception $e) {
            $responseText = $e->getMessage();
        }
        return ($responseText);
    }
    //Esta es la funcion del POST para las respuestas del Webhook
    public function webpay(Request $request)
    {
        date_default_timezone_set('America/Mexico_City');

        //Se genera la conexion a al DB para otener los datos del banco
        $data1 = (array)$this->shopping->getConexion()[0];
        
        $key128_AES = $data1['semilla_AES']; //PROD
        //Una vez generado la el strResponse se almacena y se desencripta
        $originalString = $request->post('strResponse');
        $decryptedString = self::desencriptar($originalString, $key128_AES);
        //Se desencripta y se procesa para el uso de almacenamientao
        $xml = simplexml_load_string($decryptedString);
        $json = json_encode($xml);
        $array = json_decode($json, true);
        
        //Dependiendo del estatus se mandan los parametros requeridos para el procesamiento
        if ($array['response'] == "approved") {
            $a = 'NULL';
            $b = $array['reference'];
            $c = $array['response'];
            $d = $array['foliocpagos'];
            $e = $array['auth'];
            $f = 'NULL';
            $g = $array['date'];
            $h = $array['nb_merchant'];
            $i = $array['cc_type'];
            $j = $array['tp_operation'];
            $k = $array['amount'];
            $l = $array['id_url'];
            $m = $array['email'];
            $n = 'NULL';
            $o = $array['cc_mask'];
            $resWebpay = $this->shopping->inserstWebpay($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o);
        } else if ($array['response'] == "denied") {
            $b = $array['reference'];
            $c = $array['response'];
            $d = 'NULL';
            $l = $array['id_url'];
            $m = $array['email'];
            $resWebpay = $this->shopping->inserstWebpay($a = 'NULL', $b, $c, $d, $e = 'NULL', $f = 'NULL', $g = 'NULL', $h = 'NULL', $i = 'NULL', $j = 'NULL', $k = 'NULL', $l, $m, $n = 'NULL', $o = 'NULL');
        } else if ($array['response'] == "error") {
            $b = $array['reference'];
            $c = $array['response'];
            $l = $array['id_url'];
            $m = $array['email'];
            $resWebpay = $this->shopping->inserstWebpay($a = 'NULL', $b, $c, $d = 'NULL', $e = 'NULL', $f = 'NULL', $g = 'NULL', $h = 'NULL', $i = 'NULL', $j = 'NULL', $k = 'NULL', $l, $m, $n = 'NULL', $o = 'NULL');
        }
        //Si se requiere generar el archivo XML / Se tiene que crear el dir storage/app y utilizar lass sig lib y descomentar la ultima linea
        //use Illuminate\Support\Facades\Storage;
        //use Illuminate\Http\File;
        //Storage::disk('local')->put('R-' . date('h:i:s-d:m:Y-') . strtoupper($array['response']) . '.txt', $decryptedString);
    }
    
    public function clearCart(){
        session()->forget('carrito');
        return redirect('/usuario/cuenta/pedidos');
    }

    public function reintentarPago($id_venta){
        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);

        $client = session()->get('perfilUsuario')['usuario'];

        return view( 'compras.reintentarPago', compact('id_venta', 'ordenes', 'productos', 'client') );
    }
}
