<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Librería donde se encuentran nuestros métodos de productos
use App\Repositories\Orders;
use App\Repositories\Users;

class UsersController extends Controller
{
    //Inyectamos la clase Orders por medeio del contructor e importamos la clase
    protected $orders;
    protected $user;

    //Creamos el constructor de la clase el cual nos creara una instancia de la clase Orders
    public function __construct(Orders $orders, Users $user)
    {
        $this->orders = $orders;
        $this->user = $user;
    }

    // Metodo para obtener el Dinero virtual del cliente
    public function getVirtualMoney($id_cliente){
        $virtualMoney = $this->user->getVirtualMoney($id_cliente);
        session(['virtualMoney' => compact('virtualMoney')]);
    }

    public function showMyAccount(){
        $perfil_usuario = session()->get('perfilUsuario')['usuario'];
        //En la variable productos guardamos el resultado que nos retorna el método getAllProducts de la clase products
        $ordenes = $this->orders->GetClientOrders($perfil_usuario->ID_CLIENTE, 0);
        $productos=[];
        foreach ($ordenes as $orden){
            array_push ($productos, $this->orders->GetOrderProducts($orden->ID_VENTA));
        }
        //dd($productos);
        return view('usuario.cuenta.mi-cuenta',compact('ordenes','productos', 'perfil_usuario'));
    }

    public function showReturns(){
        $perfil_usuario = session()->get('perfilUsuario')['usuario'];
        //En la variable productos guardamos el resultado que nos retorna el método getAllProducts de la clase products
        $ordenes = $this->orders->obtenerDevolucionesCliente($perfil_usuario->ID_CLIENTE);
        $productos=[];
        foreach ($ordenes as $orden){
            array_push ($productos, $this->orders->obtenerProductosDevolucion($orden->ID_DEVOLUCION_WEB));
        }
        //dd($productos);
        return view('usuario.cuenta.devoluciones', compact('ordenes','productos', 'perfil_usuario'));
    }
    
    public function showEditAccount(){
        return view('usuario.cuenta.editar-cuenta');
    }

    public function showEditAdress(){
        $perfil_usuario = session('perfilUsuario')['usuario'];

        $direcciones = $this->user->GetClientAddresses($perfil_usuario->EMAIL);
        $estados = $this->user->GetEstates();

        return view('usuario.cuenta.editar-direcciones', compact('perfil_usuario', 'direcciones', 'estados'));
    }

    public function obtenerMunicipios(Request $request){
        return json_encode($this->user->getMunicipios($request->ID_ESTADO));
    }

    public function updateAccount(){
        $editadas = $_POST;
        $nombre = $_POST['nombre'];
        $apellidoP = $_POST['apellidoP'];
        $apellidoM = $_POST['apellidoM'];
        $email_ant = $_POST['email_ant'];
        $telefono = $_POST['tel'];
        $dia = $_POST['dia'];
        $mes = $_POST['mes'];
        $anio = $_POST['anio'];
        $fecha = $anio.'-'.$mes.'-'.$dia;

        $usuario = $this->user->updateclient($email_ant, $nombre, $apellidoP, $apellidoM, $telefono, $fecha);

        session(['perfilUsuario' => compact('usuario')]);
        
        return redirect()->route('mi-cuenta');
    }

    public function EditPassword() {
        $pass_ant = $_POST['pass_ant'];
        $pass = $_POST['pass'];
        $id_cliente = $_POST['id'];

        $usuario = $this->user->editPassword($pass_ant, $pass, $id_cliente);

        return $usuario;
    }

    public function addAddress() {
        $email = $_POST['email'];
        $calle = $_POST['address'];
        $numInt = $_POST['num_int'];
        $numExt = $_POST['num_ext'];
        $colonia = $_POST['col'];
        $codigo = $_POST['zip'];
        $municipio = $_POST['province'];
        $estado = $_POST['stateProvince'];
        $alias = $_POST['alias'];
        $referencia = $_POST['referencia'];
        $pais = 'México';

        $address = $this->user->addNewAddress($email, $calle, $numInt, $numExt, $colonia, $codigo, $municipio, $estado, $pais, $alias, $referencia);

        return $address;
    }

    public function removeAddress() {
        $id_direccion = $_POST['id_direccion'];

        $address = $this->user->deleteAddress($id_direccion);

        return json_encode($address);
    }

    public function getAddressData(){
        $id_direccion = $_POST['id_direccion'];
        $origen = $_POST['origen'];
        $email = $_POST['email'];

        $address = $this->user->getAddressData($id_direccion, $origen, $email);

        return json_encode($address);
    }

    public function uploadAddress(){
        $id_direccion = $_POST['id_address'];
        $origen = $_POST['origen'];
        $email = $_POST['email'];
        
        $calle = $_POST['address'];
        $numInt = $_POST['num_int'];
        $numExt = $_POST['num_ext'];
        $colonia = $_POST['col'];
        $codigo = $_POST['zip'];
        $municipio = $_POST['province'];
        $estado = $_POST['stateProvince'];
        $alias = $_POST['alias'];
        $referencia = $_POST['referencia'];
        $pais = 'México';
        

        $direccion = $this->user->editAddress($id_direccion, $origen, $email, $calle, $numInt, $numExt, $colonia, $codigo, $municipio, $estado, $pais, $alias, $referencia);

        return json_encode($direccion);
    }

    public function MulettaCash(){
        $perfil_usuario = session()->get('perfilUsuario')['usuario'];

        $mulettaCash = $this->user->GetHistoryMulettaCash($perfil_usuario->ID_CLIENTE);

        return view('usuario.cuenta.mulettaCash', compact('perfil_usuario', 'mulettaCash'));
    }

    public function analitycsDelete($id_venta){
        $ordenes = $this->orders->GetOrderInfo($id_venta);
        $productos = $this->orders->GetOrderProducts($id_venta);

        return json_encode([
            'ID_VENTA' => $id_venta,
            'ORDEN' => $ordenes,
            'PRODUCTOS' => $productos
        ]);
    }
}
