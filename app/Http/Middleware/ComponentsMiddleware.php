<?php

namespace App\Http\Middleware;

use App\Http\Controllers\ProductsController;
use Closure;

class ComponentsMiddleware
{
    //Inyectamos la clase Products por medeio del contructor e importamos la clase
    protected $productsController;

    //Creamos el constructor de la clase el cual nos creara una instancia de la clase Products
    public function __construct(ProductsController $productsController) {
        $this->productsController = $productsController;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $this->productsController->getMenuPrimerNivel();
        $this->productsController->getMenuSubNivel();
        $this->productsController->getCountDown();
        $this->productsController->getGenero();
        $this->productsController->getTipo();
        $this->productsController->getClasificacion();
        $this->productsController->getTallas();

        return $next($request);
    }
}
