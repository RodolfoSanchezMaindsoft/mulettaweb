<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DevolucionMuletta extends Mailable
{
    use Queueable, SerializesModels;
    public $id_devolucion;
    public $productos;
    public $perfil_usuario;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id_devolucion, $productos, $perfil_usuario)
    {
        $this->id_devolucion = $id_devolucion;
        $this->productos = $productos;
        $this->perfil_usuario = $perfil_usuario;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        return $this->from('contacto@muletta.com')
            ->to($this->perfil_usuario->EMAIL)
            ->from('Tu pedido en Muletta')
            ->subject('Devolucion Generada')
            ->view('mail.devoluciones');
    }
}
