<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $i=sizeof($this->data);
        switch ($i) {
        case 3:
            $email_temp_to_send='mail.contacto_email_template';
            break;
        case 6:
            $email_temp_to_send='mail.bolsa_trabajo_email_template';
            break;
        case 13:
            $email_temp_to_send='mail.facturacion_email_template';
            break;
        }
        
    
         return $this->from('contacto@muletta.com')->subject('Nuevo correo de pagina Muletta')->view($email_temp_to_send)->with('data', $this->data);
     
      
    }
}
?>