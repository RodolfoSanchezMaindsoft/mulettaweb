<?php

namespace App\Exceptions;

use Exception;
use App\Mail\ExceptionOccured;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\Debug\Exception\FlattenException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\Debug\ExceptionHandler as SymfonyExceptionHandler;


class Handler extends ExceptionHandler
{
    /**
     * Lista de excepciones que no nos interesa se reporten
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    
    public function render($request, Exception $e)
    {               
        if ($e instanceof HttpException ) {
            //get the status code
            $status = $e->getStatusCode();
      
            return response()->view('error.500', [], 500);
        }

        $e2 = FlattenException::create($e);
        $codigo = $e2->getStatusCode();
        $facebookBot = strpos($_SERVER['HTTP_USER_AGENT'], 'facebookexternalhit');
        $googleBot = strpos($_SERVER['HTTP_USER_AGENT'], 'Googlebot');
        $twitterbot = strpos($_SERVER['HTTP_USER_AGENT'], 'Twitterbot');
        $adsbot = strpos($_SERVER['HTTP_USER_AGENT'], 'AdsBot-Google'); 
        $wordpress = strpos($_SERVER['REQUEST_URI'], 'wp-content');
        $entra = true;
        
        if ($facebookBot !== false) {
            $entra = false;
        }
        
        if ($googleBot !== false) {
            $entra = false;
        }
                
        if ($twitterbot !== false) {
            $entra = false;
        }
                
        if ($adsbot !== false) {
            $entra = false;
        }
                
        if ($wordpress !== false) {
            $entra = false;
        }

        if ($codigo >= 500 && $entra) {

            $detallesError = [
                'codigo' => $codigo,
                'url_actual' => $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"],
                'archivoError' => $e->getFile(),
                'lineaError' => $e->getLine(),
                'mensaje' => $e->getMessage(),
                'urlAnterior' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'El usuario vino de otra página',
                'origen' => $_SERVER['HTTP_USER_AGENT'],
                'params_post' => json_encode($_POST),
                'params_params' => json_encode($_GET)
            ];

            // $this->sendEmail($e, $detallesError);
        }

        return parent::render($request, $e);
                                     
    }
    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
    
    /**
     * Sends an email to the developer about the exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function sendEmail(Exception $exception, $detallesError = null)
    {
        // try {
            $e = FlattenException::create($exception);

            $handler = new SymfonyExceptionHandler();

            $html = $handler->getHtml($e);

            Mail::send(new ExceptionOccured($html, $detallesError));
        // } catch (Exception $ex) {
        //     dd($ex);
        // }
    }
    
}
