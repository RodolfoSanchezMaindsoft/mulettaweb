<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class JobAnalytics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:Analytics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Job para buscar las ventas que no están en Analytics, pasarlas a Analytics y actualizar el campo "ANALYTICS" a 1 en BD';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

     // Logica del Job
    public function handle()
    {
        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, "http://api.muletta.net/getAnalytics0");
        
        curl_setopt($ch, CURLOPT_POST, 1);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $result = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);  
        
        //echo($result);
        
        $resultado = json_decode($result);
        
        if($resultado[0]){
            
            
            $ides = [];
            
            $echoHtml = '<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68032621-1"></script>
                        <script>
                            window.dataLayer = window.dataLayer || [];
                            function gtag(){dataLayer.push(arguments);}
                            gtag("js", new Date());
                            
                            gtag("config", "UA-68032621-1");

                            gtag(function() {
                                console.log(gtag.getAll());
                            });

                            !function(f,b,e,v,n,t,s)
                            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
                                n.queue=[];t=b.createElement(e);t.async=!0;
                                t.src=v;s=b.getElementsByTagName(e)[0];
                                s.parentNode.insertBefore(t,s)}(window, document,"script",
                                "https://connect.facebook.net/en_US/fbevents.js");
                            fbq("init", "357019004873341");
                        </script>';
                        
            $echoHtml2 = '';
            
            foreach($resultado as $venta){
                
                $id_venta = $venta->ID_VENTA;
                
                $ides [] = ['ID_VENTA' => $id_venta];
                
                // ORDENES
                
                    // create curl resource
                    $ch2 = curl_init();
                
                    // set url
                    curl_setopt($ch2, CURLOPT_URL, "http://api.muletta.net/GetOrderInfo");
                    
                    curl_setopt($ch2, CURLOPT_POST, 1);
                    
                    $payload = json_encode(array('ID_VENTA' => $id_venta));
                    
                    //attach encoded JSON string to the POST fields
                    curl_setopt($ch2, CURLOPT_POSTFIELDS, $payload);
                    
                    //set the content type to application/json
                    curl_setopt($ch2, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                
                    //return the transfer as a string
                    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
                
                    // $output contains the output string
                    $ordenes = curl_exec($ch2);
                
                    // close curl resource to free up system resources
                    curl_close($ch2);  
                    
                $resultadoOrdenes = json_decode($ordenes);
                $resultadoOrdenes = $resultadoOrdenes[0];
                
                
                
                // PRODUCTOS
                    // create curl resource
                    $ch3 = curl_init();
                
                    // set url
                    curl_setopt($ch3, CURLOPT_URL, "http://api.muletta.net/GetOrderProducts");
                    
                    curl_setopt($ch3, CURLOPT_POST, 1);
                    
                    $payload2 = json_encode(array(
                        'ID_VENTA' => $id_venta,
                        'ID_ORIGEN' => '1'
                    ));
                    
                    //attach encoded JSON string to the POST fields
                    curl_setopt($ch3, CURLOPT_POSTFIELDS, $payload2);
                    
                    //set the content type to application/json
                    curl_setopt($ch3, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                
                    //return the transfer as a string
                    curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
                
                    // $output contains the output string
                    $productos = curl_exec($ch3);
                
                    // close curl resource to free up system resources
                    curl_close($ch3); 
                    
                
                $resultadoProductos = json_decode($productos);
                
                
                $track_items = '[';
                $subtotal = 0;

                foreach($resultadoProductos as $key => $product) {
                    $track_items = $track_items.'{
                        id: "'.$product->ID_MODELO.'",
                        name: "'.$product->DESCRIPCION.'",
                        variant: "'.trim($product->TALLA).'",
                        quantity: '.$product->CANTIDAD_VENDIDA.',
                        price: '.$product->PRECIO.'';

                    if (count($resultadoProductos) == ($key + 1)) {
                        $track_items = $track_items.'}';
                    }else{
                        $track_items = $track_items.'},';
                    }
                }

                $track_items = $track_items.']';
                
                //echo $track_items;

                    $echoHtml2 = $echoHtml2.'<script type="text/javascript">
                        gtag("event", "purchase", {
                            "transaction_id": '.$id_venta.',
                            "value": '.$resultadoOrdenes->TOTAL.',
                            "currency": "MXN",
                            "checkout_option": "'.$resultadoOrdenes->METODO_PAGO.'",
                            "shipping": 0,
                            "items": '.$track_items.'
                        });
                        
                        fbq("track", "Purchase", {
                            value: '.$resultadoOrdenes->TOTAL.',
                            currency: "MXN",
                            contents: "0",
                            content_type: "product"
                        });
                    </script>';
                
            }
            
            
            $ides = json_encode($ides);
            
            //echo $ides;
            
            /*$postdata = http_build_query(
                array(
                    'VENTAS' => $ides
                )
            );
            $opts2 = array('http' =>
                array(
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $postdata
                )
            );
            $context2 = stream_context_create($opts2);
            $result2 = file_get_contents('http://api.muletta.net/updateAnalyticsTo1', false, $context2);*/
            
            // create curl resource
                $ch4 = curl_init();
            
                // set url
                curl_setopt($ch4, CURLOPT_URL, "http://api.muletta.net/updateAnalyticsTo1");
                
                curl_setopt($ch4, CURLOPT_POST, 1);
                
                $payload3 = json_encode(array(
                    'VENTAS' => $ides
                ));
                
                //attach encoded JSON string to the POST fields
                curl_setopt($ch4, CURLOPT_POSTFIELDS, $payload3);
                
                //set the content type to application/json
                curl_setopt($ch4, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            
                //return the transfer as a string
                curl_setopt($ch4, CURLOPT_RETURNTRANSFER, 1);
            
                // $output contains the output string
                $productos = curl_exec($ch4);
            
                // close curl resource to free up system resources
                curl_close($ch4); 
            
            echo ($echoHtml.$echoHtml2);
            
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH");
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, token, Content-Type, cache-control");

            define( "WEBMASTER_EMAIL", 'omar.acevedo@maindsoft.net' );

            define( "USER_EMAIL", trim( 'oa624488@gmail.com' ) );

            $full_name = stripslashes( 'Omar' );
            $email = trim( 'oa624488@gmail.com' );
            $subject = stripslashes( "Alerta de Job" );
            
            $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
            $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            $cabeceras .= "From: " . $full_name . " <" . $email . ">\r\n";
            $cabeceras .= "Reply-To: " . $email . "\r\n";
            $cabeceras .= "X-Mailer: PHP/" . phpversion();
            
            
            $message  = "<html><body>";
        
            $message .= "<table width='100%' bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0'>";
        
            $message .= "<tr><td>";
        
            $message .= "<table align='center' width='100%' border='0' cellpadding='0' cellspacing='0' style='max-width:650px; background-color:#fff; font-family:Verdana, Geneva, sans-serif;'>";
            
            $message .= "<thead>
            <tr height='80'>
            <th colspan='4' style='background-color:#2175A2; border-bottom:solid 1px #bdbdbd; font-family:Verdana, Geneva, sans-serif; color:#fff; font-size:34px;' >Uvicapp</th>
            </tr>
            </thead>";
            
            $message .= "<tbody>
            <tr>
            <td colspan='4' style='padding:15px;'>
                <p style='font-size:20px;'>Hola Muletta,</p>
                <hr />
                <p style='font-size:25px;'>BBBB</p>
                <a href='https://uvicapp.com/' style='color:#fff; text-decoration:none;><img src='http://uvicapp.com/wp-content/uploads/2021/07/LOGO-PNG.png' alt='Uvicapp' style='height:auto; width:100%; max-width:100%;' /></a>
                <p style='font-size:15px; font-family:Verdana, Geneva, sans-serif;'>
                <ul>
                    <li><b>Nombre:</b> ".$full_name.". </li>
                    <li><b>Correo:</b> ".$email.". </li>
                    <li>".$ides."</li>";
                    
                $message .= "</ul>
                </p>
            </td>
            </tr>
            
            </tbody>";
            
            $message .= "</table>";
        
            $message .= "</td></tr>";
            $message .= "</table>";
        
            $message .= "</body></html>";
        
            // HTML email ends here
            

            $mail = @mail( WEBMASTER_EMAIL, $subject, $message, $cabeceras );
            //echo $result2;
        }
        else{
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH");
            header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, token, Content-Type, cache-control");

            define( "WEBMASTER_EMAIL", 'omar.acevedo@maindsoft.net' );

            define( "USER_EMAIL", trim( 'oa624488@gmail.com' ) );

            $full_name = stripslashes( 'Omar' );
            $email = trim( 'oa624488@gmail.com' );
            $subject = stripslashes( "Alerta de Job" );
            
            $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
            $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            $cabeceras .= "From: " . $full_name . " <" . $email . ">\r\n";
            $cabeceras .= "Reply-To: " . $email . "\r\n";
            $cabeceras .= "X-Mailer: PHP/" . phpversion();
            
            
            $message  = "<html><body>";
        
            $message .= "<table width='100%' bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0'>";
        
            $message .= "<tr><td>";
        
            $message .= "<table align='center' width='100%' border='0' cellpadding='0' cellspacing='0' style='max-width:650px; background-color:#fff; font-family:Verdana, Geneva, sans-serif;'>";
            
            $message .= "<thead>
            <tr height='80'>
            <th colspan='4' style='background-color:#2175A2; border-bottom:solid 1px #bdbdbd; font-family:Verdana, Geneva, sans-serif; color:#fff; font-size:34px;' >Uvicapp</th>
            </tr>
            </thead>";
            
            $message .= "<tbody>
            <tr>
            <td colspan='4' style='padding:15px;'>
                <p style='font-size:20px;'>Hola Muletta,</p>
                <hr />
                <p style='font-size:25px;'>CCCC</p>
                <a href='https://uvicapp.com/' style='color:#fff; text-decoration:none;><img src='http://uvicapp.com/wp-content/uploads/2021/07/LOGO-PNG.png' alt='Uvicapp' style='height:auto; width:100%; max-width:100%;' /></a>
                <p style='font-size:15px; font-family:Verdana, Geneva, sans-serif;'>
                <ul>
                    <li><b>Nombre:</b> ".$full_name.". </li>
                    <li><b>Correo:</b> ".$email.". </li>";
                    
                $message .= "</ul>
                </p>
            </td>
            </tr>
            
            </tbody>";
            
            $message .= "</table>";
        
            $message .= "</td></tr>";
            $message .= "</table>";
        
            $message .= "</body></html>";
        
            // HTML email ends here
            

            $mail = @mail( WEBMASTER_EMAIL, $subject, $message, $cabeceras );
        }
    }
}
