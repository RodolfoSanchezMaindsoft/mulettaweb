<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 8/15/2019
 * Time: 5:03 PM
 */

namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class Shopping
{

    //Creamos un constructor de la clase cliente
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function verifySales($id_cliente, $email, $subtotal, $importe, $envio)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'verifySales',
            ["json" => ['ID_CLIENTE' => "$id_cliente",
                'EMAIL' => "$email",
                'SUBTOTAL' => "$subtotal",
                'IMPORTE' => "$importe",
                'ENVIO' => "$envio",
                'ORIGEN' => '1'
            ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $compra = json_decode($response->getBody()->getContents());

        return $compra[0];
    }

    public function VerifyExist($productos_a_comprar)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'VerifyExist',
            ["json" => ['PRODUCTOS' => $productos_a_comprar,
        ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $compra = json_decode($response->getBody()->getContents());
        
        return $compra;
    }

    public function updateFinalSale($id_cliente,$alias,$email,$calle,$numero_interior,$numero_exterior,$colonia,$codigo_postal,$municipio,$estado,$pais,$metodo_pago,$descripcion,$nueva_direccion, $id_desc, $monto_desc, $regalo, $id_pago, $referencia, $pago_coins)
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'updateFinalSale',
            ["json" => ['ID_CLIENTE' => "$id_cliente",
                'EMAIL' => "$email",
                'CALLE' => "$calle",
                'NUM_INT' => "$numero_interior",
                'NUM_EXT' => "$numero_exterior",
                'COLONIA' => "$colonia",
                'CODIGO' => "$codigo_postal",
                'MUNICIPIO' => "$municipio",
                'ESTADO' => "$estado",
                'PAIS' => "$pais",
                'METODO_PAGO' => "$metodo_pago",
                'DESCRPCION_PEDIDO' => $descripcion,
                'NUEVA_DIRECCION' => "$nueva_direccion",
                'ID_DESC' => "$id_desc",
                'MONTO_DESC' => "$monto_desc",
                'REGALO' => "$regalo",
                'ID_PAGO' => "$id_pago",
                'ALIAS' => "$alias",
                'REFERENCIA' => "$referencia",
                'PAGO_COINS' => "$pago_coins",
                'ORIGEN' => '1'
            ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $compra = json_decode($response->getBody()->getContents());


        return $compra;
    }

    public function insertProductSale($productos_a_comprar)
    {

        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'insertProductSale',
            ["json" => ['PRODUCTOS' => $productos_a_comprar,
        ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $compra = json_decode($response->getBody()->getContents());

        return $compra;
    }

    public function getDesc($promo)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'getDescuentos',
            ["json" => ['PROMOCION' => "$promo", "ORIGEN" => "1"
        ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $compra = json_decode($response->getBody()->getContents());

        return isset($compra[0]) ? $compra[0] : $compra;
    }

    public function updateStatusToProcessing($idCliente)
     {
        $response = $this->client->request('POST', 'updateStatusToProcessing',
        ["json" => ['ID_CLIENTE' => $idCliente,
            'ORIGEN' => '1']]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $ordenes = json_decode($response->getBody()->getContents());

        return  $ordenes;
     }

    public function updateSalesInventory($idCliente)
     {
        $response = $this->client->request('POST', 'updateSalesInventory',
        ["json" => ['ID_CLIENTE' => $idCliente,
            'ORIGEN' => '1']]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $ordenes = json_decode($response->getBody()->getContents());

        return  $ordenes;
     }

    public function verifyProductExistence($id_cliente, $NO_PARTE, $CANT)
     {
        $response = $this->client->request('POST', 'verifyProductExistence',
        ["json" => ['NO_PARTE' => $NO_PARTE,
            'CLIENTE' => $id_cliente,
            'CANTIDAD' => $CANT,
            'ORIGEN' => '1']]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $ordenes = json_decode($response->getBody()->getContents());

        return  $ordenes;
     }

    public function deleteProductExistence($id_cliente, $NO_PARTE)
    {
        $response = $this->client->request('POST', 'deleteProductExistence',
        ["json" => ['NO_PARTE' => $NO_PARTE,
            'CLIENTE' => $id_cliente,
            'ORIGEN' => '1']]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $ordenes = json_decode($response->getBody()->getContents());

        return  $ordenes;
    }

    public function getCostSend(){
        $response = $this->client->request('POST', 'getCostSend',
        ["json" => ['ORIGEN' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $costos = json_decode($response->getBody()->getContents());

        return  $costos;
    }

    public function restarDineroUsuario($id_cliente, $no_ticket, $coins){
        $response = $this->client->request('POST', 'restarDineroUsuario',
        ["json" =>  [
                        'ID_CLIENTE' => $id_cliente,
                        'NO_TICKET' => $no_ticket,
                        'COINS' => $coins
                    ]
        ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $costos = json_decode($response->getBody()->getContents());

        return  $costos;
    }

    public function ErrorCodeMessage($error){
        $message = '';
        
        switch ($error['error_code']) {
            case 2004:
                $message = 'El número de tarjeta es invalido.';
                break;
            
            case 2005:
                $message = 'La fecha de expiración de la tarjeta es anterior a la fecha actual.';
                break;
            
            case 2006:
                $message = 'El código de seguridad de la tarjeta (CVV2) no fue proporcionado.';
                break;
            
            case 2007:
                $message = 'El número de tarjeta es de prueba, solamente puede usarse en Sandbox.';
                break;
            
            case 2008:
                $message = 'La tarjeta no es valida para pago con puntos.';
                break;
            
            case 2009:
                $message = 'El código de seguridad de la tarjeta (CVV2) es inválido.';
                break;
            
            
            case 2010:
                $message = 'Autenticación 3D Secure fallida.';
                break;
            
            
            case 2011:
                $message = 'Tipo de tarjeta no soportada.';
                break;
            
            case 3001:
                $message = 'La tarjeta fue declinada por el banco.';
                break;
            
            case 3002:
                $message = 'La tarjeta ha expirado.';
                break;
            
            case 3003:
                $message = 'La tarjeta no tiene fondos suficientes.';
                break;
            
            case 3004:
                $message = 'La tarjeta ha sido identificada como una tarjeta robada.';
                break;
            
            case 3005:
                $message = 'La tarjeta ha sido rechazada por el sistema antifraude.';
                break;
            
            case 3006:
                $message = 'La operación no esta permitida para este cliente o esta transacción.';
                break;
            
            case 3009:
                $message = 'La tarjeta fue reportada como perdida.';
                break;
            
            case 3010:
                $message = 'El banco ha restringido la tarjeta.';
                break;
            
            case 3011:
                $message = 'El banco ha solicitado que la tarjeta sea retenida. Contacte al banco.';
                break;
            
            case 3012:
                $message = 'Se requiere solicitar al banco autorización para realizar este pago.';
                break;
            
            case 3201:
                $message = 'Comercio no autorizado para procesar pago a meses sin intereses.';
                break;
            
            case 3203:
                $message = 'Promoción no valida para este tipo de tarjetas.';
                break;
            
            case 3204:
                $message = 'El monto de la transacción es menor al mínimo permitido para la promoción.';
                break;
            
            case 3205:
                $message = 'Promoción no permitida.';
                break;
            
            default:
                $message = 'Error al procesar la compra';
                break;
        }

        return $message;
    }

    //Otiene la conexion para el uso de los parametros del Banco
    public function getConexion()
    {
        $response = $this->client->request('POST', 'getParamsIn');
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $conexion = json_decode($response->getBody()->getContents());
        return $conexion;
    }
    //Genera el registro de los pagos
    public function inserstWebpay($a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l,$m,$n,$o)
    {
        $response = $this->client->request('POST', 'setOrdenWebPay',
            ["json" =>  [
                'ORDEN_COMPRA_WEBPAY' => $a,
                'REFERENCE' => $b,
                'RESPONSE' => $c,
                'FOLIOCPAGOS' => $d,
                'AUTH' => $e,
                'TIME' => $f,
                'DATE' => $g,
                'NB_MERCHANT' => $h,
                'CC_TYPE' => $i,
                'TP_OPERATION' => $j,
                'AMOUNT' => $k,
                'ID_URL' => $l,
                'EMAIL' => $m,
                'CD_RESPONSE' => $n,
                'CC_MASK' => $o,
            ]
            ]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $conexion = json_decode($response->getBody()->getContents());
        return $conexion;
    }
    
}