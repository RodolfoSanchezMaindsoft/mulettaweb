<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 8/2/2019
 * Time: 5:39 PM
 */

namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;

class Users
{

    //Creamos un constructor de la clase cliente
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function insertClientNormal($nombre, $apellidoP, $apellidoM, $telefono, $email, $fecha_nacimiento, $contrasena)
    {

        $response = $this->client->request('POST', 'register/normal',
            ["json" =>
                ['NOMBRE' => "$nombre",
                    'TELEFONO' => "$telefono",
                    'EMAIL' => "$email",
                    'FECHA_NACIMIENTO' => "$fecha_nacimiento",
                    'ID_ORIGEN' => '1',
                    'ID_TIPO_REGISTRO' => '1',
                    'CONTRASENA' => "$contrasena",
                    'APELLIDO_PATERNO' => "$apellidoP",
                    'APELLIDO_MATERNO' => "$apellidoM",
                    'TOKEN_PUSH' => '']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());
        
        return $usuario;
    }

    public function insertClientSale($nombre, $telefono, $email)
    {

        $response = $this->client->request('POST', 'register/UserVenta',
            ["json" =>
                ['NOMBRE' => "$nombre",
                    'TELEFONO' => "$telefono",
                    'EMAIL' => "$email",
                    'ID_ORIGEN' => '1',
                    'ID_TIPO_REGISTRO' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());
        
        return $usuario;
    }

    public function insertClientRs($nombre, $email, $id_plataforma, $id_tipo_registro, $url_imagen)
    {
        $response = $this->client->request('POST', 'register/rs',
            ["json" =>
                ['NOMBRE' => "$nombre",
                    'EMAIL' => "$email",
                    'ID_PLATAFORMA' => "$id_plataforma",
                    'ID_ORIGEN' => '1',
                    'ID_TIPO_REGISTRO' => "$id_tipo_registro",
                    'IMAGEN' => "$url_imagen",
                    'TOKEN_PUSH' => '']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());

        return $usuario[0];
    }

    public function getClientAccessNormal($email, $contrasena)
    {

        $response = $this->client->request('POST', '/login/normal',
            ["json" =>
                ['EMAIL' => "$email",
                    'CONTRASENA' => "$contrasena",
                    'ID_ORIGEN' => '1']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());

        //$usuario = $usuario[0];

        return isset($usuario[0]) ? $usuario[0] : $usuario;

    }

    public function getClientAccessRs($nombre,$email, $id_tipo_registro, $id_plataforma, $access_token, $url_imagen)
    {
        $response = $this->client->request('POST', '/login/rs',
            ["json" =>
                [
                    'NOMBRE' => "$nombre",
                    'EMAIL' => "$email",
                    'ID_ORIGEN' => '1',
                    'ID_TIPO_REGISTRO' => "$id_tipo_registro",
                    'ID_PLATAFORMA' => "$id_plataforma",
                    'ACCESS_TOKEN' => "$access_token",
                    'IMAGEN' => "$url_imagen"
                ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());

        $usuario = $usuario[0];

        return $usuario;
    }

    public function updateclient($email_ant, $nombre, $apellidoP, $apellidoM, $tel, $fecha_nacimiento)
    {
        $response = $this->client->request('POST', '/UpdateClientInfo',
            ["json" =>
                [
                    'EMAIL_ANT' => "$email_ant",
                    'NOMBRE' => "$nombre",
                    'APELLIDO_PATERNO' => "$apellidoP",
                    'APELLIDO_MATERNO' => "$apellidoM",
                    'TELEFONO' => "$tel",
                    'FECHA_NACIMIENTO' => "$fecha_nacimiento",
                    'ESTADO' => null,
                    'MUNICIPIO' => null,
                    'CALLE' => null,
                    'NUM_INT' => null,
                    'NUM_EXT' => null,
                    'COLONIA' => null,
                    'CODIGO' => null
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());
        
        $usuario = $usuario[0];
        
        return $usuario;
    }

    public function editPassword($pass_ant, $pass, $id_cliente)
    {
        $response = $this->client->request('POST', '/changePassword',
            ["json" =>
                [
                    'ID_CLIENTE' => "$id_cliente",
                    'PASS_ANT' => "$pass_ant",
                    'PASSWORD' => "$pass"
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());

        // $usuario = $usuario[0];

        return $usuario;
    }

    public function getVirtualMoney($id_cliente)
    {
        $response = $this->client->request('POST', '/getVirtualMoney',
            ["json" =>
                [
                    'ID_CLIENTE' => $id_cliente,
                    'origen' => "1"
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());
        
        return isset($usuario[0]) ? $usuario[0] : $usuario;
    }

    public function GetClientAddresses($email)
    {
        $response = $this->client->request('POST', '/GetClientAddresses',
            ["json" =>
                [
                    'EMAIL' => "$email",
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $usuario = json_decode($response->getBody()->getContents());

        return $usuario;
    }

    public function addNewAddress($email, $calle, $numInt, $numExt, $colonia, $codigo, $municipio, $estado, $pais, $alias, $referencia)
    {
        $response = $this->client->request('POST', '/InsertAddress',
            ["json" =>
                [
                    'EMAIL' => $email,
                    'CALLE' => $calle,
                    'NUM_INT' => $numInt,
                    'NUM_EXT' => $numExt,
                    'COLONIA' => $colonia,
                    'CODIGO' => $codigo,
                    'MUNICIPIO' => $municipio,
                    'ESTADO' => $estado,
                    'PAIS' => $pais,
                    'ALIAS' => $alias,
                    'REFERENCIA' => $referencia
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $address = json_decode($response->getBody()->getContents());

        return $address;
    }

    public function editAddress($id_direccion, $origen, $email, $calle, $numInt, $numExt, $colonia, $codigo, $municipio, $estado, $pais, $alias, $referencia)
    {
        $response = $this->client->request('POST', '/UpdateAddressData',
            ["json" =>
                [
                    'ID_DIRECCION' => $id_direccion,
                    'ORIGEN' => $origen,
                    'EMAIL' => $email,
                    'CALLE' => $calle,
                    'NUM_INT' => $numInt,
                    'NUM_EXT' => $numExt,
                    'COLONIA' => $colonia,
                    'CODIGO' => $codigo,
                    'MUNICIPIO' => $municipio,
                    'ESTADO' => $estado,
                    'ALIAS' => $alias,
                    'PAIS' => $pais,
                    'REFERENCIA' => $referencia
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $address = json_decode($response->getBody()->getContents());

        return $address;
    }

    public function deleteAddress($id_direccion)
    {
        $response = $this->client->request('POST', '/DeleteAddressData',
            ["json" =>
                [
                    'ID_DIRECCION' => $id_direccion,
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $address = json_decode($response->getBody()->getContents());

        return $address;
    }

    public function getAddressData($id_direccion, $origen, $email)
    {
        $response = $this->client->request('POST', '/GetAddressData',
            ["json" =>
                [
                    'ID_DIRECCION' => $id_direccion,
                    'ORIGEN' => $origen,
                    'EMAIL' => $email,
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $address = json_decode($response->getBody()->getContents());

        $address = $address[0];

        return $address;
    }

    public function resetPassword($email)
    {
        $response = $this->client->request('POST', '/resetPassword',
            ["json" =>
                [
                    'EMAIL' => $email
                ]
            ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $address = json_decode($response->getBody()->getContents());

        return isset($address[0]) ? $address[0] : $address;
    }
    
    public function getUser($id_cliente)
    {
        $response = $this->client->request('POST', '/getUser',
            ["json" =>
                [
                    'ID_CLIENTE' => $id_cliente
                ]
        ]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $users = json_decode($response->getBody()->getContents());

        return isset($users[0]) ? $users[0] : $users;
    }
    
    public function getUsersMuletta()
    {
        $response = $this->client->request('POST', '/getPersonalMuletta');

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $users = json_decode($response->getBody()->getContents());

        return $users;
    }
    
    public function GetEstates()
    {
        $response = $this->client->request('POST', '/getEstados');

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $estate = json_decode($response->getBody()->getContents());

        return $estate;
    }

    public function getMunicipios($id_estado){
        $response = $this->client->request('POST', '/obtenerMunicipiosBien', 
            [
                "json" =>
                    [
                        'ID_ESTADO' => $id_estado
                    ]
            ]
        );

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $municipio = json_decode($response->getBody()->getContents());

        return $municipio;
    }
    
    public function GetHistoryMulettaCash($id_cliente)
    {
        $response = $this->client->request('POST', '/getHistoryMulettaCash',
            [
                "json" =>
                    [
                        'ID_CLIENTE' => $id_cliente
                    ]
            ]
        );

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $historial = json_decode($response->getBody()->getContents());

        return $historial;
    }

    public function updateOpenPay($id_cliente, $id_openpay)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'updateOpenPay',
            ["json" => [
                'ID_CLIENTE' => $id_cliente,
                'ID_OPENPAY' => $id_openpay
            ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $Openpay = json_decode($response->getBody()->getContents());

        return $Openpay;
    }

    public function updateStripe($id_cliente, $id_stripe)
    {
        //Se manda a llamar la petición por medio de 2 parámetros ("El método que en este caso es POST", "Url de lo que tomemos en este caso será GetAllProducts y la URL completa será https://muletta-api.herokuapp.com/GetAllProducts")
        $response = $this->client->request('POST', 'updateStripe',
            ["json" => [
                'ID_CLIENTE' => $id_cliente,
                'ID_OPENPAY' => $id_stripe
            ]]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $Stripe = json_decode($response->getBody()->getContents());

        return $Stripe;
    }

}