<?php
namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class Orders{
     //Creamos un constructor de la clase cliente
     protected $client;

     public function __construct(Client $client)
     {
         $this->client = $client;
     }

     public function GetClientOrders($idCliente, $status)
     {
        $response = $this->client->request('POST', 'GetClientOrders',
        ["json" => ['ID_CLIENTE' => $idCliente,
            'STATUS' => $status,
            'ID_ORIGEN' => '1']]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $ordenes = json_decode($response->getBody()->getContents());

        return  $ordenes;
     }

     public function obtenerDevolucionesCliente($idCliente)
     {
        $response = $this->client->request('POST', 'obtenerDevolucionesCliente',
        ["json" => ['ID_CLIENTE' => $idCliente,
            'STATUS' => 0,
            'ID_ORIGEN' => '1']]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $ordenes = json_decode($response->getBody()->getContents());

        return  $ordenes;
     }

     public function GetOrderProducts($idVenta)
     {
        $response = $this->client->request('POST', 'GetOrderProducts',
        ["json" => ['ID_VENTA' => $idVenta,
            'ID_ORIGEN' => '1']]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $ordenes_productos = json_decode($response->getBody()->getContents());

        return  $ordenes_productos;
     }

     public function GetOrderInfo($idVenta)
     {
        $response = $this->client->request('POST', 'GetOrderInfo',
        ["json" => ['ID_VENTA' => $idVenta]]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $ordenes_single = json_decode($response->getBody()->getContents());

        $ordenes_single = $ordenes_single[0];

        return  $ordenes_single;
     }

     public function UpdatePay($id_Venta, $id_pago, $id_metodo_pago){
        $response = $this->client->request('POST', 'updatePay',
        ["json" => ['ID_VENTA' => $id_Venta, 'ID_PAGO' => $id_pago, 'ID_METODO_PAGO' => $id_metodo_pago]]);

        $pago = json_decode($response->getBody()->getContents());

        return  $pago;
     }

     //devoluciones
     public function InsertReturn($id_user, $id_venta, $razones, $total, $id_motivo)
     {
        $response = $this->client->request('POST', 'insertarDevolucion',
        ["json" => ['ID_USER' => $id_user,
            'ID_VENTA' => $id_venta,
            'ID_MOTIVO' => $id_motivo,
            'TOTAL' => $total,
            'RAZONES' => $razones]]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $id_devolucion = json_decode($response->getBody()->getContents());

        return  isset($id_devolucion[0]) ? $id_devolucion[0] : $id_devolucion;
     }

     public function InsertReturnProduct($productos)
     {
        $response = $this->client->request('POST', 'insertarProductoDevolucion',
        ["json" => ['PRODUCTOS' => $productos]]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $dev_productos = json_decode($response->getBody()->getContents());

        return  $dev_productos;
     }

     public function getMotivos()
     {
        $response = $this->client->request('POST', 'obtenerMotivos');
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $motivos = json_decode($response->getBody()->getContents());

        return  $motivos;
     }

     public function getDevolucion($id_devolucion)
     {
        $response = $this->client->request('POST', 'obtenerDevolucion',
        ["json" => ['ID_DEVOLUCION' => $id_devolucion]]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $devolucion = json_decode($response->getBody()->getContents());

        return  $devolucion[0];
     }

     public function obtenerProductosDevolucion($id_devolucion)
     {
        $response = $this->client->request('POST', 'obtenerProductosDevolucion',
        ["json" => ['ID_DEVOLUCION' => $id_devolucion]]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $devolucion = json_decode($response->getBody()->getContents());

        return  $devolucion;
     }

     public function obtenerProductosDevolucionPDF($id_devolucion, $no_parte)
     {
        $response = $this->client->request('POST', 'obtenerProductosDevolucionPdf',
        ["json" => ['ID_DEVOLUCION' => $id_devolucion, 'NO_PARTE' => $no_parte]]);
        
        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $devolucion = json_decode($response->getBody()->getContents());

        return  $devolucion[0];
     }
}

?>