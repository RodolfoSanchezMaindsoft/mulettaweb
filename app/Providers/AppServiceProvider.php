<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('GuzzleHttp\Client', function (){

            return new Client([
                // 'base_uri' => 'http://localhost:82/'
                'base_uri' => 'https://creatmos.asaiint.com/'
                // 'base_uri' => 'https://prueba.asaiint.com/'
            ]);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
