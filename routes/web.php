<?php

Route::group(['middleware' => 'components'], function () {

    Route::get('/', 'ProductsController@index')->name('index');

    Route::post('/user/deleteAnalitycs/{ID_VENTA}', 'UsersController@analitycsDelete');

    //IndexPadre
    //Route::get('/{NOMBRE}', 'ProductsController@indexPadre')->name('indexPadre');
    Route::get('/{URL_TIPO_PRODUCTO}', 'ProductsController@Banners' )->name('indexPadre');
    

    //Footer
    Route::get('ayuda/faq/faq_menu', 'ComprasController@faq_menu' )->name('faq_menu');
    Route::get('ayuda/contacto', 'ComprasController@contacto' )->name('contacto');
    Route::get('ayuda/facturacion', 'ComprasController@facturacion' )->name('facturacion');
    Route::get('ayuda/bolsa_trabajo', 'ComprasController@bolsa_trabajo' )->name('bolsa_trabajo');
    Route::get('ayuda/pagos_y_metodos_envio', 'ComprasController@politicas_envio' )->name('politicas_envio');
    Route::get('ayuda/politicas_envio', 'ComprasController@politicas_envio_politicas' )->name('politicas_envio_politicas');
    Route::get('ayuda/cambios_devoluciones', 'ComprasController@politicas_cambios_devoluciones' )->name('politicas_cambios_devoluciones');
    Route::get('ayuda/politicas_cambios_devoluciones', 'ComprasController@politicas_cambios_devoluciones_politicas' )->name('politicas_cambios_devoluciones_politicas');
    Route::get('ayuda/guia_tallas', 'ComprasController@guia_tallas' )->name('guia_tallas');
    Route::get('ayuda/catalogo_2018', 'ComprasController@catalogo_2018' )->name('catalogo_2018');
    Route::get('ayuda/terminos_condiciones', 'ComprasController@terminos_condiciones' )->name('terminos_condiciones');
    Route::get('ayuda/aviso_privacidad', 'ComprasController@aviso_privacidad' )->name('aviso_privacidad');

    //Categorias
    Route::get('categoria/{ID_TIPO_PRODUCTO}/{URL_TIPO_PRODUCTO}', 'ProductsController@getProductsCategories' )->name('productos_por_categoria');
    Route::get('/productos/infinite_products/{id_categoria}/{page}', 'ProductsController@infiniteProducts')->name('InfiniteProducts');
    
    //faq cuenta
    Route::get('ayuda/faq/faq_cuenta', 'ComprasController@faq_cuenta' )->name('faq_cuenta');

    //faq pedidos
     Route::get('ayuda/faq/faq_pedidos', 'ComprasController@faq_pedidos' )->name('faq_pedidos');

    //faq proceso_envio
     Route::get('ayuda/faq/faq_proceso_envio', 'ComprasController@faq_proceso_envio' )->name('faq_proceso_envio');
    
    //faq metodo_pago
     Route::get('ayuda/faq/faq_metodo_pago', 'ComprasController@faq_metodo_pago' )->name('faq_metodo_pago');

    //faq producto
     Route::get('ayuda/faq/faq_producto', 'ComprasController@faq_producto' )->name('faq_producto');

    //faq distribuidores
    Route::get('ayuda/faq/faq_distribuidores', 'ComprasController@faq_distribuidores' )->name('faq_distribuidores');
    
    //faq cambios
    Route::get('ayuda/faq/faq_cambios', 'ComprasController@faq_cambios' )->name('faq_cambios');

    //faq info_gen
    Route::get('ayuda/faq/faq_info_gen', 'ComprasController@faq_info_gen' )->name('faq_info_gen');

    //Producto
    Route::get('producto/{DESCRIPCION_MODELO}/{ID_MODELO}','ProductsController@getProduct')->name('producto_individual');
    
    Route::get('productos/search', 'ProductsController@searchProducts' )->name('buscar_productos');
    Route::post('productos/search', 'ProductsController@searchProducts' )->name('buscar_productos');

    //Route::get('banners/{ID_TIPO_PRODUCTO}/{URL_TIPO_PRODUCTO}', 'ProductsController@Banners' )->name('banners_productos');

    //Restablecer contraseña
    Route::get('/usuario/restablecer','Auth\ForgotPasswordController@showForgotPswdForm');

    // Login
    Route::get('/usuario/login','Auth\LoginController@showLoginForm')->middleware('guest')->name('loginForm');

    //Registro
    Route::get('/usuario/registro','Auth\RegisterController@showRegisterForm')->name('register');

    //Mostrar pedidos
    Route::get('/compras/detailsVenta/{ID_VENTA}', 'ComprasController@detailsVenta')->name('detailsVenta');
    
    //Cuenta
    Route::group(['middleware' => 'user'], function () {
        

        //Devoluciones
        Route::post('/compras/proceso-devolucion/insertReturn', 'ComprasController@insertRetorno')->name('insertReturn');
        Route::get('/compras/proceso-devolucion/{ID_VENTA}' , 'ComprasController@detailsDevolucion')->name('proceso-devolucion');
        Route::get('/usuario/cuenta/devoluciones', 'UsersController@showReturns')->name('returns');
        Route::get('/usuario/cuenta/muletta-cash', 'UsersController@MulettaCash')->name('MulettaCash');

        //Mostrar lista de pedidos
        Route::get('/usuario/cuenta/pedidos', 'UsersController@showMyAccount')->name('editar-cuenta');

        //vista formulario para editar datos de cuenta
        Route::get('/usuario/cuenta/mi-cuenta', 'UsersController@showEditAccount')->name('mi-cuenta');

        //Crud de direcciones
        Route::get('/usuario/cuenta/editar-direcciones', 'UsersController@showEditAdress')->name('editar-direcciones');
        Route::post('/usuario/cuenta/agregar-direccion', 'UsersController@addAddress')->name('nueva-direccion');
        Route::post('/usuario/cuenta/eliminar-direccion', 'UsersController@removeAddress')->name('eliminar-direccion');
        Route::post('/usuario/cuenta/obtener-direccion', 'UsersController@getAddressData')->name('obtener-direccion');
        Route::post('/usuario/cuenta/upload-direccion', 'UsersController@uploadAddress')->name('upload-direccion');
        
        //Actualizar contraseña
        Route::post('/usuario/contrasenia', 'UsersController@EditPassword')->name('editar-contrasenia');

    });

    //Compras
    Route::get('/compras/checkout', 'ComprasController@checkout')->name('compras');
    Route::get('/compras/shipping', 'ComprasController@checkoutUser')->name('carrito-user');
    Route::get('/pdf/generar/{ID_VENTA}','PdfController@getGenerar')->name('GenerarPdf');
    Route::get('/compras/muletta-pago/{id_venta}', 'ComprasController@MulettaPago')->name('mulettaPago');

    Route::get('/compras/PagoTarjeta/{ID_VENTA}/{ID_CLIENT?}', 'ComprasController@payTarjeta')->name('OpenPayRegistro');
    Route::get('/compras/transferenciaPago/{ID_VENTA}/{ID_CLIENT?}', 'ComprasController@transferenciaPago')->name('OpenPayBanco');
    Route::get('/compras/paynetPago/{ID_VENTA}/{ID_CLIENT?}', 'ComprasController@paynetPago')->name('OpenPayPaynet');


    Route::get('/compras/finalizarOpenpaySecure/{id_venta}/{id_cliente?}', 'ComprasController@finalizarOpenpaySecure')->name('finalizarOpenpaySecure');

});

//Ruta para buscar municipios
Route::post('/municipios', 'UsersController@obtenerMunicipios')->name('municipios');

// Ruta para enviar notificacion por correo
Route::post('/productos/addnotify', 'ProductsController@addNotify')->name('notify');

// Ruta que contiene la función para añadir al carrito
Route::post('/productos/addcar', 'ProductsController@addcar')->name('carrito');

// Ruta para obtener los porducto con variaciones para el precio de venta
Route::post('/producto/getProductsWithVariants','ProductsController@getProductsWithVariants')->name('producto_variantes');

// Ruta para obtener los porducto con variaciones para el nombre
Route::post('/producto/getProductsWithVariantsName','ProductsController@getProductsWithVariantsName')->name('producto_variantes_nombre');

// Ruta que contiene la función del registro de usuarios
Route::post('/usuario/registro','Auth\RegisterController@register')->name('registro');
Route::post('/usuario/editar','UsersController@updateAccount')->name('updateAccount');

// Ruta que contiene la función de restablecer contraseña
Route::post('/usuario/restablecer','Auth\ForgotPasswordController@showForgotPswdForm')->name('restablecer');

//  Ruta que contiene la función ingresar sesión usuarios por medio de google
Route::get('login/google', 'Auth\LoginController@redirectToProvider')->name('login-google');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

//  Ruta que contiene la función ingresar sesión usuarios por medio de facebook
Route::get('login/facebook', 'Auth\LoginController@redirectToProviderFacebook')->name('login-facebook');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallbackFacebook');

// Ruta que contiene la función del ingreso de usuarios
Route::post('/usuario/login','Auth\LoginController@login')->name('login');
Route::post('/usuario/login/{id_compra}','Auth\LoginController@login')->name('verificar_compra');
Route::post('/usuario/registro/{id_compra}','Auth\RegisterController@register')->name('reg_verificar_compra');

// Ruta que contiene la función del cierre de sesión de usuarios
Route::get('/usuario/logout','Auth\LoginController@logout')->name('logout');

Route::post('/compras/changedelete', 'ComprasController@changedelete')->name('deleteProd');

// Compras
Route::post('/compras/changeup', 'ComprasController@changeup');
Route::post('/compras/changedown', 'ComprasController@changedown');
Route::post('/compras/paypalOrder/{ID_CLIENTE}/{ID_VENTA}', 'ComprasController@paypalOrder');
Route::post('/sendemail/send', 'SendEmailController@send');
Route::post('/sendemail/sendFacturacion', 'SendEmailController@sendFacturacion');
Route::post('/sendemail/sendBolsaTrabajo', 'SendEmailController@sendBolsaTrabajo');

// Compras con mecado pago
Route::post('/compras/getDesc', 'ComprasController@getDesc');
Route::post('/compras/CompletarPago', 'ComprasController@completarPago')->name('CompletarPago');
Route::get('/compras/mercadoOrder/{id_cliente}/{id_venta}', 'ComprasController@mercadoOrder')->name('mercadoOrder');
Route::get('/compras/mercadoOrderUser/{nombre}/{email}/{telefono}/{id_venta}', 'ComprasController@mercadoOrderUser')->name('mercadoOrderUser');
Route::get('/usuario/login/{id_compra}','Auth\LoginController@showLoginForm')->name('verificar');
Route::get('/usuario/registro/{id_compra}','Auth\RegisterController@showRegisterForm')->name('register_verificar');
Route::post('/compras/initMercadoOrder', 'ComprasController@initMercadoOrder')->name('initMercadoOrder');
Route::post('/compras/sendOrder/{ID_CLIENTE}/{EMAIL}', 'ComprasController@sendOrder')->name('pagarOrden');
Route::post('/compras/verifySales/{ID_CLIENTE}/{EMAIL}/{SUBTOTAL}/{IMPORTE}/{ENVIO}', 'ComprasController@verifySales')->name('verifySales');

Route::post('/compras/registerUser', 'Auth\RegisterController@registerUser')->name('registerUser');

Route::get('/compras/sendEmail/{id_venta}', 'ComprasController@SendEmail');
Route::get('/compras/sendEmailUser/{nombre}/{email}/{telefono}/{id_venta}', 'ComprasController@SendEmailUser');

Route::get('/compras/Mail/{id_venta}', 'ComprasController@Mail');

Route::post('/compras/realizarPagoStripe/{id_venta}/{id_cliente?}', 'ComprasController@realizarPagoStripe')->name('realizarPagoStripe');

Route::post('/compras/realizarPagoStripeSecure/{id_venta}/{id_cliente?}', 'ComprasController@realizarPagoStripeSecure')->name('realizarPagoStripeSecure');
Route::get('/compras/finalizarStripeSecure/{id_venta}/{id_cliente?}', 'ComprasController@finalizarStripeSecure')->name('finalizarStripeSecure');


Route::post('/compras/realizarPago/{id_venta}/{id_client?}', 'ComprasController@realizarPago')->name('realizarPago');

Route::get('/compras/reintentarPago/{id_venta}', 'ComprasController@reintentarPago')->name('reintentarPago');
Route::get('/compras/initMercadoOrderReintentar/{id_venta}', 'ComprasController@initMercadoOrderReintentar')->name('initMercadoOrderReintentar');

Route::post('/componentes/filtros', 'ProductsController@getTallasMenu')->name('tallasProductos');
Route::post('/productos/filtros', 'ProductsController@buscadorProductos')->name('buscadorProductos');

// Evitar errores
Route::get('wp-admin/{OTRA_COSA}', function () {
    return redirect('');
});
Route::get('producto/{OTRA_COSA}', function () {
    return redirect('');
});
Route::get('categoria-producto/{OTRA_COSA}', function () {
    return redirect('');
});
Route::get('categoria-producto/{OTRA_COSA}/{OTRO}', function () {
    return redirect('');
});

// URL DEL POST y  EL REDIRECT
Route::post('/compras/webpay', 'ComprasController@webpay')->name('webpay_POST');
//Genera las vistas de a las alertas
Route::get('/compras/webpay_confirmacion', 'ComprasController@webpay_confirmacion')->name('confirmacionWebpay');
// Rutas para el consumo de las alertas
Route::any('/compras/webpay_url', 'ComprasController@webpay_url')->name('webpay_url');
//Limpia el carrito al finalizar la compra
Route::get('/compras/clearCart', 'ComprasController@clearCart')->name('clearCart');

Route::get('/clear/cache', function(){
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});